# Tiago Gehring, Feb/2015
#
# Sets various variables realated to Multichannel Systems MCStream library 

# Once done this will define
#  MCStream_FOUND
#  MCStream_INCLUDE_DIRS 
#  MCStream_LIBRARIES

FIND_PACKAGE(PackageHandleStandardArgs)

FIND_PATH(MCStream_INCLUDE_DIR MCSAStream.h
	PATHS ${CMAKE_CURRENT_SOURCE_DIR}/extern/MCStreamLibrary/include /usr/local/include /usr/include)

# Use MCStream library
IF (${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
	FIND_LIBRARY(MCStream_LIBRARY NAMES MCStream libMCStream
		PATHS ${CMAKE_CURRENT_SOURCE_DIR}/extern/MCStreamLibrary/osx/lib /usr/local/lib /usr/lib)
ELSEIF (${CMAKE_SYSTEM_NAME} MATCHES "Linux" AND ${CMAKE_SIZEOF_VOID_P} EQUAL "8")
	FIND_LIBRARY(MCStream_LIBRARY NAMES MCStream libMCStream
		PATHS ${CMAKE_CURRENT_SOURCE_DIR}/extern/MCStreamLibrary/linux64/lib /usr/local/lib /usr/lib)
ELSEIF (${CMAKE_SYSTEM_NAME} MATCHES "Windows" AND ${CMAKE_SIZEOF_VOID_P} EQUAL "8")
	FIND_LIBRARY(MCStream_LIBRARY NAMES MC_StreamAnsiLibS 
		PATHS ${CMAKE_CURRENT_SOURCE_DIR}/extern/MCStreamLibrary/win64/lib /usr/local/lib /usr/lib)
ENDIF()

SET(MCStream_INCLUDE_DIRS ${MCStream_INCLUDE_DIR})
SET(MCStream_LIBRARIES ${MCStream_LIBRARY})

FIND_PACKAGE_HANDLE_STANDARD_ARGS(MCStream DEFAULT_MSG MCStream_INCLUDE_DIRS MCStream_LIBRARIES)
