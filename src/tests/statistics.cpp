#define BOOST_COMPUTE_DEBUG_KERNEL_COMPILATION

#include <boost/test/unit_test.hpp>
#include "multichannel/data_block.hpp"
#include "statistics/mean_stddev.hpp"
#include "statistics/median.hpp"
#include "setup.hpp"

BOOST_AUTO_TEST_SUITE(multichannel_statistics_tests)

// data for comparing results with matlab
std::vector<double> statistics_data{ {
   0.265406, 0.819335, 0.313562, 0.687843, 0.980163, 0.855675, 0.768211,
   0.452138, 0.0272429, 0.0131091, 0.396876, 0.483397, 0.634053,
   0.46667, 0.282128, 0.53001, 0.381363, 0.604896, 0.925448, 0.407013,
   0.677931, 0.277433, 0.604638, 0.650107, 0.945457, 0.555555, 0.520742,
   0.23346, 0.975522, 0.590067, 0.115594, 0.685581, 0.797393, 0.696296,
   0.357462, 0.850843, 0.30785, 0.366868, 0.888896, 0.952173, 0.0293084,
   0.252059, 0.371741, 0.518341, 0.767493, 0.926144, 0.302432, 0.803049,
   0.291677, 0.274989 }};

const double mean = 0.537633;
const double stddev = 0.271199;
const double median = 0.525376;

using namespace data_source;

template<typename T>
void multiplex_channels(T* pdata, size_t n, multichannel::data_block<T>& blk)
{
   T* pout = blk.host_ptr();
   size_t nchan = blk.dimensions().channels;
   for (size_t i = 0; i < n; ++i)
   {
      for (size_t j = 0; j < nchan; ++j)
         pout[i*nchan + j] = pdata[i];
   }
}

BOOST_AUTO_TEST_CASE(mean_stddev_tests)
{
   using data_block_type = multichannel::data_block<double>;

   // input test data
   data_dimensions dim{ 0, 1, 0, statistics_data.size(), storage_format::row_major, 0 };
   data_block_type input{ opencl_defaults::context(), dim, &statistics_data[0] };

   statistics::mean_stddev<double> m(opencl_defaults::context(), 1);

   m(input, opencl_defaults::queue());
   BOOST_CHECK( fabs(m.mean()[0] - mean) < 1e-1 );
   BOOST_CHECK( fabs(m.standard_deviation(opencl_defaults::queue())[0]- stddev) < 1e-2 );
}

BOOST_AUTO_TEST_CASE(mean_stddev_multichannel_tests)
{
   using data_block_type = multichannel::data_block<double>;

   // input test data
   data_dimensions dim{ 0, 60, 0, statistics_data.size(), storage_format::row_major, 0 };
   data_block_type input{ opencl_defaults::context(), dim };

   multiplex_channels<double>(&statistics_data[0], statistics_data.size(), input);
   statistics::mean_stddev<double> m(opencl_defaults::context(), 1);

   m(input, opencl_defaults::queue());
   for (size_t i = 0; i < dim.channels; ++i)
   {
      BOOST_CHECK( fabs(m.mean()[i] - mean) < 1e-1 );
      BOOST_CHECK( fabs(m.standard_deviation(opencl_defaults::queue())[i]- stddev) < 1e-2 );
   }
}

BOOST_AUTO_TEST_CASE(mean_stddev_multichannel_tests_simd)
{
   using data_block_type = multichannel::data_block<double>;

   // input test data
   data_dimensions dim{ 0, 60, 0, statistics_data.size(), storage_format::row_major, 0 };
   data_block_type input{ opencl_defaults::context(), dim };

   multiplex_channels<double>(&statistics_data[0], statistics_data.size(), input);
   statistics::mean_stddev<double> m(opencl_defaults::context(), 4);

   m(input, opencl_defaults::queue());
   for (size_t i = 0; i < dim.channels; ++i)
   {
      BOOST_CHECK( fabs(m.mean()[i] - mean) < 1e-1 );
      BOOST_CHECK( fabs(m.standard_deviation(opencl_defaults::queue())[i]- stddev) < 1e-2 );
   }
}

BOOST_AUTO_TEST_CASE(median_tests)
{
   using data_block_type = multichannel::data_block<double>;

   // input test data
   data_dimensions dim{ 0, 1, 0, statistics_data.size(), storage_format::row_major, 0 };
   data_block_type input{ opencl_defaults::context(), dim, &statistics_data[0] };

   statistics::median<double> m{opencl_defaults::context(), 1, 1000};

   m(input, true, opencl_defaults::queue());

   BOOST_CHECK( fabs(m.median_value(opencl_defaults::queue())[0] - median) < 1e-1 );
}

BOOST_AUTO_TEST_CASE(median_multichannel_tests)
{
   using data_block_type = multichannel::data_block<double>;

   // input test data
   data_dimensions dim{ 0, 60, 0, statistics_data.size(), storage_format::row_major, 0 };
   data_block_type input{ opencl_defaults::context(), dim };

   multiplex_channels<double>(&statistics_data[0], statistics_data.size(), input);
   statistics::median<double> m(opencl_defaults::context(), 1, 256*256);

   auto& queue = opencl_defaults::queue();
   m(input, false, queue);
   queue.finish();
   for (size_t i = 0; i < dim.channels; ++i)
   {
      BOOST_CHECK( fabs(m.median_value(opencl_defaults::queue())[i] - median) < 1e-1 );
   }
}

BOOST_AUTO_TEST_CASE(median_multichannel_tests_simd)
{
   using data_block_type = multichannel::data_block<double>;

   // input test data
   data_dimensions dim{ 0, 60, 0, statistics_data.size(), storage_format::row_major, 0 };
   data_block_type input{ opencl_defaults::context(), dim };

   multiplex_channels<double>(&statistics_data[0], statistics_data.size(), input);
   statistics::median<double> m(opencl_defaults::context(), 4, 256*256);

   auto& queue = opencl_defaults::queue();
   m(input, false, queue);
   queue.finish();
   for (size_t i = 0; i < dim.channels; ++i)
   {
      BOOST_CHECK( fabs(m.median_value(opencl_defaults::queue())[i] - median) < 1e-1 );
   }
}

BOOST_AUTO_TEST_SUITE_END()
