#define BOOST_COMPUTE_DEBUG_KERNEL_COMPILATION

#include <boost/test/unit_test.hpp>
#include <boost/compute/algorithm/reduce.hpp>
#include "data_source/mcs_file_analog.hpp"
#include "spikes/spike_detector.hpp"
#include "spikes/spikes_waveform.hpp"
#include "multichannel/analog_data.hpp"
#include "dsp/bidirectional_filter.hpp"
//#include "spikes_smoothed_waveform.hpp"
#include <vector>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/tokenizer.hpp>
#include "setup.hpp"

namespace fs = boost::filesystem;
using namespace dsp;

BOOST_AUTO_TEST_SUITE(spike_detection_tests)

// default filter coefficients (2nd order filter)
std::vector<double> b{{0.512646935729248, -0.003868346278688, -1.017074781745147, -0.003868346278688, 0.512646935729250}};
std::vector<double> a{{1.000000000000000, -0.872973291418191, -0.487313079047431,  0.099304035657658, 0.309222050406897}};

std::vector<double> expected_noise{{
   3.1244e-06, 2.39952e-06, 3.03921e-06, 3.50795e-06, 3.46991e-06, 2.44645e-06, 2.65226e-06, 3.26932e-06, 3.08632e-06, 2.95963e-06,
   2.73085e-06, 2.57956e-06, 3.42402e-06, 2.70224e-06, 8.44335e-07, 2.47252e-06, 2.79081e-06, 2.68806e-06, 2.6003e-06, 3.04386e-06,
   3.12061e-06, 2.5166e-06, 2.67643e-06, 2.83481e-06, 3.04664e-06, 3.21196e-06, 2.47684e-06, 2.45924e-06, 2.67859e-06, 2.69079e-06,
   2.66466e-06, 2.48809e-06, 2.55275e-06, 2.86572e-06, 2.42641e-06, 2.79194e-06, 3.00701e-06, 2.90313e-06, 2.83976e-06, 3.23203e-06,
   3.76114e-06, 3.0822e-06, 2.61601e-06, 3.1663e-06, 2.6474e-06, 3.24616e-06, 2.78832e-06, 3.55695e-06, 3.31657e-06, 3.40239e-06,
   3.02204e-06, 3.19434e-06, 3.21054e-06, 2.87594e-06, 2.70622e-06, 4.03486e-06, 2.85138e-06, 2.77983e-06, 3.2605e-06, 2.5957e-06}};

/* Output from Matlab (first channel):
 *
47
orig_vals =
   1.0e-04 *
   -0.0305
   -0.0549
   -0.0098
   -0.0153
   -0.0055
   -0.0226
   -0.0665
   -0.1099
   -0.0995
   -0.1117
b =
    0.5126   -0.0039   -1.0171   -0.0039    0.5126
a =
    1.0000   -0.8730   -0.4873    0.0993    0.3092
vals =
   1.0e-05 *
    0.0214
   -0.1220
    0.2537
    0.3849
    0.4200
    0.3614
   -0.0795
   -0.3734
   -0.4160
   -0.2198
noise_std_detect =
   3.1234e-06
thr =
   1.5617e-05
   */

std::vector<double> orig_vals{{-0.0305e-4, -0.0549e-4, -0.0098e-4, -0.0153e-4, -0.0055e-4, -0.0226e-4, -0.0665e-4, -0.1099e-4, -0.0995e-4, -0.1117e-4}};
std::vector<double> filt_vals{{0.0214e-5, -0.1220e-5, 0.2537e-5, 0.3849e-5, 0.4200e-5, 0.3614e-5, -0.0795e-5, -0.3734e-5, -0.4160e-5, -0.2198e-5}};

BOOST_AUTO_TEST_CASE(detect_spikes_single_channel)
{
   using namespace std;

   fs::path ex_path = fs::system_complete(boost::unit_test::framework::master_test_suite().argv[0]);

   auto ctx = opencl_defaults::context();
   auto queue = opencl_defaults::queue();

   using spikes_store = map<size_t, vector<double>>;
   // read spikes from file
   spikes_store spikes_in;
   spikes_store spikes_found;
   ifstream fin{ex_path.parent_path().string() + "/../test_data/spikes.txt"};
   string line;
   while(getline(fin, line))
   {
      boost::tokenizer<> tok{line};
      auto it = begin(tok);
      size_t en = boost::lexical_cast<size_t>(*it);
      ++it;
      double tm = boost::lexical_cast<double>(*it);
      spikes_in[en].push_back(tm);
   }

   using source_type = data_source::mcs_file_analog;
   using data_type = source_type::data_type;
   using source_buffer_type = multichannel::analog_data<data_type>;
   using out_buffer_type = multichannel::analog_data<double>;
   using filter_type = bidirectional_filter<data_type, double>;
   using filter_kernel_type = filter_type::kernel_type;
   using spike_detector_type = spike_detector<double>;
   using spikes_type = spikes_waveform<double, spike_artifact_detector<double>>;
   filter_kernel_coefficients coeff{b, a};

   source_type f{};
   f.load_file(ex_path.parent_path().string() + "/../test_data/152_I6543_DIV36_mouse_spontaneous00097.mcd");

   compute::vector<double> noise_level{ ctx };

   for (int iter = 0; iter < 4; ++iter)
   {
      // read analog data
      size_t bs = (iter % 2) == 0 ? 100000000 : 200000; // alternate between large/small chunks
	   source_buffer_type in{ ctx, 1, bs };

      f.position(0);
      in.read(f, f.dimensions().samples, queue);

      /**
       * Test if data being read is ok
       */
      {
/*         out_buffer_type scaled{ctx, 1};
         in.scale(scaled, queue);
         queue.finish();

         auto it = scaled.cbegin(0, queue);
         for(size_t i = 0; i < orig_vals.size(); ++i)
         {
            std::cout << "Orig: " << orig_vals[i] << " scaled: " << *it << std::endl;
            BOOST_CHECK( fabs((*it - orig_vals[i])/orig_vals[i]) < 0.01 );
            ++it;
         }*/
      }

      filter_type filt{ ctx
                      , filter_kernel_type{ coeff, 1, static_cast<int>(filter_kernel_options::scale_input), in.input_offset(), in.input_scalling()}
      };
      // allocate another buffer to store the filtered data
      queue.finish();
	   out_buffer_type inter{ ctx, 1, bs };
      inter.reshape(in);
	   out_buffer_type out{ ctx, 1, bs };
      out.reshape(in);

      // apply filter
      filt(in.data()[0], inter.data()[0], out.data()[0], true, queue);
      queue.finish();
      {
      /*   std::cout << "\nINTERMEDIATE: ";
         auto it = inter.cbegin(0, queue);
         for(size_t i = 0; i < filt_vals.size(); ++i)
         {
            std::cout << *it << ", ";
            ++it;
         }*/
      }

      /**
       * Test if filtered data is ok
       */
      {
 /*        auto it = out.cbegin(0, queue);
         for(size_t i = 0; i < filt_vals.size(); ++i)
         {
            BOOST_CHECK( fabs((*it - filt_vals[i])/filt_vals[i]) < 0.02 );
            ++it;
         } */
      }

      spike_detector_type spk_det{ctx, 1, 5., 2.5};

      // storage for the spikes
      size_t pts = (iter < 2 ? 44 : 2000);
      spikes_type spks{100000, 20, pts, 1000.};

      spk_det.update_threshold(out.data()[0], true, queue);
      double sr = f.dimensions().sampling_rate;

      for (auto& x : spikes_found)
      {
         x.second.clear();
      }

      for(auto& x : out.data())
      {
         spk_det(x, spks, false, queue);
         queue.finish();
         // copy spikes
         for(auto& spk : spks)
         {
            if (spk.status() == spike_status::valid)
               spikes_found[spk.channel()].push_back(spk.sample()*1000./sr);
         }
      }

      queue.finish();

      auto& noise = spk_det.noise_level();
      if (iter == 0)
      {
         noise_level = noise;
      }
      else
      {
         // compare noise levels
         compute::vector<double> diff(noise_level.size());
         compute::transform(spk_det.noise_level().begin(), spk_det.noise_level().end(), noise_level.begin(), diff.begin(), compute::minus<double>(), queue);
         double max_val;
         compute::reduce(
            compute::make_transform_iterator(diff.begin(), compute::fabs<double>()),
            compute::make_transform_iterator(diff.end(), compute::fabs<double>()),
            &max_val,
            compute::fmax<double>(),
            queue
         );
         BOOST_CHECK( max_val < 1e-5 );
      }

	  vector<size_t> elect_map;
	  for (size_t i = 0; i < f.dimensions().channels; ++i)
	  {
		  std::string lbl = f.channel_identification(i);
		  size_t en = boost::lexical_cast<size_t>(lbl.substr(lbl.size() - 2, 2));
		  elect_map.push_back(en);
	  }
      // compare spikes
     size_t ntot = 0;
     size_t nfound = 0;
     std::cout << std::endl << "TOTAL SPIKES: ";
	  for (size_t i = 0; i < elect_map.size(); ++i)
         std::cout << spikes_found[i].size() << ", ";
	  for (size_t i = 0; i < 60; ++i)
     {
         auto it_spk_in = spikes_in.find(elect_map[i]);
		   bool has_spk = (it_spk_in != spikes_in.end()) && (!it_spk_in->second.empty());
         size_t n = spikes_found[i].size();
         nfound += n;
		   ntot += spikes_in[elect_map[i]].size();

         if (n == 0)
         {
            if (has_spk)
               std::cout << "\nChannel " << i << " found no spike where there are some";
            BOOST_CHECK(!has_spk);
         }
         else
         {
            if (!has_spk)
               std::cout << "\nChannel " << i << " found " << n << " spikes instead of none!";
            BOOST_CHECK(has_spk);
            if (has_spk)
            {
               //if (spikes[chan_map[i]].size() == n)
               //{
   //                compare timings
                  //for(size_t j = 0; j < n; ++j)
                  //{
                     //if (fabs(spks.spike_sample(i, j) - spikes_in[chan_map[i]][j]) > 3.)
                     //{
                        //std::cout << "Mismatched spike time: spike #" << j << ", sample " << spks[j].time_ms << " -> should be " << spikes[chan_map[i]][j] << std::endl;
                     //}
                     //BOOST_CHECK(fabs(spks[j].time_ms - spikes[chan_map[i]][j]) < 2.);
                  //}
               //}
            }
         }
      }
      std::cout << "Iteration: " << iter << ", total spikes (MATLAB): " << ntot << "; found " << nfound << " of in the data" << std::endl;
      // has to find at least 99% (or 101%..) of the spikes
      BOOST_CHECK( (1. - (double)ntot/nfound) < 0.01);
   }
}

BOOST_AUTO_TEST_SUITE_END();
