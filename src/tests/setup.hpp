#pragma once

#include <boost/compute/core.hpp>

namespace compute = boost::compute;

struct opencl_defaults
{
   static compute::device device()
   {
      static int idev = -1;

      if (compute::system::device_count() == 0)
         throw std::runtime_error{ "No OpenCL devices found. Please install a OpenCL runtime (e.g. from Intel or AMD) and try again." };

      if (idev == -1)
      {
         int idx = 0;
         for (const auto& dev : compute::system::devices())
         {
            // check if no explicit device was specified and if it is a CPU; in that case use it
            if (dev.type() == compute::device::cpu)
            {
               idev = idx;
               break;
            }
            ++idx;
         }
      }

      if (idev == -1)
         throw std::runtime_error{ "No suitable OpenCL device!" };

      return compute::system::devices()[idev];
   }

   static compute::context context()
   {
      static compute::context ctx{ device() };
      return ctx;
   }

   static compute::command_queue& queue()
   {
      static compute::command_queue queue{ context(), device() };
      return queue;
   }
};