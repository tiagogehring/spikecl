function export_spikes( dir_name, out_fn )
%EXPORT_SPIKES Summary of this function goes here
%   Detailed explanation goes here
    spks = [];
    files = dir(fullfile(dir_name, '/*spikes.mat'));
    for i = 1:length(files)
        fn = files(i).name;
        pos = strfind(fn, 'chan_x');
        chan = str2num(fn(pos + 6:pos + 7));
    
        load(fullfile(dir_name, files(i).name));
        for j = 1:length(index)
            spks = [spks; chan, index(j)];
        end        
    end
    csvwrite(out_fn, spks);
end

