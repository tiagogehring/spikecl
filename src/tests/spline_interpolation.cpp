#include <boost/test/unit_test.hpp>
#include <vector>
#include "interpolation/cubic_spline.hpp"

BOOST_AUTO_TEST_SUITE(spline_interpolation_tests)

BOOST_AUTO_TEST_CASE(cubic_spline_interpolation_single_test)
{
   // input test data
   std::vector<double> spline_data;
   std::vector<double> spline_vals;
   for (int i = 0; i < 40; ++i)
   {
      spline_vals.push_back(i*i);
      if (i % 4 == 0)
         spline_data.push_back(i*i);
   }
   auto ctx = compute::system::default_context();
   compute::vector<double> input{spline_data};
   compute::vector<size_t> offsets{1};
   offsets[0] = 0;
   compute::vector<double> expected{spline_vals};

   interpolation::cubic_spline<double> interpol{spline_data.size(), 4};

   compute::vector<double> output(4*(spline_data.size() - 1) + 1);

   interpol(input, offsets, offsets.size(), output, compute::system::default_queue());

   for (int i = 4; i <= output.size() - 4; ++i)
      BOOST_CHECK( fabs(output[i] - expected[i]) / expected[i] < 0.03 );
}

BOOST_AUTO_TEST_SUITE_END()
