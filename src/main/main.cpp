#define BOOST_COMPUTE_DEBUG_KERNEL_COMPILATION

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "data_source/data_source_merged.hpp"
#include "data_source/mcs_file_analog.hpp"
#include "config/common_setup.hpp"
#include "config/filter_setup.hpp"
#include "config/spike_detection_setup.hpp"
#include "config/burst_detection_setup.hpp"
#include "config/logging.hpp"
#include "git_sha1.hpp"
#include "mea_data.hpp"
#include "spikes_store.hpp"
#include "utility/filename_match.hpp"

namespace po = boost::program_options;
namespace fs = boost::filesystem;

using mcs_file_type = data_source::mcs_file_analog;
using data_source_type = data_source::data_source_merged<mcs_file_type>;
using mea_data_type = mea_data<fp_type, data_source_type>;
using mea_data_ptr = std::unique_ptr<mea_data_type>;
using bursts_type = typename mea_data_type::bursts_type;
using bursts_ptr = std::unique_ptr<bursts_type>;
//
int main(int argc, char* argv[])
{
   using namespace std;

   init_logging("", logging_severity_level::debug, logging_severity_level::none);

   /********
    * Process command line arguments
    */
   mea_data_type::options opts = mea_data_type::default_options();
   bool create_dir = true; // create sub-directory
   bool filter = true; // filter data flag
   bool info = true; // save info flag
   bool detect_spikes = true; // save spikes flag
   bool detect_bursts = false; // detect bursts flag
   bool extract_chan = false; // extract channel flag
   bool overwrite = false; // true if output files should be overwritten
 
   // the next objects process command line arguments and initialize different
   // aspects of the program (OpenCL device, filtering, spike detection, etc)
   common_setup cs;
   filter_setup fs;
   spike_detection_setup sds;
   burst_detection_setup bds;

   try
   {
      // construct list of available command line arguments
	   po::options_description desc("Allowed options", 120);

      // general options
	   po::options_description go("General", 120);
      go.add_options()
         ("help", "show this message")
         ("input", po::value<vector<string>>(), "input file names")
         ("output,o", po::value<string>(), "output directory (if ommited current directory will be used)")
         ("no-create-dir,C", po::value<bool>(&create_dir)->default_value(create_dir)->implicit_value(false)->zero_tokens(), "don't create sub-directory for generated files")
         ("overwrite,O", po::value<bool>(&overwrite)->default_value(overwrite)->implicit_value(true)->zero_tokens(), "overwrite output files if they already exist")
         ("no-info", po::value<bool>(&info)->default_value(info)->implicit_value(false)->zero_tokens(), "don't create information file ([input]_info.txt)")
         ("no-spikes", po::value<bool>(&detect_spikes)->default_value(detect_spikes)->implicit_value(false)->zero_tokens(), "don't detect spikes")
         ("no-filter", po::value<bool>(&filter)->default_value(filter)->implicit_value(false)->zero_tokens(), "don't apply filter to raw data")
         ("extract-channels,E", po::value<bool>(&extract_chan)->default_value(extract_chan)->implicit_value(true)->zero_tokens(), "save full channels to individual files (file [input]_chan_x##.dat will be generated for each channel)")
         ("bursts", po::value<bool>(&detect_bursts)->default_value(detect_bursts)->implicit_value(true)->zero_tokens(), "Detect multi-site activiy bursts");
       // other common options
      cs.add_options(go);
      desc.add(go);

      // filtering options
      po::options_description fo{"Filtering", 120};
      fs.add_options(fo);
      desc.add(fo);

      // spike detection options
      po::options_description sdo{"Spike detection", 120};
      sds.add_options(sdo);
      desc.add(sdo);
      
      // burst detection options
      po::options_description bdo{ "Burst detection", 120 };
      bds.add_options(bdo);
      desc.add(bdo);

	   po::positional_options_description pos_desc;
	   pos_desc.add("input", -1);

	   po::variables_map vm;
	   po::store( po::command_line_parser(argc, argv)
				 .options(desc)
				 .positional(pos_desc)
				 .run(), vm);
	   po::notify(vm);

	   if (vm.count("help"))
	   {
		  cout << endl << desc << endl;
		  cout << endl <<
			 "By default all channels of the input file will be processed." << endl <<
			 "Spike times and waveforms will be saved in the files (input)_spikes.txt and " << endl <<
			 "(input)_waveforms.txt; general file information such as number of channels and samples will be " << endl <<
			 "saved to file (input)_info.txt." << endl;
		  return 0;
	   }

	   if (vm.count("input") == 0)
	   {
		  cerr << "No input file specified!";
		  cout << endl << desc << endl;
		  return 1;
	   }

      // process command line options, initialize different sub-systems
      cs.process_options(vm, logger{ logging_severity_level::info });
      opts.common = cs.values();
      fs.process_options(vm); // filtering
      sds.process_options(vm); // spike detection
      bds.process_options(vm);
      opts.spike_detection = sds.values();           
      opts.burst_detection = bds.values();

	   vector<string> files = vm["input"].as<vector<string>>();
	   vector<fs::path> files_p;
      // input files (the data source can merge more than one MCS file)
      data_source_type f{};
      bool merge = opts.common.merge;
      
      // sanity check
      if (detect_bursts && !detect_spikes)
         throw std::runtime_error{ "Spike detection needs to be active for detecting bursts!" };

	   // process input files
	   for(auto& fn : files)
	   {
		  // is it a file mask?
		  if (fn.find('*') == string::npos)
		  {
			 // regular file - just check if it exists
			 if (!fs::exists(fn))
			 {
				BOOST_LOG_SEV(g_log, logging_severity_level::error) << "File " << fn << " not found!" << endl;
				return 1;
			 }
			 if (fs::is_directory(fn))
			 {
				BOOST_LOG_SEV(g_log, logging_severity_level::error) << fn << " is a directory!" << endl;
				return 1;
			 }

          files_p.push_back(fn);
          if (merge)
          {                          
             // merge it to the data source
             f.add(mcs_file_type{ fn });
          }          
		  }
		  else
		  {
			 // file mask -> match files
			 fs::path p(fn);

			 fs::directory_iterator end_itr; // Default ctor yields past-the-end
			 for( fs::directory_iterator i(p.parent_path()); i != end_itr; ++i )
			 {
				 // Skip if not a file
				 if( !fs::is_regular_file( i->status() ) )
					continue;

				 try
				 {
					 if (!filename_match(fn, i->path().string()))
						 continue;
				 }
				 catch (exception&)
				 {
					 throw runtime_error{ "Invalid file mask!" };
				 }

             files_p.push_back(i->path());
             if (merge)
             {                
                // merge it to the data source
                f.add(mcs_file_type{ i->path().string() });
             }             
			 }
		  }
	   }

	   fs::path outdir;
	   if (vm.count("output"))
	   {
		  outdir = fs::path(vm["output"].as<string>());
		  if (!fs::exists(outdir))
		  {
			 cerr << "Directory " << outdir << " not found. Aborting.";
			 return 1;
		  }
	   }
	   else
	   {
		  outdir = fs::current_path();
	   }      
      
      // this will store and write spikes to a file
      spikes_store<fp_type> spk_store{ 500000, sds.values() };

      // process each file sequencially  
      auto start_total = chrono::steady_clock::now();
      size_t nsamples_tot = 0;
      size_t fi = 0;

      // pointer to the object that does all the hard work
      mea_data_ptr mea_dt;

      for(const auto& fpath : files_p)
      {
         auto start = chrono::steady_clock::now();

         ++fi;
         if (merge)
         {
            BOOST_LOG_SEV(g_log, logging_severity_level::info) << "************ Merging " << f.count() << " file(s) *************";
            for (size_t i = 0; i < files_p.size(); ++i)
            {
               BOOST_LOG_SEV(g_log, logging_severity_level::info) << "#" << (i + 1) << ": " << files_p[i].filename();
            }
         }
         else
         {
            BOOST_LOG_SEV(g_log, logging_severity_level::info) << "************ Processing file " << fi << "/" << files_p.size() << "*************";
            // load file and add it to the list of files
            f = data_source_type{};
            f.add(mcs_file_type{ fpath.string() });            
            BOOST_LOG_SEV(g_log, logging_severity_level::info) << "File " << fpath.filename() << " loaded.";
         }         

         BOOST_LOG_SEV(g_log, logging_severity_level::info) << "Sampling rate: " << f.dimensions().sampling_rate << " Hz  Time span: " << f.dimensions().timespan() << " seconds";

         // maps channel indices to electrode numbers
         vector<size_t> elect_map;
         for (size_t i = 0; i < f.dimensions().channels; ++i)
         {
            std::string lbl = f.channel_identification(i);
            size_t en = boost::lexical_cast<size_t>(lbl.substr(lbl.size() - 2, 2));
            elect_map.push_back(en);
         }

         // create output directory
         fs::path foutdir = outdir;
         if (create_dir)
         {
            fs::path p = outdir;
            p /= fpath.stem();
            if (!fs::exists(p))
            {
               BOOST_LOG_SEV(g_log, logging_severity_level::info) << "Creating directory " << p.string();
               fs::create_directory(p);
            }
            else
            {
               if (!overwrite)
               {
                  // skip file
                  BOOST_LOG_SEV(g_log, logging_severity_level::warning) << "Warning: output directory " << p.string()
                     << " already exists, skipping file (use --overwrite to force overwritting)";
                  if (merge)
                     break;
                  else
                     continue;
               }
            }
            foutdir = p;
         }

         BOOST_LOG_SEV(g_log, logging_severity_level::info) << "Found " << f.dimensions().channels << " analog channels with " << f.dimensions().samples << " samples each.";

         if (info)
         {
            fs::path p = foutdir;
            p /= fpath.stem();
            p += "_info.txt";
            if (fs::exists(p) && !overwrite)
            {
               BOOST_LOG_SEV(g_log, logging_severity_level::warning) << "Warning: file " << p.filename() <<
                     " already exists, skipping file (use --overwrite to force overwritting)";
               continue;
            }

            BOOST_LOG_SEV(g_log, logging_severity_level::info) << "Writing file information to " << p.filename();
            ofstream fout{p.string()};
            // same information in the same format as the original get_info tool
            fout << f.dimensions().timespan()*1000. << endl
                 << f.dimensions().samples << endl
                 << f.dimensions().sampling_rate << endl
                 << "Nelec" << f.dimensions().channels << endl;
            for (auto n : elect_map)
               fout << n << endl;
         }

         // do we want to look for the spikes?
         if (!detect_spikes && !extract_chan)
            continue;

         vector<ofstream> channel_data_f;
         if (extract_chan)
         {
            channel_data_f.resize(f.dimensions().channels);
         }

         // check if we need to construct the main processing object
         auto max_vw = cs.maximum_vector_width(f.dimensions().channels);
         if (!mea_dt || (mea_dt->options_values().common.vector_width != max_vw))
         {
            opts.common.vector_width = max_vw;
            opts.common.parallel_channels = cs.optimal_parallel_channels(max_vw);
            mea_dt.reset( new mea_data_type{cs.device(), opts} );

            // set the progress update function -> does not depend on the actual file being processed
            mea_dt->on_notification(
               [&](mea_data_operation op, mea_data_operation_status stat, const data_source::data_dimensions& dim, size_t tm_us) {
               switch(stat)
               {
                  case mea_data_operation_status::block_start:
                  case mea_data_operation_status::block_finish:
                  {
                     std::string op_desc;
                     switch(op)
                     {
                        case mea_data_operation::read_data:
                           op_desc = "reading"; break;
                        case mea_data_operation::filter_data:
                           if (stat == mea_data_operation_status::block_start)
                              op_desc = "processing";
                           else
                              return;
                           break;
                        case mea_data_operation::detect_spikes:
                           if (stat == mea_data_operation_status::block_finish)
                           {
                              op_desc = "processing";
                              if (opts.common.verbose)
                              {
                                 auto thresh = mea_dt->spike_det().thresholds(mea_dt->opencl_queue());
                                 mea_dt->opencl_queue().finish();
                                 std::stringstream buf;
                                 for (auto x : thresh)
                                    buf << x << ", ";
                                 BOOST_LOG_SEV(g_log, logging_severity_level::debug) << "] THRESHOLDS: " << buf.str();
                              }
                           }
                           else
                              return;
                           break;
                        case mea_data_operation::detect_bursts:
                           if (stat == mea_data_operation_status::block_start)
                              op_desc = "detecting bursts";
                           else
                              return;
                           break;
                        case mea_data_operation::all_operations:
                           {
                           if (extract_chan)
                           {                             
                              // write raw data out						            
                              for (size_t c = dim.channel_offset; c < dim.channels + dim.channel_offset; ++c)
                              {
                                 // make sure that the individual files for each channel are open
                                 if (!channel_data_f[c].is_open())
                                 {
                                    // open file
                                    fs::path dat_path = foutdir;
                                    dat_path /= fpath.stem();
                                    stringstream ss;
                                    // ss << "_chan_x" << elect_map[c] << ".dat";
                                    ss << "_chan" << c << ".dat";
                                    dat_path += ss.str();
                                    channel_data_f[c].open(dat_path.string().c_str(), ios::out | ios::trunc | ios::binary);
                                 }
                              }

                              BOOST_LOG_SEV(g_log, logging_severity_level::info) << "Ch [" << dim.channel_offset + 1 << "-" << dim.channel_offset + dim.channels << "]: Writing channel data to disk";
                             
                              mea_dt->output_buffer().output_channels(&channel_data_f[dim.channel_offset]);
                           }
                           }
                           break;
                        default:
                           assert(false && "Unknown operation");
                     }
                     std::stringstream ss;
                     ss <<"Ch [" << dim.channel_offset + 1 << "-" << dim.channels + dim.channel_offset << "] | [" <<
                           dim.start_time() << "-" << dim.end_time() << "] sec: ";
                     ss << op_desc;
                     if (op == mea_data_operation::read_data &&
                         stat == mea_data_operation_status::block_finish)
                     {
                        ss << " took ";
                        if (tm_us < 1e3)
                           ss << tm_us << " microseconds";
                        else if (tm_us < 1e6)
                           ss << (int)(tm_us/1.e3) << " milliseconds";
                        else
                           ss << tm_us/1.e6 << " seconds;";
                     }
                     else if (stat != mea_data_operation_status::block_start)
                        return;
                     BOOST_LOG_SEV(g_log, logging_severity_level::info) << ss.str();
                  }
                  default:
                  {}
               }
            });
         }

         // construct filter
         if (filter)
            mea_dt->enable_filter(fs.filter_coefficients(f.dimensions().sampling_rate), f);
         else
            mea_dt->disable_filter();

         spikes<fp_type>* spks = nullptr;
         if (detect_spikes)
         {
            // open files to store the spikes and waveforms
            fs::path spk_path = foutdir;
            spk_path /= fpath.stem();
            // reset spike storage
            spk_store.reset(spk_path, f.dimensions().channels, f.dimensions().sampling_rate, elect_map);
            spks = spk_store.spikes();            
         }                  

         bursts_ptr bursts;
         if (detect_bursts)         
            bursts.reset(new bursts_type{});         

         // process file
         size_t nspk = 0;
         mea_dt->process(f, spks, bursts.get(), [&](spikes<fp_type>* new_spks) {
               nspk += new_spks->count();
               // store spikes...
               spk_store(new_spks);
               
               BOOST_LOG_SEV(g_log, logging_severity_level::info) << "Spikes found: " << nspk;
            }
         );

         if (detect_spikes)
         {
            spk_store.flush();
            BOOST_LOG_SEV(g_log, logging_severity_level::info) << "Total number of spikes found: " << nspk;
         }

         if (detect_bursts)
         {
            fs::path p = foutdir;
            p /= fpath.stem();
            p += "_bursts.txt";

            ofstream fout{ p.string() };
            fout << "Start,Peak,End,Half Width,Spikes,Left Threshold, Right Threshold" << endl;

            for (auto b : *bursts)
            {
               fout << b.left_idx*opts.burst_detection.bin_size << "," 
                    << b.peak_idx*opts.burst_detection.bin_size << "," 
                    << b.right_idx*opts.burst_detection.bin_size << ","
                    << b.half_width << "," 
                    << b.spikes << "," 
                    << b.left_thresh_idx*opts.burst_detection.bin_size << ","
                    << b.right_thresh_idx*opts.burst_detection.bin_size << endl;
            }

            // write also detailed information?
            if (opts.burst_detection.detailed_output)            
            {
               /**
                * spikes per bin
                */
               fout.close();
               p = foutdir;
               p /= fpath.stem();
               p += "_bursts_bins.txt";

               fout.open(p.string());
               fout << "Spikes per bin per Channel, bin (ms): " << opts.burst_detection.bin_size << endl;
               
               size_t ic = 0;
               for (auto x : mea_dt->burst_det().binned_data())
               {
                  fout << x;
                  if (++ic % f.dimensions().channels == 0)
                     fout << std::endl;
                  else
                     fout << ",";
               }

               /**
                * bin statistics
                */
               fout.close();
               p = foutdir;
               p /= fpath.stem();
               p += "_bursts_bins_stat.txt";

               fout.open(p.string());
               fout << "Active sites,Total spikes,Activity" << endl;
               
               for (const auto& bin : mea_dt->burst_det().bin_statistics())
               {
                  fout << bin.active_sites << "," << bin.spikes << "," << bin.activity << endl;                  
               }
            }
         }

         size_t ns = f.dimensions().samples*f.dimensions().channels;
         float mb = static_cast<float>(ns*sizeof(data_source_type::data_type)/1024/1024);
         nsamples_tot += ns;
         auto duration = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - start);
         BOOST_LOG_SEV(g_log, logging_severity_level::info) << "DONE. Processed " << ns<< " samples [" <<
           (mb > 1024 ? mb/1024. : mb) << (mb > 1024 ? " GiB" : " MiB") << "] in " << duration.count()/1000. << " seconds. ";		   

         if (merge)
            break;
      }

      if (files_p.size() > 1)
      {
         float mb = static_cast<float>(nsamples_tot*sizeof(data_source_type::data_type)/1024/1024);
         auto duration = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - start_total);
         BOOST_LOG_SEV(g_log, logging_severity_level::info) << "Processing of " << files_p.size() << " files done. Processed " << nsamples_tot <<
            " samples [" << (mb > 1024 ? mb/1024. : mb) << (mb > 1024 ? " GiB" : " MiB") << "] in " << duration.count()/1000. << " seconds.";
      }

      mea_dt.reset();
   }
   catch(exception& e)
   {
      BOOST_LOG_SEV(g_log, logging_severity_level::error) << e.what();
      return 1;
   }

   return 0;
}


