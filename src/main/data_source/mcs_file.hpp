#ifndef MCS_FILE_HPP
#define MCS_FILE_HPP

/// @addtogroup Data_Source
///@{

/**
 * @file   mcs_file.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Provides data input support for Multichannel MCS files via the MCStream library
 */

#include "data_source.hpp"
#include "mcstream_wrapper.hpp"
#include <cmath>
#include <sstream>

namespace data_source
{

/**
 @brief Provides data extraction functions from MCS files (@ref MCS_File_Structure)

 This class is meant to be used as a base for other classes that extract specific data from the MCS file (e.g. electrode raw data)

 @tparam T data type of the output data that will be retrieved (defined by a super-class)
 */
template<typename T>
class mcs_file : public data_source<T>
{
protected:
   using base_type = data_source < T > ;
   // opaque pointer to the MCS wrapper
   void* f_;
   // data stream number
   size_t istrm_;

public:
   /**
    @brief Constructor
    */
   mcs_file()
      : f_{ mcstream_create_file() }
      , istrm_{ (size_t)-1 }
   {}

   mcs_file(mcs_file&& other)
      : base_type(std::move(other))
      , f_{ other.f_ }
      , istrm_{ other.istrm_ }
   {
      other.f_ = nullptr;
   }

   /**
    @brief Destructor
    */
   virtual ~mcs_file()
   {
      if (f_)
      {
         mcstream_delete_file(f_);
         f_ = nullptr;
      }
   }

   /**
   @brief Assignment operators
   */
   mcs_file& operator=(mcs_file&& other)
   {
      base_type::operator=(other);
      f_ = other.f_;
      istrm_ = other.istrm_;
      other.f_ = nullptr;
      return *this;
   }

   // disable copying
   mcs_file(const mcs_file&) = delete;
   mcs_file& operator=(const mcs_file&) = delete;
   
   /**
    @brief Loads an MCS file

    @param fn file name
    */
   virtual void load_file(const std::string& fn)
   {
      short r = mcstream_load_file(f_, fn.c_str());
      if (r != 0)
      {
         std::stringstream ss;
         ss << "Error opening file " << fn;
         switch (r)
         {
         case 2:
            ss << "Wrong file format.";
            break;
         case 3:
            ss << "Wrong file header.";
            break;
         case 4:
            ss << "Empty data file.";
            break;
         default:
            break;
         }
         throw std::runtime_error{ ss.str() };
      }
      istrm_ = (size_t)-1;
   }

   /**
   @brief Returns a string identifier for a channel.
   @pre A file has to be loaded (see @ref load_file)

   @param n Channel number

   @return Channel identification
   */
   std::string channel_identification(size_t n) const override
   {
      assert(f_);
      if (istrm_ == (size_t)-1)
         throw std::runtime_error{ "no stream selected" };

      auto strm = mcstream_stream(f_, static_cast<int>(istrm_));
      return mcstream_channel_identification(strm, static_cast<int>(n));
   }

   /**
    @return Stream number currently active
    */
   size_t stream()
   {
      return istrm_;
   }

   /**
    @brief Sets the current active data stream

    @param istrm Stream number
    @param fmt Data format (row/column major)
    */
   void stream(size_t istrm, storage_format fmt)
   {
      istrm_ = istrm;

      data_dimensions dim;
      dim.sampling_rate = mcstream_sampling_rate(f_);
      dim.sample_offset = 0;
      dim.samples = static_cast<size_t>(dim.sampling_rate*(mcstream_timespan_ns(f_) / 1.e9));
      dim.channel_offset = 0;
      dim.format = fmt;
      auto strm = mcstream_stream(f_, static_cast<int>(istrm_));
      dim.channels = mcstream_channel_count(strm);

      base_type::_reset(dim, mcstream_ad_zero(strm), mcstream_units_per_ad(strm));
   }
};

} // namespace data_source

///@}

#endif
