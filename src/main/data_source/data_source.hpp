#ifndef DATA_SOURCE_HPP
#define DATA_SOURCE_HPP

/// @defgroup Data_Source Data input related classes and functions
///@{

/**
 * @file   data_source.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Provides data input support for Multichannel MCS files via the MCStream library
 */


/**
 * @page MCS_File_Structure MCS file structure
 *
 * MCS files contain one or more data streams (e.g. analog data, digital data). Each data stream
 * in turn can contain multiple data channels.
 */

#include <string>
#include <limits>
#include <algorithm>

#include "data_dimensions.hpp"

namespace data_source
{

/**
 @brief Wrapper around a MCStream file object; provides data extraction functions from MCS files (@ref MCS_File_Structure)

 Currently only extraction of analog data is supported. Multiple analog streams are not supported.
 */
template<typename T>
class data_source
{
   // reference value (e.g. zero volt)
   T input_off_;
   // volts per AD unit
   double input_fac_;
   // current position (sample number) in the file
   size_t pos_;
   // properties about the data (samples, channels, etc)
   data_dimensions dim_;

public:
   using data_type = T;
   const static storage_format data_format = storage_format::unknown;

   /**
    @brief Constructor

    @param fn (optional) File name to load
    */
   data_source()
      : input_off_{(T)0.}
      , input_fac_{std::numeric_limits<double>::quiet_NaN()}
      , pos_{(size_t)-1}
   {}

   const data_dimensions& dimensions() const
   {
      return dim_;
   }

   /**
    @brief Returns a string identifier for a channel.
    @pre A file has to be loaded (see @ref load_file)

    @param is Stream number
    @param n Channel number

    @return Channel identification
    */
   virtual std::string channel_identification(size_t n) const
   {
	   assert(false && "not implemented");
      return "";
   }

   /**
    @brief Reads data from an analog channel
    @pre A file has to be loaded (see @ref load_file)

    @tparam T data type
    @param ichan Channel number
    @param offset Data sample offset
    @param n Number of samples to read
    @param out Output buffer
    */
   virtual size_t read_channel(size_t ichan, size_t offset, size_t n, data_type* out)
   {
	   assert(false && "not implemented");
      return 0;
   }

   /**
    @brief Scales the raw data

    @param raw_data
    @param n
    */
   template<typename U>
   void scale_data(data_type* x, U* y, size_t n)
   {
      // scale data
      std::transform(x, x + n, y, [&](short val) {
            return static_cast<U>((static_cast<data_type>(val) - input_off_)*input_fac_);
      });
   }

   /**
    @brief Reference value for analog values

    @return Reference value
    */
   data_type reference_value() const
   {
      return input_off_;
   }

   /**
    @brief Current offset into the file

    @return Sample number of the current offset
    */
   size_t position() const
   {
      return pos_;
   }

   /**
    @brief Sets the current position in the file`

    @param pos Sample number to move to
    */
   void position(size_t pos)
   {
      assert(pos <= dim_.samples);
      pos_ = pos;
   }

   /**
    @brief Returns volts per unit of the AD

    @return V/unit
    */
   double scalling_factor() const
   {
      return input_fac_;
   }

   /**
    @brief Extracts data from the analog stream
    @pre A file has to be loaded (see @ref load_file)

    Data returned is in the 'row-major' format (@ref sec_data_input_format), i.e.,
    values for each channel for one sample are returned in sequence.

    @param offset Sample offset
    @param n Number of samples to read (output buffer has to have space for n*[number of channels] samples.
    @param out Output buffer
    */
   virtual size_t read(size_t n, data_type* out)
   {
	   assert(false && "not implemented");
      return 0;
   }

protected:
	void _reset(data_dimensions dim, T xoff, double xfac)
	{
      pos_ = 0;
		dim_ = dim;
		input_off_ = xoff;
		input_fac_ = xfac;
	}
};

}

///@}

#endif /* DATA_SOURCE_HPP */
