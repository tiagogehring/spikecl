#ifndef MCS_FILE_ANALOG_HPP
#define MCS_FILE_ANALOG_HPP

/// @addtogroup Data_Source
///@{

/**
 * @file   mcs_file_analog.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Analog (electrode raw) data input from MCS files
 */

/**
 * @page MCS_File_Structure MCS file structure
 *
 * MCS files contain one or more data streams (e.g. analog data, digital data). Each data stream
 * in turn can contain multiple data channels.
 */

#include <limits>
#include <cstdint>
#include "mcs_file.hpp"

// Multichannel Systems headers

namespace data_source
{

/**
 @brief Implements analog data extraction from MCS files
 */
class mcs_file_analog : public mcs_file< unsigned short >
{
   using base_type = mcs_file< unsigned short >;
   using base_type::istrm_;
   using base_type::f_;

public:
   using data_type = unsigned short;
   const static storage_format data_format = storage_format::row_major;

   /**
    @brief Constructor
    */
   mcs_file_analog()
   {
   }

   mcs_file_analog(const std::string& fn)
   {
      load_file(fn);
   }

   /**
    @brief Extracts data from the analog stream
    @pre A file has to be loaded (see @ref load_file)

    Data returned is in the 'row-major' format (@ref sec_data_input_format), i.e.,
    values for each channel for one sample are returned in sequence.

    @param n Number of samples to read (output buffer has to have space for n*[number of channels] samples.
    @param out Output buffer
    */
   size_t read(size_t n, data_type* out) override
   {
      assert(istrm_ != (size_t)-1);
      auto nc = dimensions().channels;
      auto pos = position();
      auto r = mcstream_raw_read(f_, stream(), pos, n, out, n*nc);
      if (r != n*nc)
      {
         std::stringstream ss;
         ss << "Error: read " << r/nc << " instead of " << n << " samples!";
         throw std::runtime_error{ ss.str() };
      }
      // number of samples per channel read (r is the total number of samples)
      auto ds = r / nc;
      position(pos + ds);
      return ds;
   }

   /**
    @brief Loads a file. Reads information such as number of channels and samples

    Data contents of the file are not read at this stage.

    @param fn File name to read
    */
   void load_file(const std::string& fn) override
   {
	  base_type::load_file(fn);

     bool found = false;
     for (int is = 0; is < mcstream_stream_count(f_); ++is)
     {
        auto strm = mcstream_stream(f_, is);
        std::string bn = mcstream_buffer_id(strm);

        // analog channel ?
        if (bn.find("elec") != std::string::npos || bn.find("anlg") != std::string::npos || bn.find("filt") != std::string::npos)
        {
           if (found)
              throw std::runtime_error{ "Multiple analog streams detected (currently unsupported)" };

           // set this as the active stream
           stream(is, storage_format::row_major);
        }
     }
   }
};

} // namespace data_source

///@}

#endif /* MCS_FILE_ANALOG_HPP */
