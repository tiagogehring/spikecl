#ifndef DATA_DIMENSIONS_HPP
#define DATA_DIMENSIONS_HPP

/// @addtogroup Data_Source
///@{

/**
 * @file   data_dimensions.hpp
 * @author Tiago Gehring
 * @date   January, 2015
 * @brief  Definition of data source dimensions (channels x samples)
 */

#include <limits>
#include "data_dimensions.hpp"

namespace data_source
{

/**
 @brief Data storage format: how the data is stored
 */
enum class storage_format
{
   column_major, //!< Data is stored channel wise (all samples from channel 1, followed by all samples from channel 2, and so on..)
   row_major, //!< Data is stored sample wise (sample 1 of all channels, followed by sample 2, and so on...)
   unknown //!< No idea how the data is stored
};

/**
 @brief Stores information about number of channels and samples of a data block or other data source
 */
struct data_dimensions
{
   size_t channel_offset; //!< First channel
   size_t channels; //!< Number of channels
   size_t sample_offset; //!< First sample number
   size_t samples; //!< Number of samples
   storage_format format; //!<  Data storage format (column/row major)
   double sampling_rate; //!< Data sampling rate in Hz

   /**
    @brief Empty constructor
    */
   data_dimensions()
      : channel_offset{(size_t)-1}
      , channels{0}
      , sample_offset{(size_t)-1}
      , samples{0}
      , format{storage_format::unknown}
      , sampling_rate{std::numeric_limits<double>::quiet_NaN()}
   {}

   /**
    @brief Constructor

    @param co #channel_offset
    @param nc #channels
    @param so #sample_offset
    @param ns #samples
    @param fmt #format
    @param sr #sampling_rate
    */
   data_dimensions(size_t co, size_t nc, size_t so, size_t ns, storage_format fmt = storage_format::unknown, double sr = 0)
      : channel_offset{co}
      , channels{nc}
      , sample_offset{so}
      , samples{ns}
      , format{fmt}
      , sampling_rate{sr}
   {}

   /**
    @return true if dimensions are empty/zero
    */
   bool empty() const
   {
      return samples == 0 || channels == 0;
   }

   /**
    @brief Computes the starting time based on the sample number and sampling rate

    @return Start time in seconds
    */
   double start_time() const
   {
      return 1./sampling_rate*sample_offset;
   }

   /**
    @brief Computes the end time based on the sample number and sampling rate

    @return End time in seconds
    */
   double end_time() const
   {
      return 1./sampling_rate*(sample_offset + samples);
   }

   /**
    @return Time span in seconds
    */
   double timespan() const
   {
      assert(sampling_rate > 0.);
      return samples/sampling_rate;
   }

   /**
   @return Tests if we have valid values
   */
   bool valid() const
   {
      return (channel_offset != static_cast<size_t>(-1)) && (sample_offset != static_cast<size_t>(-1));
   }
};

}

///@}

#endif /* DATA_DIMENSIONS_HPP */
