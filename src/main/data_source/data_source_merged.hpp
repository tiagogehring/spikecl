#ifndef DATA_SOURCE_MERGED_HPP
#define DATA_SOURCE_MERGED_HPP

/// @defgroup Data_Source Data input related classes and functions
///@{

/**
* @file   data_source_merged.hpp
* @author Tiago Gehring
* @date   April, 2016
* @brief
*/

#include "data_source.hpp"

namespace data_source
{
   /**
   @brief Combines multiple data sources making extracting data from them transparent (@ref MCS_File_Structure)
   */
   template<typename DataSource>
   class data_source_merged : public data_source<typename DataSource::data_type>
   {
      using base_type = data_source<typename DataSource::data_type>;

      // the data sources that are to be merged
      std::vector<DataSource> data_src_;
      // index of current data source being read
      size_t i_src_;

   public:
      using data_type = typename DataSource::data_type;
      const static storage_format data_format = DataSource::data_format;

      /**
      @brief Constructor

      @param fn (optional) File name to load
      */
      data_source_merged()
         : i_src_{ 0 }
      {}

      size_t count() const
      {
         return data_src_.size();
      }

      void add(DataSource ds)
      {
         auto dim = base_type::dimensions();
         if (!data_src_.empty())
         {
            // some sanity checking
            bool consistent =
               (ds.dimensions().channels == data_src_[0].dimensions().channels) &&
               (ds.scalling_factor() == data_src_[0].scalling_factor()) &&
               (ds.reference_value() == data_src_[0].reference_value());
            if (!consistent)
               throw std::runtime_error{ "Inconsistent data sources: cannot be merged" };
            dim = data_dimensions{
               dim.channel_offset
             , dim.channels
             , dim.sample_offset
             , dim.samples + ds.dimensions().samples
             , dim.format
             , dim.sampling_rate };
         }
         else
         {
            dim = ds.dimensions();
         }

         // current position
         auto pos = base_type::position();

         data_src_.emplace_back(std::move(ds));
         base_type::_reset(dim, data_src_.front().reference_value(), data_src_.front().scalling_factor());

         if (pos != static_cast<size_t>(-1))
         {
            // return to the old position
            base_type::position(pos);
         }
      }

      /**
      @brief Returns a string identifier for a channel.
      @pre A file has to be loaded (see @ref load_file)

      @param is Stream number
      @param n Channel number

      @return Channel identification
      */
      virtual std::string channel_identification(size_t n) const
      {
         assert(!data_src_.empty());
         return data_src_.front().channel_identification(n);
      }

      /**
      @brief Reads data from an analog channel
      @pre A file has to be loaded (see @ref load_file)

      @tparam T data type
      @param ichan Channel number
      @param offset Data sample offset
      @param n Number of samples to read
      @param out Output buffer
      */
      virtual size_t read_channel(size_t ichan, size_t offset, size_t n, data_type* out)
      {
         assert(!data_src_.empty());

         throw std::runtime_error{ "Not supported" };
      }

      /**
      @brief Extracts data from the analog stream
      @pre A file has to be loaded (see @ref load_file)

      Data returned is in the 'row-major' format (@ref sec_data_input_format), i.e.,
      values for each channel for one sample are returned in sequence.

      @param offset Sample offset
      @param n Number of samples to read (output buffer has to have space for n*[number of channels] samples.
      @param out Output buffer
      */
      virtual size_t read(size_t n, data_type* out)
      {
         assert(!data_src_.empty());

         size_t off = 0;
         // read from each source sequentially
         for (; i_src_ < data_src_.size(); ++i_src_)
         {
            size_t nn = std::min(n - off, data_src_[i_src_].dimensions().samples - data_src_[i_src_].position());
            if (nn > 0)
            {
               nn = data_src_[i_src_].read(nn, out + off);
               off += nn;
            }
            if (off == n)
               break;
         }
         base_type::position(base_type::position() + off);

         return off;
      }
   };

}

///@}

#endif /* DATA_SOURCE_MERGED_HPP */
