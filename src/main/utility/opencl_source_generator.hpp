#ifndef OPENCL_SOURCE_GENERATOR_HPP
#define OPENCL_SOURCE_GENERATOR_HPP

#include <sstream>
#include "opencl_type_name.hpp"

class opencl_source_generator
{
   std::stringstream src_;
   int ident_;
   int par_;
   bool first_param_;

public:
   opencl_source_generator();

   std::string str() const;
   std::stringstream& src();
   const std::stringstream& src() const;
   opencl_source_generator& new_line();
   opencl_source_generator& kernel(const std::string& nm, bool dp);
   opencl_source_generator& open(const std::string& delim);
   opencl_source_generator& close(const std::string& delim);

   template<typename T>
   opencl_source_generator& vector_width_qualifier(size_t vw)
   {
      // add kernel attribute specifying the main vector width
      // src_ << "__attribute__((vec_type_hint(" << opencl_type_name<T>(vw) << ")))" << std::endl;
      return *this;
   }

   template<typename R>
   opencl_source_generator& function(const std::string& nm)
   {
      src_ << opencl_type_name<R>() << nm;
      first_param_ = true;
      return *this;
   }

   template<typename T>
   opencl_source_generator& parameter(const std::string& name, size_t vw = 1)
   {
      if (!first_param_)
         src_ << ", ";
      else
         first_param_ = false;
      if (std::is_pointer<T>::value)
         src_ << "__global ";
      src_ << opencl_type_name<T>(vw) << " " << name;
      return *this;
   }
};

template<typename T>
opencl_source_generator& operator << (opencl_source_generator& src, T&& t)
{
   src.src() << t;
   return src;
}

#endif /* OPENCL_SOURCE_GENERATOR_HPP */
