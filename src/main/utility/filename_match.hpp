#ifndef FILENAME_MATCH_HPP
#define FILENAME_MATCH_HPP

#include <string>
#include <regex>
#include <boost/algorithm/string.hpp>

/**
 @internal

 @brief matches a file name to a given mask
 */
template<typename Char>
bool filename_match(std::basic_string<Char> wc, std::basic_string<Char> fn)
{
   // have to escape all characters that regex interprets as patterns
	boost::replace_all(wc, "\\", "\\\\");
	boost::replace_all(wc, "^", "\\^");
	boost::replace_all(wc, ".", "\\.");
	boost::replace_all(wc, "$", "\\$");
	boost::replace_all(wc, "|", "\\|");
	boost::replace_all(wc, "(", "\\(");
	boost::replace_all(wc, ")", "\\)");
	boost::replace_all(wc, "[", "\\[");
	boost::replace_all(wc, "]", "\\]");
	boost::replace_all(wc, "*", "\\*");
	boost::replace_all(wc, "+", "\\+");
	boost::replace_all(wc, "?", "\\?");
	boost::replace_all(wc, "/", "\\/");

	// Convert chars '*?' back to their regex equivalents
	boost::replace_all(wc, "\\?", ".");
	boost::replace_all(wc, "\\*", ".*");

	return std::regex_match(fn, std::regex{ wc });
}

#endif /* FILENAME_MATCH_HPP */
