#ifndef OPENCL_LOGGING_HPP
#define OPENCL_LOGGING_HPP

#include <string>

void opencl_log_kernel(const std::string& kern);

#endif /* OPENCL_LOGGING_HPP */
