#include "opencl_logging.hpp"
#include <iostream>
#include <boost/serialization/singleton.hpp>
#include <boost/lexical_cast.hpp>

struct environment : public boost::serialization::singleton<environment>
{
   bool show_kernels;
   environment()
   {
      show_kernels = false;
      const char* val = std::getenv("SHOW_KERNELS");
      if (val && boost::lexical_cast<int>(val) > 0)
         show_kernels = true;
   }
};

void opencl_log_kernel(const std::string& kern)
{
   if (environment::get_const_instance().show_kernels)
   {
      std::cerr << std::endl << kern;
   }
}
