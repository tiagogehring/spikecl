#ifndef SHORT_TYPE_NAME_HPP
#define SHORT_TYPE_NAME_HPP

#include <type_traits>
#include "const_string.hpp"
#include <boost/config.hpp>

template<typename T>
constexpr const_string type_abbreviation()
{
	return (std::is_same<T, double>::value ?
		const_string{ "d" } : (std::is_same<T, float>::value ?
		const_string{ "f" } : (std::is_same<T, short>::value ?
		const_string{ "i16" } : (std::is_same<T, unsigned short>::value ?
		const_string{ "u16" } : (std::is_same<T, int>::value ?
		const_string{ "i32" } : (std::is_same<T, unsigned int>::value ?
		const_string{ "u32" } : const_string{ "unk" }
		)))))
		);
}
#define TYPE_ABBREVIATION(X) (type_abbreviation<X>())

#endif /* SHORT_TYPE_NAME_HPP */
