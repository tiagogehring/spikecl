#ifndef TYPE_TRAITS_HPP
#define TYPE_TRAITS_HPP

#include <type_traits>
#include <boost/config.hpp>

#if !defined(BOOST_NO_CXX11_CONSTEXPR)
/// @internal Helper to constraint templates
#define REQUIRES(...) typename std::enable_if<(__VA_ARGS__), int>::type = 0

template<typename T, typename ... Args>
constexpr bool is_constructible_()
{
	return std::is_constructible<T, Args...>::value;
}
#else
// MS VC++ 2013 does not understand constexpr so have to work around this
#define REQUIRES(X, ...) int UNUSED = 0
#define DUMMY_REQUIRES

#endif

#endif /* TYPE_TRAITS_HPP */
