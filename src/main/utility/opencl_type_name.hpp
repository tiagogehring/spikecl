#ifndef OPENCL_HELPERS_HPP
#define OPENCL_HELPERS_HPP

#include <string>
#include <cassert>
#include <boost/compute/type_traits/type_name.hpp>
#include <boost/lexical_cast.hpp>

template<typename T>
struct opencl_type_name_impl
{
   static std::string type_name(size_t w = 1)
   {
      assert(w > 0);
      if (w == 1)
         return boost::compute::type_name<T>();
      else
         return boost::compute::type_name<T>() + boost::lexical_cast<std::string>(w);
   }
};

template<typename T>
struct opencl_type_name_impl<T*>
{
   static std::string type_name(size_t w = 1)
   {
      return opencl_type_name_impl<T>::type_name(w) + "*";
   }
};

template<typename T>
std::string opencl_type_name(size_t w = 1)
{
   return opencl_type_name_impl<T>::type_name(w);
};


#endif /* OPENCL_HELPERS_HPP */
