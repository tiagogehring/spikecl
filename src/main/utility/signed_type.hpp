#ifndef SIGNED_TYPE_HPP
#define SIGNED_TYPE_HPP

#include <type_traits>

/// @internal Helper object to compute the unsigned type of our input
///@{
template<typename T, bool>
struct signed_data_type_impl;

template<typename T>
struct signed_data_type_impl<T, true>
   : public std::make_signed<T>
{};

template<typename T>
struct signed_data_type_impl<T, false>
{
   using type = T;
};

template<typename T>
struct signed_data_type
   : public signed_data_type_impl<T, std::is_integral<T>::value>
{};

///@}
#endif /* SIGNED_TYPE_HPP */
