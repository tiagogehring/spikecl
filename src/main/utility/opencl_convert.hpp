#ifndef OPENCL_CONVERT_HPP
#define OPENCL_CONVERT_HPP

#include "opencl_type_name.hpp"
#include <string>
#include <sstream>

template<typename T, typename U>
std::string opencl_convert(size_t vw)
{
   std::stringstream ret;
   if (vw > 1)
      ret << "convert_" << opencl_type_name<U>(vw) << "(";
   else
      ret << "(" << opencl_type_name<U>(vw) << ")(";

   return ret.str();
}
#endif /* OPENCL_CONVERT_HPP */
