#ifndef OPENCL_CONSTANT_HPP
#define OPENCL_CONSTANT_HPP

#include "opencl_type_name.hpp"
#include <string>
#include <sstream>

template<typename T, typename U>
std::string opencl_constant(size_t vw, U&& val)
{
   std::stringstream ret;
   ret << std::setprecision(15);
   if (vw > 0)
   {
      ret << "(" << opencl_type_name<T>(vw) << ")(" << val;
      for (int i = 1; i < vw; ++i)
         ret << "," << val;
      ret << ")";
   }
   else
      ret << val;
   return ret.str();
}

#endif /* OPENCL_CONSTANT_HPP */
