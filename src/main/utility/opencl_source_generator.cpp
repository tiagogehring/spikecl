#include "opencl_source_generator.hpp"

opencl_source_generator::opencl_source_generator()
   : ident_{0}
   , par_{0}
   , first_param_{true}
{}

std::string opencl_source_generator::str() const
{
   return src_.str();
}

std::stringstream& opencl_source_generator::src()
{
   return src_;
}

const std::stringstream& opencl_source_generator::src() const
{
   return src_;
}

opencl_source_generator& opencl_source_generator::new_line()
{
   src_ << std::endl;
   for (int i = 0; i < ident_; ++i)
      src_ << "  ";
   return *this;
}

opencl_source_generator& opencl_source_generator::kernel(const std::string& nm, bool dp)
{
   if (dp)
      src_ << "#pragma OPENCL EXTENSION cl_khr_fp64 : enable" << std::endl;

   src_ << "__kernel void " << nm;
   first_param_ = true;
   return *this;
}

opencl_source_generator& opencl_source_generator::open(const std::string& delim)
{
   if (delim == "{")
   {
      new_line();
      src_ << "{";
      ++ident_;
   }
   else if (delim == "(")
   {
      src_ << "(";
      ++par_;
   }
   else
   {
      assert(0 && "invalid delimiter");
   }
   return *this;
}

opencl_source_generator& opencl_source_generator::close(const std::string& delim)
{
   if (delim == "}")
   {
      --ident_;
      assert(ident_ >= 0 && "unbalanced brackets!");
      new_line();
      src_ << "}";
   }
   else if (delim == ")")
   {
      --par_;
      assert(par_ >= 0 && "unbalanced parenthesis");
      src_ << ")";
   }
   return *this;
}

