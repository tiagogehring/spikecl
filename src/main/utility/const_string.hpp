#ifndef CONST_STRING_HPP
#define CONST_STRING_HPP

#include <boost/config.hpp>

class const_string
{
   const char* str_;
   const size_t size_;
   const const_string* const head_;

public:
   template<size_t N>
   constexpr const_string(const char (&arr)[N], const const_string* head = nullptr)
      : str_{arr}
      , size_{N}
      , head_{head}
   {
      static_assert( N >= 1, "not a string literal" );
   }

   constexpr const_string(const char* ptr, size_t sz, const const_string* head = nullptr)
      : str_{ptr}
      , size_{sz >= 1 ? sz : throw std::logic_error{"not a string literal"}}
      , head_{head}
   {
   }

   constexpr operator const char* () const
   {
      return str_;
   }

   constexpr char operator[](size_t i) const
   {
      return (head_ != nullptr ?  char_from_head(i, head_->size()) :
                                 (i < size_ ?  str_[i] : '\0'));
   }

   constexpr size_t size() const
   {
     return size_ + (head_ != nullptr ? head_->size() : 0);
   }

   template<size_t N>
   constexpr const_string operator+ (const char(&arr)[N]) const
   {
      return const_string(arr, this);
   };

   constexpr const_string operator+ (const const_string& tail) const
   {
      return const_string{tail.str_, tail.size_, this};
   };

   template <typename FwdIter>
   void copy_to(FwdIter beg, FwdIter end) const
   {
      _copy_to(beg, end);
   }

   void copy_to(char* beg, char* end) const
   {
      _copy_to(beg, end);
   }

private:
   constexpr char char_from_head(size_t i, size_t hd_size) const
   {
      return (i < hd_size ? (*head_)[i] : (i < (hd_size + size_) ? str_[i - hd_size] : '\0'));
   };

   template <typename FwdIter>
   void _copy_to(FwdIter& beg, FwdIter end) const
   {
      if (head_ != nullptr)
         head_->_copy_to(beg, end);
      for(size_t i = 0; (i < size_) && (beg != end); ++i, ++beg)
         *beg = str_[i];
   }

   void _copy_to(char*& beg, char* end) const
   {
      if (head_ != nullptr )
         head_->_copy_to(beg, end);
      std::size_t sz_to_cpy = (end - beg < (int)size_ ? end - beg : (int)size_);
      std::memcpy(beg, str_, sz_to_cpy);
      beg += sz_to_cpy;
   }
};

/*
std::string to_string(const const_string& str)
{
   std::string res(str.size(), ' ');
   str.copy_to(res.begin(), res.end());
   return res;
}*/

inline constexpr const_string operator"" _c(const char* str, size_t sz)
{
   return const_string{str, sz};
}

constexpr int compare_impl(const const_string& s1, const const_string& s2, int i)
{
   return (i >= static_cast<int>(s1.size()) && i >= static_cast<int>(s2.size()) ? 0 :
             (i >= static_cast<int>(s1.size()) ? -1 :
                 (i >= static_cast<int>(s2.size()) ? 1 :
                    (s1[i] < s2[i] ? -1 :
                       (s2[i] < s1[i] ? 1 : compare_impl(s1, s2, i + 1)
          )))));
};

constexpr int compare(const const_string& s1, const const_string& s2)
{
   return compare_impl(s1, s2, 0);
}

constexpr bool operator == (const const_string& s1, const const_string& s2)
{
   return compare(s1, s2) == 0;
}

constexpr bool operator != (const const_string& s1, const const_string& s2)
{
   return compare(s1, s2) != 0;
}

constexpr bool operator < (const const_string& s1, const const_string& s2)
{
   return compare(s1, s2) < 0;
}

constexpr bool operator <= (const const_string& s1, const const_string& s2)
{
   return compare(s1, s2) <= 0;
}

constexpr bool operator > (const const_string& s1, const const_string& s2)
{
   return compare(s1, s2) < 0;
}

constexpr bool operator >= (const const_string& s1, const const_string& s2)
{
   return compare(s1, s2) >= 0;
}

#endif /* CONST_STRING_HPP */
