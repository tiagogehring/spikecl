#ifndef BIDIRECTIONAL_FILTER_HPP
#define BIDIRECTIONAL_FILTER_HPP

/// @addtogroup DSP
///@{

/**
 * @file   bidirectional_filter.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Definition and implementation of the bidirectional_filter template class
 */

#include "filter.hpp"
#include "filtfilt_initial_state.hpp"
#include "utility/opencl_constant.hpp"
#include <vector>
#include <tuple>

#include <boost/compute/core.hpp>
#include <boost/compute/buffer.hpp>
#include <boost/compute/utility/program_cache.hpp>
#include "utility/opencl_logging.hpp"

namespace compute = boost::compute;

namespace dsp
{

/**
 @brief Filter that performs first a forward and then a backward pass on the data.

 This filter can be used to perform an zero-phase filtering of a signal
 even when the filter kernel is not casual and/or zero-phase

 @tparam Tin Signal data type
 @tparam Tout Output data type
 @tparam IS Initial state function object
 */
template< typename Tin
        , typename Tout
        , template <typename, typename> class IS = filtfilt_initial_state
> class bidirectional_filter
{
public:
   using input_type = Tin;
   using output_type = Tout;
   using input_block_type = multichannel::data_block<Tin>;
   using output_block_type = multichannel::data_block<Tout>;
   using data_dimensions = data_source::data_dimensions;
   using kernel_type = filter_kernel<Tin, Tout>;

private:
   // stores information about a data chunk - we need this for the backpass filter step
   using data_chunk = std::pair<output_block_type*, output_block_type*>;
   // pointers to processed chunks (for applying the filter in the BWD direction later)
   std::vector<data_chunk> chunks_;
   // filters used for the forward/backward pass
   filter<Tin, Tout> fwd_filter_;
   filter<Tout, Tout> bwd_filter_;
   // OpenCL kernels used to compute the lead in/lead out data
   compute::kernel lead_in_kernel_;
   compute::kernel lead_out_kernel_;
   // lead in/lead out data (additional points not provided as input to the filter)
   input_block_type pad_in_;
   output_block_type pad_filt_;
   output_block_type pad_out_;
   // types that computes the initial state in the forward/backward direction
   using forward_initial_state_type = IS<Tin, Tout>;
   using backward_initial_state_type = IS<Tout, Tout>;
   // instantiate them too
   forward_initial_state_type is_fwd_;
   backward_initial_state_type is_bwd_;
   size_t nci_;
   bool reset_;

public:
   /**
    @brief Constructor

    @param q OpenCL command queue
    @param kern Filter kernel
    @param name Filter name (for the generated OpenCL kernel)
    @param nchan Number of data channels
    */
   template<typename Kern>
   bidirectional_filter(compute::context ctx, Kern&& kern)
      : fwd_filter_{ctx, std::forward<Kern>(kern)}
      , bwd_filter_{ctx, filter_kernel<Tout, Tout>{
              fwd_filter_.kernel().coefficients
            , fwd_filter_.kernel().vector_width
            , static_cast<int>(filter_kernel_options::reverse) // no other options supported in the back-pass
        }}
      , is_fwd_{ctx, fwd_filter_.kernel()}
      , is_bwd_{ctx, bwd_filter_.kernel()}
      , nci_{(size_t)-1}
      , reset_{true}
   {}

   /**
    @brief Reset filter state
    */
   void reset()
   {
      reset_ = true;
      // reset filters
      fwd_filter_.reset();
      bwd_filter_.reset();
      // reset initial state of filters
      is_fwd_.reset();
      is_bwd_.reset();
      chunks_.clear();
   }

   ///@{
   /**
    @brief Applies filter to data chunk

    @tparam ContIn Type of input data container
    @tparam ContOut Type of output data container
    @param in Input data container
    @param ns Number of samples
    @param fwd Intermediate data container (after forward pass)
    @param out Output data container (might be the same as the input so that only 2 buffer are required)
    @param last True if this is the last data chunk (so that the backward pass will be applied afterwards)

    @return OpenCL event with status of the kernel invocation
    */
   compute::event operator()(input_block_type& x, output_block_type& fwd, output_block_type& y, bool all, compute::command_queue& q)
   {
      auto dim = x.dimensions();
      auto ev = _filter_impl(x, dim.channel_offset, dim.channels, fwd, y, x.next() == nullptr, q);
      if (all && x.next() != nullptr)
      {
         // some sanity checks
         assert(fwd.next() != nullptr);
         assert(y.next() != nullptr);
         assert(fwd.next()->dimensions().channels == dim.channels);
         assert(y.next()->dimensions().channels == dim.channels);
         // the recursive call
         return (*this)(*x.next(), *fwd.next(), *y.next(), true, q);
      }
      return ev;
   }

   compute::event operator()(input_block_type& x, size_t co, size_t nc, output_block_type& fwd, output_block_type& y, bool all, compute::command_queue& q)
   {
      auto ev = _filter_impl(x, co, nc, fwd, y, x.next() == nullptr, q);
      if (all && x.next() != nullptr)
      {
         // sanity checks..
         assert(fwd.next() != nullptr);
         assert(y.next() != nullptr);
         assert(fwd.next()->dimensions().channels == nc);
         assert(y.next()->dimensions().channels == nc);
         return (*this)(*x.next(), co, nc, *fwd.next(), *y.next(), true, q);
      }
      return ev;
   }
   ///@}

private:
   // Actual filter implementation. For description of the arguments see operator() above
   compute::event _filter_impl(input_block_type& in, size_t co, size_t nc, output_block_type& fwd, output_block_type& out, bool last, compute::command_queue& q)
   {
      const auto order = fwd_filter_.kernel().order;
      const auto ns = in.dimensions().samples;
      const auto nci = in.parent_dimensions().channels;
      const size_t vw = fwd_filter_.kernel().vector_width;

      if (reset_)
      {
         if (nci_ != nci)
         {
            std::string name;
            compute::program prog;
            std::tie(prog, name) = _boundary_values_prog(q.get_context(), nci, in.dimensions().format, true);
            lead_in_kernel_ = compute::kernel{prog, name};
            std::tie(prog, name) = _boundary_values_prog(q.get_context(), nci, in.dimensions().format, false);
            lead_out_kernel_ = compute::kernel{prog, name};

            // lead-in/out buffers
            data_dimensions dim{0, nci, 0, 6*order, in.dimensions().format, in.dimensions().sampling_rate};
            pad_in_ = input_block_type{q.get_context(), dim};
            auto dim_blk = dim;
            dim_blk.channel_offset = co;
            dim_blk.channels = nc;
            pad_out_ = output_block_type{q.get_context(), dim_blk, dim};
            pad_filt_ = output_block_type{q.get_context(), dim_blk, dim};
         }
         reset_ = false;
      }

      if (chunks_.empty())
      {
         // first chunk, initialize leading buffer
         if (ns <= 6*order)
            throw std::runtime_error{"Insufficient data"};

         // leading data
         lead_in_kernel_.set_arg(0, in.buffer().get_buffer());
         lead_in_kernel_.set_arg<cl_ulong>(1, ns);
         lead_in_kernel_.set_arg(2, pad_in_.buffer().get_buffer());
         q.enqueue_1d_range_kernel(lead_in_kernel_, co/vw, nc/vw, 1);

         // set initial filter state
         fwd_filter_.reset_state(pad_in_, co, nc, is_fwd_, q);

         // filter leading data
         fwd_filter_(pad_in_, co, nc, pad_out_, false, q);
      }

      // process current data chunk (forward) - use "low-level" implementation
      compute::event ev = fwd_filter_(in, co, nc, fwd, false, q);

      // remember this data chunk -> we will need it for the backward pass
      chunks_.push_back(std::make_pair(&fwd, &out));

      if (last)
      {
         // trailing data
         lead_out_kernel_.set_arg(0, in.buffer().get_buffer());
         lead_out_kernel_.set_arg<cl_ulong>(1, ns);
         lead_out_kernel_.set_arg(2, pad_in_.buffer().get_buffer());

         // channel offset has to be 0 here
         q.enqueue_1d_range_kernel(lead_out_kernel_, co/vw, nc/vw, 1);

         fwd_filter_(pad_in_, co, nc, pad_out_, false, q);

        /*
         * Backward pass
         */
         // note again that the backward filter uses as input the intermediate buffer
         // and so not scalling of shifting of input channels is necessary
         bwd_filter_.reset_state(pad_out_, co, nc, is_bwd_, q);
         bwd_filter_(pad_out_, co, nc, pad_filt_, false, q);

         // backward pass of the rest of the data
         for(int i = static_cast<int>(chunks_.size()) - 1; i >= 0; --i) // *don't* use an unsigned index variable here...
         {
            ev = bwd_filter_(*chunks_[i].first, co, nc, *chunks_[i].second, false, q);
         }

         // release reference to the buffers -> we don't need them anymore
         chunks_.clear();
      }

      return ev;
   }

   // Generates the OpenCL code for computing the boundary (lead-in/out) values
   std::pair<compute::program, std::string> _boundary_values_prog(compute::context ctx, size_t nci, data_source::storage_format fmt, bool lead_in) const
   {
      const size_t order = fwd_filter_.kernel().order;

      std::stringstream name;
      name << "filter_" << (lead_in ? "leadin_" : "leadout_") << nci << "_" << fwd_filter_.kernel().vector_width
           << "_" << TYPE_ABBREVIATION(Tin) << "_" << TYPE_ABBREVIATION(Tout)
           << "_" << fwd_filter_.kernel().coefficients.hash();

      auto cache = compute::program_cache::get_global_cache(ctx);
      auto prog = cache->get(name.str(), "");
      if (prog)
         return std::make_pair(*prog, name.str());

      const size_t vw = fwd_filter_.kernel().vector_width;
      // if the vectorize output option is set we assume that the input data is sequential
      opencl_source_generator src;
      src.vector_width_qualifier<Tin>(vw);
      src.kernel(name.str(), std::is_same<Tin, double>::value || std::is_same<Tout, double>::value).open("(");
      src.parameter<Tin*>("x", vw);
      src.parameter<cl_ulong>("ns");
      src.parameter<Tin*>("out", vw);
      src.close(")");
      src.open("{");
      src.new_line() << "int i = get_global_id(0);";
      src.new_line() << "int co = get_global_offset(0);";
      src.new_line() << "int j;";
      size_t l = 6*order;
      std::string two = opencl_constant<Tin>(vw, 2.);

      if (lead_in)
      {
         src.new_line() << "for(j = " << l << "; j > 0; --j)";
         src.open("{");
         if (fmt == data_source::storage_format::row_major)
            src.new_line() << "out[(" << l << " - j)*" << nci/vw << " + i] = " << two << "*x[i] - x[j*" << nci/vw << " + i];";
         else
            src.new_line() << "out[(i + 1)*" << l << " - j] = " << two << "*x[ns*i] - x[ns*i + j];";
      }
      else
      {
         src.new_line() << "for(j = 1; j <= " << l << "; ++j)";
         src.open("{");
         if (fmt == data_source::storage_format::row_major)
         {
            src.new_line() << "out[(j - 1)*" << nci/vw << " + i] = " << two << "*x[(ns - 1)*" << nci/vw << " + i] - x[(ns - j - 1)*" << nci/vw << " + i];";
         }
         else
            src.new_line() << "out[i*" << l << " + j - 1] = " << two << "*x[ns*(i + 1) - 1] - x[ns*(i + 1) - j - 1];";
      }
      src.close("}");
      src.close("}");

      opencl_log_kernel(src.str());
      compute::program prgm;
      try
      {
         prgm = compute::program::create_with_source(src.str(), ctx);
         prgm.build();
         cache->insert(name.str(), "", prgm);
      }
      catch(std::exception &)
      {
         // program failed to compile, print out the build log
         std::cout << prgm.build_log() << std::endl;
      }

      return std::make_pair(prgm, name.str());
   }
};

///@{
/**
 @brief Tests if type is a bidirectional_filter

 @tparam T type
 */
template<typename T>
struct is_bidirectional_filter : std::false_type {};

template<typename In, typename Out, template<typename, typename> class IS>
struct is_bidirectional_filter<bidirectional_filter<In, Out, IS>> : std::true_type {};
///@}

/**
 @internal bidirectional_filter is also a filter
 */
template<typename In, typename Out, template<typename, typename> class IS>
struct is_filter<bidirectional_filter<In, Out, IS>> : std::true_type {};

} // namespace dsp

///@}

#endif /* BIDIRECTIONAL_FILTER_HPP */
