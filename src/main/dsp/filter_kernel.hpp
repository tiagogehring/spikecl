#ifndef FILTER_KERNEL_HPP
#define FILTER_KERNEL_HPP

/// @defgroup DSP Digital signal processing classes and functions
///@{

/**
 * @file   filter_kernel.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Filter kernel coefficients, attributes, and OpenCL code generation functions
 */

#include <cstdio>
#include <string>
#include <sstream>
#include <boost/compute/core.hpp>
#include <boost/optional.hpp>
#include <boost/compute/utility/program_cache.hpp>
#include "filter_kernel_coefficients.hpp"
#include "data_source/data_dimensions.hpp"
#include "utility/type_traits.hpp"
#include "utility/opencl_source_generator.hpp"
#include "utility/type_abbreviation.hpp"
#include "utility/opencl_constant.hpp"
#include "utility/opencl_convert.hpp"
#include "utility/opencl_logging.hpp"
#include "utility/signed_type.hpp"

namespace compute = boost::compute;

namespace dsp
{

/**
 @brief Options for the filter kernel generation
 */
enum class filter_kernel_options : int
{
   none = 0x0,              //!< No option set
   reverse = 0x01,          //!< filter from back to front instead of the other way around
   unroll = 0x02,           //!< not supported yet
   transpose_output = 0x04, //!< transforms from a sequential input (one channel after another) to interlaced channels in the output (one sample after another)
   scale_input = 0x08      //!< pre-scale and shift input values (e.g. for converting from AD to float/double values)
};

/**
 * @brief Combines two kernel options flags
 */
inline filter_kernel_options operator | (filter_kernel_options x, filter_kernel_options y)
{
   return static_cast<filter_kernel_options>(static_cast<int>(x) | static_cast<int>(y));
}


/**
 @brief Filter kernel

 Defines a filter kernel of order N. 2 sets of 2*N+1 coefficients of the difference equation
 have to be provided to define the filter.

 @tparam Tin Input data type of the signal
 @tparam Tout Output data type

 */
template<typename Tin, typename Tout>
class filter_kernel
{
public:
   // filter coefficients
   filter_kernel_coefficients coefficients;
   // Filter order
   size_t order;
   // If true filter values are sorted per sample, otherwise per channel (@ref sec_data_input_format)
   data_source::storage_format data_format;
   // SIMD vector width
   size_t vector_width;
   // Input offset and scalling values for converting input to output data types (e.g. integer values from an AD to floating-point voltages)
   Tin input_offset;
   Tout input_scalling;

private:
   // one or more options (see filter_kernel_options)
   int opts_;

public:
   /**
    @brief Constructor

    @param bb Coefficients of the input values
    @param aa Coefficients of the previous values (recursive coefficients, for IIR filters)
    @param opts One or more options for the filter code generation (@ref filter_kernel_options)
    */
   filter_kernel(filter_kernel_coefficients coeff, size_t vw = 1, int opts = 0, Tin xoff = (Tin)0., Tout xfac = (Tout)1.)
      : coefficients(coeff)
      , order{coeff.order}
      , vector_width{vw}
      , input_offset{xoff}
      , input_scalling{xfac}
      , opts_{opts}
   {}

   /**
    @brief Checks if an option value is setg

    @param opt Option (@see filter_kernel_options)

    @return True if option value is set
    */
   bool option_set(filter_kernel_options opt) const
   {
      return ((opts_ & static_cast<int>(opt)) != 0);
   }

   /**
    @brief Generates the OpenCL kernel source for the filter function.

    The generated filter function, f(n, x, y, z), takes 4 input parameters:
      -# n: number of samples
      -# x: input array
      -# y: output array
      -# z: internal state of the filter (same number of values as the Order)
    Note that the the input vector may consist of multiple channels concataned together
    (i.e. the x values contain n samples for the 1st channel, followed by n for the 2nd
    and so on). Multiple channels can in this form be processed in parallel. The y and z
    arrays follow the same convention.

    @param ctx OpenCL context
    @param name The name of the function (as seen by the OpenCL runtime)
    @param ncb Total number of channels in the input data

    @return String containing the source for the kernel
    */
   std::pair<compute::program, std::string> program(compute::context ctx, size_t nci, bool partial, size_t ncb, data_source::storage_format fmt) const
   {
      using signed_type = typename signed_data_type<Tin>::type;

      // create a unique name
      std::stringstream name;
      name << "filter_" << ncb << "_" << vector_width
           << (option_set(filter_kernel_options::reverse) ? "_rev" : "")
           << (partial ? "_p" : "")
           << "_" << TYPE_ABBREVIATION(Tin) << "_" << TYPE_ABBREVIATION(Tout)
           << "_" << coefficients.hash();
      int nexp;
      if (option_set(filter_kernel_options::scale_input))
         name << "_" << static_cast<int>(input_offset) << "_" << static_cast<int>(frexp(input_scalling, &nexp)*1000);

      // see if program already cached
      auto cache = compute::program_cache::get_global_cache(ctx);
      auto prog = cache->get(name.str(), "");
      //if (prog)
       //  return std::make_pair(*prog, name.str());

      // if the transpose_output option is set we assume that the input data is sequential
      const size_t vw_in = option_set(filter_kernel_options::transpose_output) ? 1 : vector_width;
      opencl_source_generator src;
      src.vector_width_qualifier<Tout>(vw_in);
      src.kernel(name.str(), std::is_same<Tin, double>::value || std::is_same<Tout, double>::value).open("(");
      // arguments
      src.parameter<Tin*>("x", vw_in);
      src.parameter<cl_ulong>("n");
      src.parameter<Tout*>("y", vw_in);
      src.parameter<Tout*>("z", vw_in);
      src.close(")").open("{");
      // treat first values differently
      src.new_line() << "long i = (long)get_global_id(0);";
      src.new_line() << "long co = (long)get_global_offset(0);";
      // the z vector has 2*Order + 1 values per channel
      src.new_line() << "z[" << 2*order  << "*" << nci/vw_in << " + i] = 0.;";
      src.new_line() << "ulong j;";

      if (option_set(filter_kernel_options::reverse))
      {
         src.new_line() << "ulong k;";
         src.new_line() << "for(k = 0; k < n; ++k)";
         src.open("{");
         // now, I tried to make this the "smart" way by doing a backwards
         // iteration but ran into troubles with the mixing of signed/unsigned stuff
         // OpenCL seems to change the variable types automatically so better not to
         // try to do a reverse iteration in this case
         src.new_line() << "j = n - k - 1;";
      }
      else
      {
         src.new_line() << "for(j = 0; j < n; ++j)";
         src.open("{");
      }
      std::stringstream outidx, inidx;
      if (option_set(filter_kernel_options::transpose_output))
      {
         // mixed: sequential channels input, sequential samples out
         if (!partial)
            inidx << "i*n + j";
         else
            inidx << "(i - co)*n + j";
         outidx << "j*" << ncb/vw_in << " + i - co";
      }
      else
      {
         if (fmt == data_source::storage_format::row_major)
         {
            // sequential samples
            if (partial)
               inidx << "j*" << nci/vw_in << " + i";
            else
               inidx << "j*" << nci/vw_in << " + (i - co)";
            outidx << "j*" << ncb/vw_in << " + i - co";
         }
         else
         {
            // sequential channels
            if (!partial)
               inidx << "(i - co)*n + j";
            else
               inidx << "i*n + j";
            outidx << "(i - co)*n + j";
         }
      }

      // x value: scaled or not
      std::stringstream xval;
      if (option_set(filter_kernel_options::scale_input))
      {
         xval << opencl_convert<Tin, Tout>(vw_in)
              << opencl_convert<Tin, signed_type>(vw_in) << "x[" << inidx.str() << "] - "
              << "(" << opencl_type_name<Tin>() << ")" << input_offset << "))*"
              << "(" << opencl_type_name<Tout>() << ")" << input_scalling;
      }
      else
      {
         xval << "x[" << inidx.str() << "]";
      }

      src.new_line() << std::setprecision(15) << "y[" << outidx.str() << "] = "
         << "(" << opencl_type_name<Tout>() << ")" << coefficients.b[0] << "*" << xval.str() << " + z[i];" ;
      for (int k = 1; k <= 2*order; ++k)
      {
         src.new_line() << "z[" << k - 1 << "*" << nci/vw_in << " + i] = "
            << "(" << opencl_type_name<Tout>() << ")" << coefficients.b[k] << "*" << xval.str()
            << " + z[" << k << "*" << nci/vw_in << " + i] - (" << opencl_type_name<Tout>() << ")" << coefficients.a[k] << "*y[" << outidx.str() << "];";
      }
      src.close("}");
      src.close("}");

      opencl_log_kernel(src.str());

      compute::program prgm;
      try
      {
         prgm = compute::program::create_with_source(src.str(), ctx);
         prgm.build();
         cache->insert(name.str(), "", prgm);
      }
      catch(std::exception &)
      {
         // program failed to compile, print out the build log
         std::cout << prgm.build_log() << std::endl;
      }

      return std::make_pair(prgm, name.str());
   }
};

} // namespace dsp

#endif /* FILTER_KERNEL_HPP */
