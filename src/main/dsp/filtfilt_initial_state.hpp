#ifndef FILTFILT_INITIAL_STATE_HPP
#define FILTFILT_INITIAL_STATE_HPP

/// @addtogroup DSP
///@{

/**
 * @file   filtfilt_initial_state.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Computation of filter initial state as implemented by Matlab's filtfilt function
 */

#include <string>

#include <boost/compute/core.hpp>
#include <boost/compute/buffer.hpp>
#include <boost/compute/container/vector.hpp>
#include <boost/compute/utility/program_cache.hpp>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/operation.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include "utility/opencl_constant.hpp"
#include "utility/opencl_convert.hpp"
#include "utility/opencl_logging.hpp"
#include "utility/signed_type.hpp"

namespace ublas = boost::numeric::ublas;
namespace compute = boost::compute;

namespace dsp
{

/**
 @brief Computes filter initial state in the same way as MATLAB's filtfilt function

 This class is to be used as a template parameter for the bidirectional_filter class (@ref bidirectional_filter).

 The filter initial state is computed as to minimize transients at the boundaries. For more information please
 refer to Matlab's filtfilt function documentation.

 @tparam Tin Input data type (same as the filter function)
 @tparam Tout Output data type
 */
template<typename Tin, typename Tout>
class filtfilt_initial_state
{
   // Filter kernel definitions (coefficients and other parameters)
   filter_kernel<Tin, Tout> filt_kern_;
   // intermediate state vector
   ublas::vector<Tout> zi_blas_;
   // the vector in OpenCL memory space
   compute::vector<Tout> zi_;
   // OpenCL kernel (function)
   compute::kernel kernel_;
   // reset flag
   bool reset_;
   // total number of input channels
   size_t ncb_;

public:
   using input_block_type = multichannel::data_block<Tin>;
   using filter_state_type = compute::vector<Tout>;

   /**
    @brief Constructor

    @param q OpenCL command queue
    @param name Name to be given to the OpenCL kernel
    @param kern Filter kernel
    */
   filtfilt_initial_state(compute::context ctx, filter_kernel<Tin, Tout> kern)
      : filt_kern_{std::move(kern)}
      , zi_blas_(2*filt_kern_.order)
      , zi_(2*filt_kern_.order, ctx)
      , reset_{true}
      , ncb_{(size_t)-1}
   {
      using namespace std;

      // permutation matrix needed by the LU decomposition
      ublas::permutation_matrix<Tout> p{2*filt_kern_.order};
      // construct matrix
      ublas::matrix<Tout> A(2*filt_kern_.order, 2*filt_kern_.order);
      ublas::vector<double> x{2*filt_kern_.order};

      // matrix definition taken from Matlab
      A.clear();
      size_t i;
      for (i = 0; i < 2*filt_kern_.order; ++i)
      {
         A(i, 0) = static_cast<Tout>(filt_kern_.coefficients.a[i + 1]);
         if (i == 0)
            A(i, 0) += 1.;
         if (i < 2*filt_kern_.order - 1)
            A(i, i + 1) = -1.;
         if (i > 0)
            A(i, i) = 1.;
      }

      // right hand side -> use own result vector
      for(i = 0; i < 2*filt_kern_.order; ++i)
         zi_blas_[i] = static_cast<Tout>(filt_kern_.coefficients.b[i + 1] - filt_kern_.coefficients.b[0]*filt_kern_.coefficients.a[i + 1]);

      ublas::lu_factorize(A, p);
      ublas::lu_substitute(A, p, zi_blas_);
   }

   /**
    @brief Resets state on next invocation (changing possibly the number of input channels, for example)

    @return
    */
   void reset()
   {
      reset_ = true;
   }

   ///@{
   /**
    @brief Computes the initial state of the filter

    @param x Input data
    @param co Channel offset
    @param nc Number of channels
    @param z Output data (has to be of sufficient size)

    @return OpenCL event (can be used for asynchronous operations)
    */
   compute::event operator()(input_block_type& x, size_t co, size_t nc, filter_state_type& z, compute::command_queue& q)
   {
      return _initial_state(x, x.dimensions().channels, co, nc, z, q);
   }

   compute::event operator()(input_block_type& x, filter_state_type& z, compute::command_queue& q)
   {
      const auto& dim = x.dimensions();
      return _initial_state(x, x.dimensionks().channels, dim.channel_offset, dim.channels, z ,q);
   }
   ///@}

private:
   compute::event _initial_state(input_block_type& x, size_t ncb, size_t co, size_t nc, filter_state_type& z, compute::command_queue& q)
   {
      using namespace std;
      const auto ns = x.dimensions().samples;
      if (reset_)
      {
         if (ncb_ != ncb)
         {
            bool partial = nc < ncb; // multi-block if number of channels to process is smaller than the input
            auto prog = _initial_state_prog(q.get_context(), ncb, partial);
            kernel_ = compute::kernel{prog.first, prog.second};
            ncb_ = ncb;
            /// a bit of a hack this being here - it cannot be moved to the constructor since we don't have a command queue there, though
            compute::copy(begin(zi_blas_), end(zi_blas_), begin(zi_), q);
         }
         reset_ = false;
      }

      kernel_.set_arg(0, x.buffer().get_buffer());
      kernel_.set_arg<cl_ulong>(1, ns);
      kernel_.set_arg(2, zi_);
      kernel_.set_arg(3, z);
      // always run the initial state for all the data chunks
      const auto vw = filt_kern_.vector_width;
      return q.enqueue_1d_range_kernel(kernel_, co/vw, nc/vw, 1);
   }

   // generates the OpenCL program that computes the initial state
   std::pair<compute::program, std::string> _initial_state_prog(compute::context ctx, size_t ncb, bool partial) const
   {
      using signed_type = typename signed_data_type<Tin>::type;

      std::stringstream name;
      name << "filtfilt_is_" << ncb << (partial ? "_partial" : "")
           << (filt_kern_.option_set(filter_kernel_options::reverse) ? "_rev" : "")
           << "_" << TYPE_ABBREVIATION(Tin) << "_" << TYPE_ABBREVIATION(Tout)
           << "_" << filt_kern_.coefficients.hash();
      int nexp;
      if (filt_kern_.option_set(filter_kernel_options::scale_input))
         name << "_" << static_cast<int>(filt_kern_.input_offset) << "_" << static_cast<int>(frexp(filt_kern_.input_scalling, &nexp)*1000);

      auto cache = compute::program_cache::get_global_cache(ctx);
      auto prog = cache->get(name.str(), "");
      if (prog)
         return std::make_pair(*prog, name.str());

      const auto vw = filt_kern_.vector_width;

      // if the vectorize output option is set we assume that the input data is sequential
      opencl_source_generator src;
      src.vector_width_qualifier<Tout>(vw);
      src.kernel(name.str(), std::is_same<Tin, double>::value || std::is_same<Tout, double>::value).open("(");
      src.parameter<Tin*>("x", vw);
      src.parameter<cl_ulong>("ns");
      src.parameter<Tout*>("zi");
      src.parameter<Tout*>("z", vw);
      src.close(")");
      src.open("{");
      src.new_line() << "int i = get_global_id(0);";
      src.new_line() << "int co = get_global_offset(0);";
      src.new_line() << "int j;";
      src.new_line() << "for(j = 0; j < 2*" << filt_kern_.order << "; ++j)";
      src.open("{");
      size_t l = 2*filt_kern_.order + 1;
      std::stringstream x0;
      if (!filt_kern_.option_set(filter_kernel_options::reverse))
      {
         if (!partial)
            x0 << "x[i - co]";
         else
            x0 << "x[i]";
      }
      else
      {
         if (!partial)
            x0 << "x[(ns - 1)*" << ncb/vw << " + i - co]";
         else
            x0 << "x[(ns - 1)*" << ncb/vw << " + i]";
      }
      if (filt_kern_.option_set(filter_kernel_options::scale_input))
      {
         src.new_line() << "z[j*" << ncb/vw << " + i] = " << opencl_convert<Tin, Tout>(vw)
              << opencl_convert<Tin, signed_type>(vw) << x0.str() << " - "
              << "(" << opencl_type_name<Tin>() << ")" << filt_kern_.input_offset << "))*"
              << "(" << opencl_type_name<Tout>() << ")" << filt_kern_.input_scalling << "*zi[j];";
      }
      else
      {
         src.new_line() << "z[j*" << ncb/vw << " + i] = " << x0.str() << "*zi[j];";
      }
      src.close("}");
      src.new_line() << "z[" << l - 1 << "*" << ncb/vw << " + i] = " << opencl_constant<Tout>(vw, 0) << ";";
      src.close("}");

      opencl_log_kernel(src.str());
      compute::program prgm;

      try
      {
         prgm = compute::program::create_with_source(src.str(), ctx);
         prgm.build();
         cache->insert(name.str(), "", prgm);
      }
      catch(std::exception &)
      {
         // program failed to compile, print out the build log
         std::cout << prgm.build_log() << std::endl;
      }
      return std::make_pair(prgm, name.str());
   }
};

} // namespace dsp
///@}

#endif /* FILTFILT_INITIAL_STATE_HPP */
