#ifndef FILTER_KERNEL_COEFFICIENTS_HPP
#define FILTER_KERNEL_COEFFICIENTS_HPP

/// @addtogroup DSP
///@{

/**
 * @file   filter_kernel.hpp
 * @author Tiago Gehring
 * @date   January, 2015
 * @brief  Filter kernel coefficients
 */

#include <vector>
#include <functional>

namespace dsp
{

/**
 @brief Stores filter kernel coefficients of an IIR filter; provides some other utility functions
 */
class filter_kernel_coefficients
{
   // Hash value
   mutable size_t hash_;
public:
   /// Coefficients of the input values
   const std::vector<double> b;
   /// Recursive term (i.e. previous values) coefficient values
   const std::vector<double> a;
   // Filter order
   const size_t order;

   /**
    @brief Creates an empty list
    */
   filter_kernel_coefficients();

   /**
    @brief Constructor

    @param _b see #a
    @param _a see #b
    */
   filter_kernel_coefficients(std::vector<double> _b, std::vector<double> _a);

   /**
    @brief Move constructor

    @param
    */
   filter_kernel_coefficients(filter_kernel_coefficients&&);

   /**
    @brief Copy constructor
    */
   filter_kernel_coefficients(const filter_kernel_coefficients&);

   /**
    @return True if coefficients is empty, false if otherwise
    */
   bool empty() const;

   /**
    @return Hash value
    */
   size_t hash() const;
};

} // namespace dsp

namespace std {
   /// @internal
   template<>
   struct hash<dsp::filter_kernel_coefficients>
   {
      size_t operator()(const dsp::filter_kernel_coefficients& coeff) const {
         return coeff.hash();
      }
   };
}

///@}

#endif /* FILTER_KERNEL_COEFFICIENTS_HPP */
