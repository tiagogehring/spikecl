#ifndef FILTER_HPP
#define FILTER_HPP

/// @addtogroup DSP
///@{

/**
 * @file   filter.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Definition and implementation of the IIR filtering methods
 */

#include <iostream>
#include <limits>
#include <boost/compute/core.hpp>
#include <boost/compute/container/vector.hpp>
#include "filter_kernel.hpp"
#include "multichannel/data_block.hpp"
#include "utility/type_traits.hpp"

namespace compute = boost::compute;

namespace dsp
{

/**
 @brief Generic digital filter class

 Provides means to apply a given filter kernel to one or more data channels, which are processed in parallel via OpenCL

 @tparam Tin Data type of the samples
 @tparam Tout Output data type
 */
template<typename Tin, typename Tout>
class filter
{
public:
   using input_type = Tin;
   using output_type = Tout;
   using input_buffer_type = multichannel::data_block<Tin>;
   using output_buffer_type = multichannel::data_block<Tout>;
   using kernel_type = filter_kernel<Tin, Tout>;
   using filter_state_type = compute::vector<Tout>;

private:
   // OpenCL kernel for the filtering function
   compute::kernel kernel_;
   // Filtering kernel attributes
   kernel_type filt_kern_;
   // Internal state. Used to make it possible to apply the filter in blocks.
   public:
   filter_state_type z_;
   // total number of input channels
   size_t nci_;
   // reset flag
   bool reset_;

public:
   /**
    @brief Constructor

    @param q OpenCL command queue
    @param kern Filter kernel attributes
    @param name Name of the generated OpenCL kernel
    */
   template< typename Kern
           , REQUIRES( is_constructible_<kernel_type, Kern>()
   )> filter(compute::context ctx, Kern&& kern)
      : filt_kern_{std::forward<Kern>(kern)}
      , z_(ctx)
      , nci_{(size_t)-1}
      , reset_{true}
   {
      reset();
   }

   ///@{
   /**
    @brief Apply the filter to a vector.

    @tparam Cont Container class type (supported by boost compute)
    @param x Input vector
    @param co First channel offset
    @param nc Number of channels to process
    @param ns Number of samples
    @param y Output vector; has to be pre-allocated and be at least of the same length as the input
    */
   compute::event operator()(input_buffer_type& x, output_buffer_type& y, bool all, compute::command_queue& q)
   {
      const auto dim = x.dimensions();
      // make sure that the dimensions of y are the same (including channel offset)
      y.reshape(dim);
      auto ev = _filter_impl(x, dim.channel_offset, dim.channels, y, q);
      if (all && x.next() != nullptr)
      {
         assert(y.next() != nullptr);
         return (*this)(*x.next(), *y.next(), true, q);
      }
      return ev;
   }

   compute::event operator()(input_buffer_type& x, size_t co, size_t nc, output_buffer_type& y, bool all, compute::command_queue& q)
   {
      const auto& dimx = x.dimensions();
      const data_source::data_dimensions dim {
         co, nc,
         dimx.sample_offset, dimx.samples,
         dimx.format, dimx.sampling_rate
      };
      y.reshape(dim);
      auto ev = _filter_impl(x, co, nc, y, q);
      if (all && x.next() != nullptr)
      {
         assert(y.next() != nullptr);
         return (*this)(*x.next(), co, nc, *y.next(), true, q);
      }
      return ev;
   }
   ///@}

   /**
    @brief Filter kernel attributes

    @return Filter kernel
    */
   const filter_kernel<Tin, Tout>& kernel() const
   {
      return filt_kern_;
   }

   /**
    @brief Reset filter state
    */
   void reset()
   {
      reset_ = true;
   }

   /**
    @brief Internal filter state

    @return
    */
   template<typename F>
   void reset_state(input_buffer_type& x, size_t co, size_t nc, F& f, compute::command_queue& q)
   {
      _init(x.parent_dimensions().channels, x.dimensions().channels, co, nc, x.dimensions().format, q);
      f(x, co, nc, z_, q);
   }

   /**
    @brief Total number of channels in the input data

    @return Number of channels
    */
   size_t input_channels() const
   {
      return nci_;
   }

private:

   void _init(size_t nci, size_t ncb, size_t co, size_t nc, data_source::storage_format dsf, compute::command_queue& q)
   {
      if (reset_)
      {
         if (nci_ != nci)
         {
            // need the compute program and kernel names to generate an OpenCL kernel object
            bool partial = nc < ncb;
            compute::program prog;
            std::string name;
            std::tie(prog, name) = filt_kern_.program(q.get_context(), ncb, partial, nc, dsf);
            kernel_ = compute::kernel{prog, name};
            // allocate filter state space for all input channels - even if we will only process a subset at a time
            const auto sz = nci*(2*filt_kern_.order + 1);
            z_.resize(sz);
            nci_ = nci;
         }
         compute::fill(z_.begin(), z_.end(), 0., q);
         reset_ = false;
      }
   }

   // Implementation of the filtering function. For description of arguments see operator() above.
   compute::event _filter_impl(input_buffer_type& x, size_t co, size_t nc, output_buffer_type& y, compute::command_queue& q)
   {
      const auto ns = x.dimensions().samples;
      _init(x.parent_dimensions().channels, x.dimensions().channels, co, nc, x.dimensions().format, q);

      kernel_.set_arg(0, x.buffer().get_buffer());
      kernel_.set_arg<cl_ulong>(1, ns);
      kernel_.set_arg(2, y.buffer().get_buffer());
      kernel_.set_arg(3, z_);

      const auto vw = filt_kern_.vector_width;
      return q.enqueue_1d_range_kernel(kernel_, co/vw, nc/vw, 1);
   }
};

///@{
/**
 @brief Tests if a type is a filter

 @tparam T object type
 */
template<typename T>
struct is_filter : std::false_type {};

template<typename In, typename Out>
struct is_filter<filter<In, Out>> : std::true_type {};
///@}

} // namespace dsp

///@}

#endif /* FILTER_HPP */
