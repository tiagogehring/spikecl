#include "filter_kernel_coefficients.hpp"
#include <cassert>
#include <boost/functional/hash.hpp>

namespace dsp
{

filter_kernel_coefficients::filter_kernel_coefficients()
   : hash_{0}
   , b{}
   , a{}
   , order{0}
{}

filter_kernel_coefficients::filter_kernel_coefficients(std::vector<double> _b, std::vector<double> _a)
	: hash_(0)
	, b(std::move(_b))
	, a(std::move(_a))
	, order((a.size() - 1) / 2)
{
	assert(a.size() % 2 == 1);
	assert(b.size() == a.size());
}

filter_kernel_coefficients::filter_kernel_coefficients(const filter_kernel_coefficients& other)
	: hash_(other.hash_)
	, b(other.b)
	, a(other.a)
   , order(other.order)
{};

filter_kernel_coefficients::filter_kernel_coefficients(filter_kernel_coefficients&& other)
	: hash_(other.hash_)
	, b(std::move(other.b))
	, a(std::move(other.a))
	, order(other.order)
{}

bool filter_kernel_coefficients::empty() const
{
   return a.empty();
}

size_t filter_kernel_coefficients::hash() const
{
   if (hash_ == 0)
   {
      for(auto x : b)
         boost::hash_combine(hash_, x);
      for(auto x : a)
         boost::hash_combine(hash_, x);
   }
   return hash_;
}

} // namespace dsp

