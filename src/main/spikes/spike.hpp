#ifndef SPIKE_HPP
#define SPIKE_HPP

/// @addtogroup Spike_detection
///@{

/**
 * @file   spike_detector.hpp
 * @author Tiago Gehring
 * @date   February, 2015
 * @brief  Basic spike class
 */

#include "spike_base.hpp"
#include "spikes_iterator.hpp"

/**
 @brief Simple spike class that does nothing more than sample time and channel of a spike

 This class is the fundament for the spike_iterator class as it already allows to iterate
 through the spikes (although this is not exposed in the public interface);

 @tparam Container container class
 */
template<typename Container>
class spike
{
protected:
   using container_type = Container;
   using type = spike<container_type>;

   // all those are our friends
   friend class spikes_iterator<container_type>;
   using data_block_type = typename container_type::data_block_type;

   // parent object (container)
   container_type* par_;
   // spike index
   mutable size_t idx_;

   void _next()
   {
      ++idx_;
   }

   bool _end() const
   {
      return (idx_ >= par_->nspk_->at(0));
   }

public:
   /**
    @brief Constructor

    @param par Parent container
    @param idx Spike index
    */
   spike(container_type* par, size_t idx)
      : par_{par}
      , idx_{idx}
   {}
   /**
    @brief Comparison operator
    */
   bool operator == (const type& other) const
   {
      return (par_ == other.par_ && idx_ == other.idx_) || (_end() && other._end());
   }

   /**
    @return Sample number of spike
    */
   size_t sample() const
   {
      return par_->spk_[idx_].sample;
   }

   double peak() const
   {
      return par_->spk_[idx_].peak;
   }

   /**
    @return Channel number of spike
    */
   size_t channel() const
   {
      return par_->spk_[idx_].channel;
   }

   /**
    @return Spike status
    */
   spike_status status() const
   {
      return par_->spk_[idx_].status;
   }

   /**
    @brief Spike sign
    */
   int sign() const
   {
      return (par_->spk_[idx_].peak > 0. ? 1 : -1);
   }
};

///@}

#endif /* SPIKE_HPP */
