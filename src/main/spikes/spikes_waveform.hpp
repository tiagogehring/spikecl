#ifndef SPIKES_WAVEFORM_HPP
#define SPIKES_WAVEFORM_HPP

/// @addtogroup Spike_detection
///@{

/*
 * @file   spikes_waveform.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Provides storage for detected spikes and spike waveforms
 */

#include <algorithm>
#include <utility>
#include <vector>
#include <boost/compute/core.hpp>
#include <boost/compute/container/mapped_view.hpp>
#include "spikes.hpp"
#include "spike_artifact_detector.hpp"
#include "spikes_iterator.hpp"
#include "spike_waveform.hpp"
#include "multichannel/iterator.hpp"
#include "utility/type_traits.hpp"

namespace compute = boost::compute;

/**
 @brief Provides storage for a list of spikes from multipe data channeles

 This class also provides basic filtering of spikes removing ones that lie to close to
 the edges of the data set. The number of spike points to keep track for each spike can
 be det in the constructor.

 @tparam T Data type of the input data
 */
template< typename T
        , typename ArtifactDet = spike_artifact_detector<T>
> class spikes_waveform : public spikes<T>
{
public:
   using type = spikes_waveform<T, ArtifactDet>;
   using base_type = spikes<T>;
   using data_block_type = typename base_type::data_block_type;
   using data_iterator_type = multichannel::iterator<T>;
   using artifact_detector_type = ArtifactDet;

   using spike_type = spike_waveform<type>;
   using iterator_type = spikes_iterator<type>;

   friend class spike_waveform<type>;
   friend class spike<type>;

protected:
   using base_type::nspk_;
   using base_type::spk_;

   // Number of points before spike peak
   size_t pt_pre_;
   // Number of points after spike peak
   size_t pt_post_;
   // Artifact detector
   artifact_detector_type artif_det_;
   // pointer to associated buffer with the data
   data_block_type* data_;
   compute::command_queue* queue_;

public:
   /**
    @brief Constructor

    @param pt_pre Number of points to keep track of before each spike
    @param pt_post Number of points to keep track of after each spike
    */
   template< typename ... ADArg
           , REQUIRES( is_constructible_<ArtifactDet, ADArg...>()
   )> spikes_waveform(size_t nmax, size_t pt_pre, size_t pt_post, ADArg&& ... arg)
      : base_type{nmax}
      , pt_pre_{pt_pre}
      , pt_post_{pt_post}
      , artif_det_{std::forward<ADArg>(arg)...}
      , data_{nullptr}
      , queue_{nullptr}
   {}

   iterator_type begin()
   {
      return iterator_type{ spike_type{this, 0, queue_} };
   }

   iterator_type end()
   {
      return iterator_type{ spike_type{this, (size_t)-1, queue_} };
   }

   using base_type::count;

protected:
   virtual void _pre_process(data_block_type& x) override
   {
      // if this block is a follow up to the previous one copy right-boundary spikes
      // to the next set of spikes
      int in = 0;
      if (data_ != nullptr && x.prev() == data_)
      {
         for (unsigned is = 0; is < count(); ++is)
         {
            if (spk_[is].status == static_cast<int>(spike_status::right_boundary))
            {
               spk_[in] = spk_[is];
               spk_[in].status = static_cast<int>(spike_status::valid);
               ++in;
            }
         }
      }
      nspk_->at(0) = in;
   }

   /**
    @brief Processes a data buffer. Current spikes are replaced by ones found in the new data set.

    This function is usually called from the spike_detector class (@ref spike_detector). Note that cumulative number of spikes is not reset.

    @param x Input buffer
    @param spks List of spikes
    @param nspks Number of spikes in each channel
    @param spk_det Reference to spike detector
    */
   virtual void _process(compute::command_queue& q, data_block_type& x, const compute::vector<T>& noise_level) override
   {
      base_type::_process(q, x, noise_level);
      artif_det_.noise_level(noise_level);

      data_ = &x;
      queue_ = &q;

      // make sure that spike data is synchronized with the OpenCL memory space
      base_type::_map(q);
      q.finish();

      // check each spike
      for (unsigned is = 0; is < count(); ++is)
      {         
         // look for spikes at the left boundary
         if (static_cast<long>(spk_[is].sample) - static_cast<long>(x.dimensions().sample_offset) < static_cast<long>(pt_pre_) - 1)
            spk_[is].status = static_cast<int>(spike_status::left_boundary);

         // look for spikes at the right boundary -> those will possibly be taken care of by the next buffer
         if (spk_[is].sample >= (x.dimensions().sample_offset + x.dimensions().samples - pt_post_))
         {
            spk_[is].status = static_cast<int>(spike_status::right_boundary);
         }
      }
   }
};

///@}

#endif
