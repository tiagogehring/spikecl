#ifndef SPIKE_SMOOTHED_WAVEFORM_HPP
#define SPIKE_SMOOTHED_WAVEFORM_HPP
#include "spike.hpp"

template<typename Container>
class spike_smoothed_waveform : public spike_waveform<Container>
{
protected:
   using type = spike_smoothed_waveform<Container>;
   using base_type = spike_waveform<Container>;
   using container_type = typename base_type::container_type;
   using data_block_type = typename base_type::container_type;
   using data_iterator_type = typename base_type::data_iterator_type;
   using waveform_iterator_type = typename container_type::waveform_iterator_type;

   using base_type::par_;
   using base_type::idx_;

   friend class spikes_iterator<container_type>;
   friend class spike<container_type>;
   friend class spike_waveform<container_type>;

public:
   using base_type::status;

   spike_smoothed_waveform(container_type* par, size_t idx, compute::command_queue* q)
      : base_type{par, idx, q}
   {}

   waveform_iterator_type smoothed_waveform_begin() const
   {
      assert(status() == spike_status::valid);
      return par_->wf_.begin() + (par_->pt_pre_ + par_->pt_post_ - 2*par_->fac_)*idx_;
   }

   waveform_iterator_type smoothed_waveform_end() const
   {
      assert(status() == spike_status::valid);
      return par_->wf_.begin() + (par_->pt_pre_ + par_->pt_post_ - 2*par_->fac_)*(idx_ + 1);
   }

   data_iterator_type waveform_begin() const
   {
      auto it = base_type::waveform_begin();
      it += par_->fac_;
      return it;
   }

   data_iterator_type waveform_end() const
   {
      auto it = base_type::waveform_end();
      it -= par_->fac_;
      return it;
   }
};

#endif /* SPIKE_SMOOTHED_WAVEFORM_HPP */
