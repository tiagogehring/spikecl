#ifndef SPIKE_ARTIFACT_DETECTOR_HPP
#define SPIKE_ARTIFACT_DETECTOR_HPP

#include "multichannel/iterator.hpp"
#include "spikes.hpp"

/**
 @file multichanne_data_iterator.hpp
 @author Tiago Gehring
 @date Nov 2014
 */

/**
 @brief Basic artifact detector that discards spikes where any sample crosses a pre-defined threshold

 @tparam T data type supported
 */
template<typename T>
class spike_artifact_detector
{
   T thresh_sd_; //!< The maximum sample value (in standard deviations) above which spikes are considered artifacts
   const compute::vector<T>* noise_;

public:
   using iterator = multichannel::iterator<T>;

   /**
    @brief Constructor

    @param sd_max Threshold (in SD) above which spikes are considered artifacts
    */
   spike_artifact_detector(T sd_max)
      : thresh_sd_{sd_max}
      , noise_{nullptr}
   {}


   // @todo: possibly remove this "hack"
   void noise_level(const compute::vector<T>& nl)
   {
      noise_ = &nl;
   }

   /**
    @brief Test for artifacts

    @param beg Iterator pointing to beginning of spike
    @param end Iterator pointing to end of spike
    @param noise Noise level of data channel

    @return true if spike is an artifact
    */
   bool operator()(const spike_base& spk, const iterator& beg, const iterator& end) const
   {
      assert(noise_ != nullptr);
      for (iterator it = beg; it < end; ++it)
      {
         if (std::abs(*it) > thresh_sd_*(*noise_)[spk.channel])
            return true;
      }
      return false;
   }
};

#endif /* SPIKE_ARTIFACT_DETECTOR_HPP */
