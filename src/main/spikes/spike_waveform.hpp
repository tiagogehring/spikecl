#ifndef SPIKE_WAVEFORM_HPP
#define SPIKE_WAVEFORM_HPP

#include "spike.hpp"

template<typename Container>
class spike_waveform : public spike<Container>
{
protected:
   using type = spike_waveform<Container>;
   using base_type = spike<Container>;
   using container_type = typename base_type::container_type;
   using data_block_type = typename base_type::container_type;
   using data_iterator_type = typename container_type::data_iterator_type;

   using base_type::par_;
   using base_type::idx_;

   friend class spikes_iterator<container_type>;
   friend class spike<container_type>;

   // iterator valid falg
   mutable bool it_valid_;
   mutable data_iterator_type wf_beg_;
   mutable data_iterator_type wf_end_;

   compute::command_queue* queue_;

   inline void _it_set() const
   {
      assert( par_->spk_[idx_].status == static_cast<int>(spike_status::unknown) ||
              par_->spk_[idx_].status == static_cast<int>(spike_status::valid));
      if (!it_valid_)
      {
         wf_beg_ = data_iterator_type{par_->data_, base_type::channel(), base_type::sample() - par_->pt_pre_ + 1 , queue_};
         wf_end_ = data_iterator_type{par_->data_, base_type::channel(), base_type::sample() + par_->pt_post_ + 1, queue_};
         it_valid_ = true;
      }
   }

   void _next()
   {
      base_type::_next();
      it_valid_ = false;
   }

public:
   spike_waveform(container_type* par, size_t idx, compute::command_queue* q)
      : base_type{par, idx}
      , it_valid_{false}
      , queue_{q}
   {}

   spike_status status() const
   {
      if (par_->spk_[idx_].status == static_cast<int>(spike_status::unknown))
      {
         _it_set();
         // check if the spike is valid
         par_->spk_[idx_].status = static_cast<cl_ushort>(
            par_->artif_det_(par_->spk_[idx_], wf_beg_, wf_end_) ? spike_status::artifact
                                                                 : spike_status::valid);
      }
      return static_cast<spike_status>(par_->spk_[idx_].status);
   }

   data_iterator_type waveform_begin() const
   {
      assert(status() == spike_status::valid);
      _it_set();
      return wf_beg_;
   }

   data_iterator_type waveform_end() const
   {
      assert(status() == spike_status::valid);
      _it_set();
      return wf_end_;
   }
};

#endif /* SPIKE_WAVEFORM_HPP */
