#ifndef SPIKES_SMOOTHED_WAVEFORM_HPP
#define SPIKES_SMOOTHED_WAVEFORM_HPP

#include <vector>
#include <utility>

#include "spikes_waveform.hpp"
#include "spike_smoothed_waveform.hpp"
#include "interpolation/cubic_spline.hpp"
#include "utility/opencl_logging.hpp"

/**
 @brief Spike storage class that stores not only the spike times but also a fixed number of
        samples before and after each spike. No further processing of the waveforms is performed.

 @tparam T Data type supported by the class
 */
template< typename T
        , typename ArtifactDetector = spike_artifact_detector<T>
        , typename InterpolatingFunction = interpolation::cubic_spline<T>
> class spikes_smoothed_waveform : public spikes_waveform<T, ArtifactDetector>
{
public:
   using interpolating_function_type = InterpolatingFunction;
   using type = spikes_smoothed_waveform<T, ArtifactDetector, InterpolatingFunction>;
   using base_type = spikes_waveform<T, ArtifactDetector>;
   using spike_type = spike_smoothed_waveform<type>;
   using waveform_iterator_type = typename compute::vector<T>::iterator;
   using iterator_type = spikes_iterator<type>;
   using data_block_type = typename base_type::data_block_type;

private:
   // OpenCL kernel that extracts waveforms
   compute::kernel kernel_;
   // stores the interpolated waveforms (more points)
   compute::vector<T> wf_interp_;
   // stores final waveforms of spikes
   compute::vector<T> wf_;
   using spikes_offset_type = std::vector<channel_offset>;
   using spikes_offset_view = compute::mapped_view<channel_offset>;
   spikes_offset_type off_;
   spikes_offset_view off_view_;
   // spike smoothing factor
   size_t fac_;
   // spike sign
   int sign_;
   interpolating_function_type f_;
   using base_type::spk_;
   using base_type::queue_;
   using base_type::pt_pre_;
   using base_type::pt_post_;

public:
   friend class spike_smoothed_waveform<type>;
   friend class spike_waveform<type>;
   friend class spike<type>;

   using base_type::count;

   /**
    @brief Constructor

    @param pt_pre Number of points to keep track of before each spike
    @param pt_post Number of points to keep track of after each spike
    */
   template< typename ... ADArg
           , REQUIRES( is_constructible_<ArtifactDetector, ADArg...>()
   )> spikes_smoothed_waveform(size_t nmax, size_t pt_pre, size_t pt_post, size_t fac, int sign, ADArg&& ... arg)
      : base_type{nmax, pt_pre + fac, pt_post + fac, std::forward<ADArg>(arg)...}
      , fac_{fac}
      , sign_{sign}
      , f_{pt_pre + pt_post + 2*fac, fac}
   {
   }

   /**
    @brief Processes a data buffer. Current spikes are replaced by ones found in the new data set.

    This function is usually called from the spike_detector class (@ref spike_detector). Note that cumulative number of spikes is not reset.

    @param x Input buffer
    @param spks List of spikes
    @param nspks Number of spikes in each channel
    @param spk_det Reference to spike detector
    */
   virtual void _process(compute::command_queue& q, data_block_type& x, const compute::vector<T>& noise_level) override
   {
      base_type::_process(q, x, noise_level);

      if (fac_ > 0)
      {
         if (off_.size() < count())
         {
            if (off_.empty())
            {
               // kernel to extract waveforms
               auto prog = _extract_waveforms_program(q.get_context(), pt_pre_, pt_pre_ + pt_post_, fac_, sign_);
               kernel_ = compute::kernel{prog.first, prog.second};
            }

            wf_interp_ = compute::vector<T>{count()*(pt_pre_ + pt_post_)*fac_, q.get_context()};
            // buffer for final waveforms
            wf_ = compute::vector<T>{count()*(pt_pre_ + pt_post_ - 2*fac_), q.get_context()};
            off_.resize(count());
            off_view_ = spikes_offset_view(&off_[0], off_.size(), q.get_context());
         }

         // check each spike
         for (unsigned is = 0; is < count(); ++is)
         {
            off_[is].channel = static_cast<cl_uint>(spk_[is].channel - x.dimensions().channel_offset);
            off_[is].offset = static_cast<cl_uint>(std::abs(spk_[is].sample) - x.dimensions().sample_offset - pt_pre_);
         }

         // interpolate spike waveforms
         f_(x, off_view_, count(), wf_interp_, q);

         // extract final waveforms
         kernel_.set_arg(0, wf_interp_.get_buffer());
         kernel_.set_arg(1, wf_.get_buffer());
         q.enqueue_1d_range_kernel(kernel_, 0, count(), 1);
      }
   }

   iterator_type begin()
   {
      return iterator_type{ spike_type{this, 0, queue_} };
   }

   iterator_type end()
   {
      return iterator_type{ spike_type{this, (size_t)-1, queue_} };
   }

private:
   static std::pair<compute::program, std::string> _extract_waveforms_program(compute::context ctx, size_t npre, size_t ntot, size_t fac, int sign)
   {
      std::stringstream name;
      name << "extract_waveforms_" << TYPE_ABBREVIATION(T) << "_" << "_f" << fac << "_" << npre << "_" << ntot << "_" << sign;

      auto cache = compute::program_cache::get_global_cache(ctx);
      auto prog = cache->get(name.str(), "");
      if (prog)
         return std::make_pair(*prog, name.str());

      // full length of intermediate interpolated spike (with extra trailing/leading points)
      size_t l = ntot*fac;

      opencl_source_generator src;
      src << compute::type_definition<channel_offset>() << "\n";
      src.kernel(name.str(), std::is_same<T, double>::value).open("(");
      // arguments
      src.parameter<T*>("x");
      src.parameter<T*>("out");
      src.close(")").open("{");
      src.new_line() << "int i = get_global_id(0);";
      src.new_line() << "int j, imax = -1;";
      src.new_line() << opencl_type_name<T>() << " xmax = 0.;";
      src.new_line() << "for(j = " << npre*fac << "; j < " << (npre*fac + 2*fac*fac) << "; ++j)";
      src.open("{");
      if (sign > 0)
         src.new_line() << "if (imax == -1 || x[i*" << l << " + j] > xmax)";
      else if (sign < 0)
         src.new_line() << "if (imax == -1 || x[i*" << l << " + j] < xmax)";
      else
         src.new_line() << "if (imax == -1 || fabs(x[i*" << l << " + j]) > fabs(xmax))";
      src.open("{");
      src.new_line() << "imax = j; xmax = x[i*" << l << " + j];";
      src.close("}");
      src.close("}");
      // ok, found the maximum, just copy the spike now
      src.new_line() << "for(j = 0; j < " << (ntot - 2*fac) << "; ++j)";
      src.open("{");
      //src.new_line() << "imax = " << (npre + 2)*fac << ";";
      //src.new_line() << "out[i*" << ntot << " + j] = " << "x[i*" << l<< " + imax - " << npre*fac << " + j*" << fac << "];";
      src.new_line() << "out[i*" << (ntot - 2*fac) << " + j] = " << "x[i*" << l<< " + (j + " << fac << ")*" << fac << "];";
      src.close("}");
      src.close("}");

      opencl_log_kernel(src.str());
      compute::program prgm;
      try
      {
         prgm = compute::program::create_with_source(src.str(), ctx);
         prgm.build();
         cache->insert(name.str(), "", prgm);
      }
      catch(std::exception &)
      {
         // program failed to compile, print out the build log
         std::cerr << prgm.build_log() << std::endl;
      }
      return std::make_pair(prgm, name.str());
   }
};

#endif /* SPIKES_SMOOTHED_WAVEFORM_HPP */
