#ifndef SPIKE_DETECTOR_HPP
#define SPIKE_DETECTOR_HPP

/// @defgroup Spike_detection Spike detection functions
///@{

/**
 * @file   spike_detector.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Spike detection algorithm that can process multiple data channels in parallel
 */

#include <vector>
#include <tuple>
#include <boost/compute/core.hpp>
#include <boost/compute/lambda.hpp>
#include <boost/compute/algorithm/fill.hpp>
#include <boost/compute/container/vector.hpp>
#include <boost/compute/algorithm/transform.hpp>
#include <boost/compute/types/struct.hpp>
#include <boost/compute/utility/program_cache.hpp>
#include "statistics/median.hpp"
#include "multichannel/data_block.hpp"
#include "spikes.hpp"
#include "utility/opencl_source_generator.hpp"
#include "utility/opencl_type_name.hpp"
#include "utility/type_abbreviation.hpp"
#include "utility/opencl_logging.hpp"

namespace compute = boost::compute;

// make sure that structure is not padded since it is not supported by Boost Compute (VC++ seems to need some hints)
#ifdef _MSC_VER
	#pragma pack(push, 1)
#endif
struct spike_detector_state_f
{
   cl_int last_spk;
   cl_int peak_idx;
   cl_float peak;
};
struct spike_detector_state_d
{
   cl_int last_spk;
   cl_int peak_idx;
   cl_double peak;
};
#ifdef _MSC_VER
	#pragma pack(pop)
#endif

BOOST_COMPUTE_ADAPT_STRUCT(spike_detector_state_f, spike_detector_state_f, (last_spk, peak_idx, peak));
BOOST_COMPUTE_ADAPT_STRUCT(spike_detector_state_d, spike_detector_state_d, (last_spk, peak_idx, peak));

template<typename T>
struct spike_detector_state;

template<>
struct spike_detector_state<float>
{
   using type = spike_detector_state_f;
   static const char name[];
};

template<>
struct spike_detector_state<double>
{
   using type = spike_detector_state_d;
   static const char name[];
};

/**
 @brief Implements a spike detector. Spikes are found by comparing sample values to a threshold.

 The threshold for spike detection is given in standard deviations, which is estimated from the data itself.
 This class does not store spikes, it merelly implements a kernel function that processes a block of data returning
 the sample times of the spikes that were found. These are then forwarded to another class which post-processes and/or
 stores the spikes.

 @tparam T Data type to support
 */
template<typename T>
class spike_detector
{
public:
   using values_type = compute::vector<T>;

private:
   // OpenCL kernel (function) that performs the spike detection
   compute::kernel kernel_;
   // SIMD vector width
   size_t vw_;
   // minimum SD for spike threshold
   T sd_min_;
   // refractory time (in samples)
   double refr_ms_;
   // spike sign
   int sign_;
   // Need median and SD of each channel to compute spike threshold
   statistics::median<T> med_;
   compute::vector<T> sd_;
   // spike detector state (one for each channel)
   using spike_detector_state_type = typename spike_detector_state<T>::type;
   compute::vector<spike_detector_state_type> state_;
   // total number of input channels
   size_t nci_;
   // number of input channels per block
   size_t ncb_;
   // reset flag
   bool reset_;
   // refractory number of samples (computed from the sampling rate of the source)
   size_t rns_;

public:
   using data_block_type = multichannel::data_block<T>;

   /**
    @brief Constructor

    @param q OpenCL command_queue
    @param vw vector width to support (for explicit SIMD coding)
    @param sd_min Spike threshold in standard deviations
    @param refr_ms Refractory period in milliseconds
    @param sign Spike sign filter (+1/-1 only positive/negative spikes); 0 = all spikes
    @param bs Spike buffer size (maximum number of spikes)
    */
   spike_detector(compute::context ctx, size_t vw, T sd_min, double refr_ms, int sign = 0)
      : vw_{vw}
      , sd_min_{sd_min}
      , refr_ms_{refr_ms}
      , sign_{sign}
      , med_{ctx, vw, 256*256, true}
      , sd_(ctx)
      , state_(ctx)
      , nci_{(size_t)-1}
      , ncb_{(size_t)-1}
      , reset_{true}
      , rns_{(size_t)-1}
   {}

   /**
    @brief resets state of detector (such as the spike threshold estimate)
    */
   void reset()
   {
      reset_ = true;
      med_.reset();
   }

   /**
    @brief refractory time in ms

    @return Refractory time
    */
   double refractory_time() const
   {
      return refr_ms_;
   }

   /**
    @brief returns the noise level, or standard deviation, of each channel

    @param chan channel number
    @return standard deviation value for channel
    */
   const compute::vector<T>& noise_level() const
   {
	   return sd_;
   }

   const compute::vector<T> thresholds(compute::command_queue& q) const
   {
      using compute::lambda::_1;

      compute::vector<T> thresh(sd_.size(), q.get_context());         

      compute::copy(sd_.begin(), sd_.end(), thresh.begin(), q);      
      compute::transform(thresh.begin(), thresh.end(), thresh.begin(), _1*sd_min_, q);

      return thresh;
   }

   ///@{
   /**
    @brief Updates the spike firing threshold estimate

    The estimate is based on the median value of the data which is estimated from the standard deviation

    @param x Data block
    @param co First channel offset to process
    @param nc Number of channels to process
    */
   void update_threshold(data_block_type& x, bool all, compute::command_queue& q)
   {
      _update_threshold_impl(x, x.dimensions().channel_offset, x.dimensions().channels, all, q);
   }

   void update_threshold(data_block_type& x, size_t co, size_t nc, bool all, compute::command_queue& q)
   {
      _update_threshold_impl(x, co, nc, all, q);
   }
   ///@}

   ///@{
   /**
    @brief Processes a data block

    @tparam SpikesStorage Spike storage class
    @param x Data block
    @param out Spike storage
    @param last Indicates if this is the last data block to be processed
    */
   void operator()(data_block_type& x, spikes<T>& out, bool upd_thresh, compute::command_queue& q)
   {
      _process_impl(x, x.dimensions().channel_offset, x.dimensions().channels, out, upd_thresh, q);
   }

   void operator()(data_block_type& x, size_t co, size_t nc, spikes<T>& out, bool upd_thresh, compute::command_queue& q)
   {
      _process_impl(x, co, nc, out, upd_thresh, q);
   }
   ///@}

   /**
    @brief Spike detector kernel.

    The generated function, f(ns, x*, off, spk*, nspk*, sd*, state*), takes five arguments:
    -# ns: number of samples to process
    -# off: offset of first sample (to compute the right times)
    -# x:  input data
    -# spk: vector where to store the spikes (size of the vector has to be large enough to support at least the
            maximum number of spikes given when generating the kernel (see parameter nmax below)
    -# nspk: output number of spikes
    -# sd: standard deviation estimate used to calculate the thresholds
    -# state: spike detector state

    @param mult_blk If false it is assumed that the input offset is always zero
    @param nref Number of refractory samples
    @param sign Sign of spikes (-1/0/1): (negative only/all/positive only)
    @param sd_min Minimum threshold in standard deviations
    @param nmax Maximum number of spikes to detect

    @return Kernel source
    */
   static compute::program program(compute::context ctx, const std::string& name, size_t ncb, bool partial, size_t nref, int sign, T sd_min)
   {
      opencl_source_generator src;
      // add type definition to the start of the program source
      src << compute::type_definition<spike_base>();
      src.new_line() << compute::type_definition<spike_detector_state_type>() << "\n";

      // see if program already cached
      auto cache = compute::program_cache::get_global_cache(ctx);
      auto prog = cache->get(name, "");
      if (prog)
         return *prog;

      src.kernel(name, std::is_same<T, double>::value).open("(");
      // arguments
      src.parameter<cl_ulong>("ns");
      src.parameter<cl_ulong>("off");
      src.parameter<T*>("x");
      src << ", __global spike_base* spk";
      src.parameter<cl_int*>("nspk");
      src.parameter<cl_ulong>("nmax");
      src.parameter<T*>("sd");
      src << ", __global " << spike_detector_state<T>::name << "* state";
      src.close(")").open("{");
      src.new_line() << "ulong i = (ulong)get_global_id(0);";
      src.new_line() << "ulong co = (ulong)get_global_offset(0);";
      src.new_line() << "if (off == 0)";
      src.open("{");
      src.new_line() << "state[i].last_spk = -1; state[i].peak_idx = -1; state[i].peak = 0.;";
      src.close("}");
      src.new_line() << "ulong j;";
      src.new_line() << "for(j = 0; j < ns; ++j)";
      src.open("{");
      src.new_line() << "if (state[i].last_spk == - 1 || off + j - state[i].last_spk >= " << nref << ")";
      src.open("{");
      // input index
      std::stringstream xin;
      !partial ? xin << "x[j*" << ncb << " + (i - co)]"
               : xin << "x[j*" << ncb << " + i]";
      switch(sign)
      {
         case -1:
            src.new_line() << "if (" << xin.str() << " < -" << sd_min << "*sd[i])";
            break;
         case 1:
            src.new_line() << "if (" << xin.str() << " > " << sd_min << "*sd[i])";
            break;
         case 0:
            src.new_line() << "if (fabs(" << xin.str() << ") > " << sd_min << "*sd[i])";
            break;
         default:
            assert(0 && "Invalid value!");
      }
      src.open("{");
      src.new_line() << "state[i].last_spk = off + j;";
      src.new_line() << "state[i].peak = " << xin.str() << ";";
      src.new_line() << "state[i].peak_idx = off + j;";
      src.close("}");
      src.close("}");
      src.new_line() << "else";
      src.open("{"); src.new_line() << "if (state[i].last_spk != -1 && off + j - state[i].last_spk >= " << (int)(nref/2) << ")"; src.open("{");
      src.new_line() << "if (state[i].peak_idx != 0)";
      src.open("{");
      src.new_line() << "int k = atomic_inc(nspk);";
      src.new_line() << "if (k < nmax)";
      src.open("{");
      // initially the status of the spike is "unknown" since it has to be validated later
      src.new_line() << "spk[k].status = " << static_cast<int>(spike_status::unknown) << ";";
      src.new_line() << "spk[k].channel = i;";
      src.new_line() << "spk[k].peak = state[i].peak;";
      src.new_line() << "spk[k].sample = state[i].peak_idx;";
      src.close("}");
      src.new_line() << "state[i].last_spk = state[i].peak_idx; state[i].peak_idx = 0;";
      src.close("}");
      src.close("}");
      src.new_line() << "else";
      src.open("{");
      switch (sign)
      {
      case -1:
         src.new_line() << "if (state[i].peak_idx != 0 && " << xin.str() << " < state[i].peak)";
         break;
      case 1:
         src.new_line() << "if (state[i].peak_idx != 0 && " << xin.str() << " > state[i].peak)";
         break;
      case 0:
         src.new_line() << "if (state[i].peak_idx != 0 && fabs(" << xin.str() << ") > fabs(state[i].peak))";
         break;
      default:
         assert(0 && "Invalid value!");
      }      
      src.open("{");
      src.new_line() << "state[i].peak = " << xin.str() << "; state[i].peak_idx = off + j;";
      src.close("}");
      src.close("}");
      src.close("}");
      src.close("}");
      src.close("}");

      opencl_log_kernel(src.str());
      compute::program prgm;
      try
      {
         prgm = compute::program::create_with_source(src.str(), ctx);
         prgm.build();
         cache->insert(name, "", prgm);
      }
      catch(std::exception &)
      {
         // program failed to compile, print out the build log
         std::cout << prgm.build_log() << std::endl;
      }
      return prgm;
   }

protected:

   void _init(data_block_type& x, size_t nc, compute::command_queue& q)
   {
      if (reset_)
      {
         // total number of channels
         const auto nci = x.parent_dimensions().channels;
         // channels per block
         const auto ncb = x.dimensions().channels;
         assert(x.dimensions().sampling_rate > 0.);
         size_t rns = static_cast<size_t>(std::ceil(refr_ms_/ (1000./ x.dimensions().sampling_rate)) + 1.e-1); // refractory samples
         if (nci != nci_ || rns_ != rns)
         {
            bool partial = nc < ncb;
            std::stringstream name;
            name << "detect_spikes_" << ncb << "_" << vw_ << (partial ? "_partial" : "")
                 << "_" << rns << "_" << TYPE_ABBREVIATION(T);
            kernel_ = compute::kernel{program(q.get_context(), name.str(), ncb, partial, rns, sign_, sd_min_), name.str()};
            sd_.resize(nci, q);
            state_.resize(nci, q);
            nci_ = nci;
            rns_ = rns;
         }

         compute::fill(sd_.begin(), sd_.end(), 0, q);
         reset_ = false;
      }
      assert(x.dimensions().channel_offset < nci_);
      assert(x.dimensions().channel_offset + nc <= nci_);
   }

   void _update_threshold_impl(data_block_type& x, size_t co, size_t nc, bool all, compute::command_queue& q)
   {
      using compute::lambda::_1;

      _init(x, nc, q);

      // update median
      med_(x, co, nc, all, q);

      auto& m = med_.median_value(q);
      compute::copy(m.begin(), m.end(), sd_.begin(), q);
      q.finish();

      // update SD estimate (median / 0.6745)
      compute::transform(sd_.begin(), sd_.end(), sd_.begin(), _1 / 0.6745, q);
   }

   void _process_impl(data_block_type& x, size_t co, size_t nc, spikes<T>& out, bool upd_thresh, compute::command_queue& q)
   {
      if (upd_thresh)
         _update_threshold_impl(x, co, nc, false, q);
      else
         _init(x, nc, q);

      // make sure that we have storage space for the spikes
      out._init_storage(q, nci_);
      kernel_.set_arg<cl_ulong>(0, x.dimensions().samples);
      kernel_.set_arg<cl_ulong>(1, x.dimensions().sample_offset);
      kernel_.set_arg(2, x.buffer().get_buffer());
      kernel_.set_arg(3, out.spk_view_.get_buffer());
      kernel_.set_arg(4, *(out.nspk_));
      kernel_.set_arg<cl_ulong>(5, out.max_spikes());
      kernel_.set_arg(6, sd_);
      kernel_.set_arg(7, state_);
      out._pre_process(x);
      q.enqueue_1d_range_kernel(kernel_, co, nc, 1);
      out._process(q, x, noise_level());
   }
};

///@}

#endif /* SPIKE_DETECTOR_HPP */
