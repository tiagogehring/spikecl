#ifndef SPIKES_ITERATOR_HPP
#define SPIKES_ITERATOR_HPP

#include <vector>
#include <boost/iterator/iterator_facade.hpp>

template<typename Spikes>
class spikes_iterator
   : public boost::iterator_facade< spikes_iterator<Spikes>
                                  , const typename Spikes::spike_type
                                  , boost::forward_traversal_tag>
{
   using storage_type = Spikes;
   using type = spikes_iterator<typename storage_type::spike_type>;
public:
   using spike_type = typename storage_type::spike_type;
   using base = boost::iterator_facade< spikes_iterator<spike_type>
                                      , const spike_type
                                      , boost::forward_traversal_tag>;

   spike_type spk_;

public:
   friend class boost::iterator_core_access;
   using difference_type = typename base::difference_type;

   spikes_iterator(spike_type spk)
      : spk_{std::move(spk)}
   {}

   void increment()
   {
      assert(!spk_._end());
      spk_._next();
   }

   bool equal(const spikes_iterator<Spikes>& other) const
   {
      return (spk_ == other.spk_);
   }

   const spike_type& dereference() const
   {
      return spk_;
   }
};

#endif /* SPIKES_ITERATOR_HPP */
