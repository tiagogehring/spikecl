#ifndef SPIKES_HPP
#define SPIKES_HPP

/// @addtogroup Spike_detection
///@{

/**
 * @file   spikes.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Provides storage for detected spikes
 */

#include <cassert>
#include <memory>
#include <boost/compute/core.hpp>
#include "multichannel/data_block.hpp"
#include <boost/compute/types/struct.hpp>
#include <boost/compute/container/array.hpp>
#include "spike.hpp"

namespace compute = boost::compute;


BOOST_COMPUTE_ADAPT_STRUCT(spike_base, spike_base, (status, channel, sample, peak));

template<typename T>
class spike_detector;
/**
 @brief Provides storage for a list of spikes from multipe data channeles

 This class is meant to be a base class for more sophisticated spike filtering and
 storing mechanisms.

 @tparam T Data type of the input data
 */
template<typename T>
class spikes
{
   friend class spike_detector<T>;
   friend class spike<spikes<T>>;

public:
   using spikes_list_type = std::vector<spike_base>;
   using data_block_type = multichannel::data_block<T>;
   using spikes_view_type = compute::mapped_view<spike_base>;

protected:
   spikes_view_type spk_view_;
   // buffer with spike sample numbers for all channels
   spikes_list_type spk_;
   // maximum number of spikes to store (per channel)
   size_t nmax_;
   // number of input data channels
   size_t nci_;
   // reset flag
   bool reset_;
   // if true buffer is mapped
   bool mapped_;
   // total number of spikes (for all channels)
   // need to put inside array since the value has to be a OpenCL memory object (for atomic operations)
   using spike_counter_type = compute::array<cl_uint, 1>;
   std::unique_ptr<spike_counter_type> nspk_;

public:
   spikes(size_t nmax)
      : nmax_{nmax}
      , nci_{(size_t)-1}
      , reset_{true}
      , mapped_{false}
   {}

   virtual ~spikes()
   {}

   /**
    @brief Maximum number of spikes per channel

    @return Maximum number of spikes
    */
   size_t max_spikes() const
   {
      return nmax_;
   }

   /**
    @brief Resets list of spikes
    */
   virtual void reset()
   {
      reset_ = true;
   }

   size_t count() const
   {
      if (nspk_)
         return (*nspk_)[0];
      return 0;
   }

   const spikes_list_type& spikes_list(compute::command_queue& q)
   {
      _map(q);
      return spk_;
   }

   const spikes_view_type& buffer(compute::command_queue& q)
   {
      _unmap(q);
      return spk_view_;
   }


protected:
   /**
    @brief Maps buffer so that OpenCL <-> host data are synchronized
    */
   void _map(compute::command_queue& q)
   {
      if (!mapped_)
      {
         spk_view_.map(q);
         mapped_ = true;
      }
   }

   /**
    @brief Unmaps memory (no guarantee that host <-> OpenCL memory is synchronized)
    */
   void _unmap(compute::command_queue& q)
   {
      if (mapped_)
      {
         spk_view_.unmap(q);
         mapped_ = false;
      }
   }
   
   virtual void _pre_process(data_block_type& x)
   {}

   /**
    @brief Processes a data buffer. Current spikes are replaced by ones found in the new data set.

    This function is usually called from the spike_detector class (@ref spike_detector). Note that cumulative number of spikes is not reset.

    @param x Input buffer
    @param spks List of spikes
    @param nspks Number of spikes in each channel
    @param spk_det Reference to spike detector
    */
   virtual void _process(compute::command_queue& q, data_block_type& x, const compute::vector<T>& noise_level)
   {}

private:
   // make sure that we have enough space to store the spikes
   void _init_storage(compute::command_queue& q, size_t nci)
   {
      if (reset_)
      {
         if (nci != nci_)
         {
            nspk_.reset(new spike_counter_type{q.get_context()});
            _unmap(q);
            spk_.resize(nci*nmax_);
            // view that maps the buffer to OpenCL space
            spk_view_ = spikes_view_type(&spk_[0], spk_.size(), q.get_context());
            nci_ = nci;
         }
         reset_ = false;
      }
      assert(nci_ >= nci);
   }
};

///@}

#endif /* SPIKES_HPP */
