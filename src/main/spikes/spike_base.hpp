#ifndef SPIKE_BASE_HPP
#define SPIKE_BASE_HPP

#include <boost/compute/core.hpp>

enum class spike_status : cl_ushort
{
   unknown = 0,
   valid = 1,
   artifact = 2,
   left_boundary = 3,
   right_boundary = 4
};

#ifdef _MSC_VER
	#pragma pack(push, 1)
#endif
struct spike_base
{
   cl_ushort status; // @see spike_status enumeration
   cl_ushort channel;
   cl_int sample;
   cl_double peak;
};
#ifdef _MSC_VER
	#pragma pack(pop)
#endif

#endif /* SPIKE_BASE_HPP */
