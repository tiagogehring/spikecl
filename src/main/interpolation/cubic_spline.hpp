#ifndef CUBIC_SPLINE_INTERPOLATION_HPP
#define CUBIC_SPLINE_INTERPOLATION_HPP

#include <string>
#include <sstream>
#include <iostream>
#include <boost/compute/core.hpp>
#include <boost/compute/types/struct.hpp>
#include <boost/compute/container/vector.hpp>
#include <boost/compute/utility/program_cache.hpp>
#include "multichannel/data_block.hpp"
#include "utility/opencl_source_generator.hpp"
#include "utility/opencl_type_name.hpp"
#include "utility/type_abbreviation.hpp"
#include "utility/opencl_logging.hpp"

namespace compute = boost::compute;

#ifdef _MSC_VER
	#pragma pack(push, 1)
#endif
struct channel_offset
{
   cl_uint channel;
   cl_uint offset;
};
#ifdef _MSC_VER
	#pragma pack(pop)
#endif

BOOST_COMPUTE_ADAPT_STRUCT(channel_offset, channel_offset, (channel, offset));

namespace interpolation
{

/**
 @brief Cubic-spline interpolating function using natural boundary conditions (second derivatives are zero at the edges)

 Takes N points, computes the cubic spline (smooth first and second derivatives) passing through them and returns (N*k - 1)
 points, where k is an interpolating factor controlling how many points are added for each original (support point)

 @tparam T data type supported
 */
template<typename T>
class cubic_spline
{
   compute::kernel kernel_;
   size_t n_;
   size_t fac_;
   size_t nci_;

public:
   using data_block_type = multichannel::data_block<T>;
   /**
    @brief Constructor

    @param ctx OpenCL context
    @param n number of interpolating points
    @param fac interpolating factor (how many points to add for each support point)
    */
   cubic_spline(size_t n, size_t fac)
      : n_{n}
      , fac_{fac}
      , nci_{(size_t)-1}
   {}

   /**
    @brief Applies the interpolation to one or more sets of points

    @param x Input vector
    @param off Offset in x of each set of points (each with length n)
    @param out Output vector (has to be of size of at least (n*fac - 1)*N, where N = length of the offset vector)
    */
   void operator()(data_block_type& x, compute::mapped_view<channel_offset>& off, size_t n, compute::vector<T>& out, compute::command_queue& q)
   {
      if (nci_ != x.dimensions().channels)
      {
         auto prog = program(q.get_context(), n_, fac_, x.dimensions().channels);
         kernel_ = compute::kernel{prog.first, prog.second};
         nci_ = x.dimensions().channels;
      }
      kernel_.set_arg(0, x.buffer().get_buffer());
      kernel_.set_arg<cl_ulong>(1, x.dimensions().samples);
      kernel_.set_arg(2, off.get_buffer());
      kernel_.set_arg(3, out);
      q.enqueue_1d_range_kernel(kernel_, 0, n, 1);
   }

   /**
    @brief Interpolating kernel source

    @param n Number of points to support
    @param fac Interpolating factor

    @return Kernel source
    */
   static std::pair<compute::program, std::string> program(compute::context ctx, size_t n, size_t fac, size_t nci)
   {
      std::stringstream name;
      name << "cubic_spline_interpolation_" << TYPE_ABBREVIATION(T) << "_" << n << "_" << fac;

      auto cache = compute::program_cache::get_global_cache(ctx);
      auto prog = cache->get(name.str(), "");
      if (prog)
         return std::make_pair(*prog, name.str());
      size_t nout = fac*(n - 1) + 1;

      opencl_source_generator src;
      src << compute::type_definition<channel_offset>() << "\n";
      src.kernel(name.str(), std::is_same<T, double>::value).open("(");
      // arguments
      src.parameter<T*>("y");
      src.parameter<cl_ulong>("ns");
      src << ", __global channel_offset* off";
      src.parameter<T*>("out");
      src.close(")").open("{");
      src.new_line() << "int i = get_global_id(0);";
      src.new_line() << "if (off[i].offset < 0 || ns - off[i].offset < " << n << ") return;";
      src.new_line() << opencl_type_name<T>() << " u[" << n << "];";
      src.new_line() << opencl_type_name<T>() << " y2[" << n << "];";
      src.new_line() << opencl_type_name<T>() << " p;";
      src.new_line() << "y2[0] = 0.;";
      src.new_line() << "y2[" << n - 1 << "] = 0.;";
      src.new_line() << "u[0] = 0.;";
      src.new_line() << "int j;";
      src.new_line() << "for(j = 1; j < " << n - 1 << "; ++j)";
      src.open("{");
      src.new_line() << "p = 0.5*y2[j - 1] + 2;";
      src.new_line() << "y2[j] = -0.5 / p;";
      std::string ybeg = "y[(off[i].offset";
      std::stringstream ss;
      ss << ")*" << nci << " + off[i].channel]";
      std::string yend = ss.str();
      src.new_line() << "u[j] = (" << ybeg << " + j + 1" << yend << " - " << ybeg << " + j" << yend << ") - (" << ybeg << " + j" << yend << " - " << ybeg <<  " + j - 1 " << yend << ");";
      src.new_line() << "u[j] = (2.*u[j] - 0.5*u[j - 1])/p;";
      src.close("}");
      src.new_line() << "for(j = " << n - 2 << "; j >= 0; --j)";
      src.open("{");
      src.new_line() << "y2[j] = y2[j]*y2[j + 1] + u[j];";
      src.close("}");
      src.new_line() << "for(j = 0; j < " << n - 1 << "; ++j)";
      src.open("{");
      src.new_line() << "out[i*" << nout << " + j*" << fac << "] = " << ybeg << " + j" << yend << ";";
      T a, b;
      for (size_t i = 1; i < fac; ++i)
      {
         a = static_cast<T>(1. - ((T)i/fac));
         b = static_cast<T>(1. - (T)a);
         src.new_line() << "out[i*" << nout << " + j*" << fac << " + " << i << "] = "
                        << a << "*" << ybeg << " + j" << yend << " + " << b << "*" << ybeg << " + j + 1" << yend << " + (" << (a*a*a - a)
                        << "*y2[j] + " << (b*b*b - b) << "*y2[j + 1])/6.;";
      }
      src.close("}");
      src.new_line() << "out[(i + 1)*" << nout << " - 1] = " << ybeg << " + " << n-1 << yend << ";";
      src.close("}");

      opencl_log_kernel(src.str());
      compute::program prgm;

      try
      {
         prgm = compute::program::create_with_source(src.str(), ctx);
         prgm.build();
         cache->insert(name.str(), "", prgm);
      }
      catch(std::exception &)
      {
         // program failed to compile, print out the build log
         std::cerr << prgm.build_log() << std::endl;
      }
      return std::make_pair(prgm, name.str());
   }
};

} // namespace interpolation
#endif /* CUBIC_SPLINE_INTERPOLATION_HPP */
