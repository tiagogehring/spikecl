#ifndef BURST_DETECTOR_HPP
#define BURST_DETECTOR_HPP

#define BOOST_COMPUTE_DEBUG_KERNEL_COMPILATION

#include <vector>
#include <limits>
#include <sstream>
#include <boost/compute/core.hpp>
#include <boost/compute/algorithm/transform_if.hpp>
#include <boost/compute/algorithm/reduce_by_key.hpp>
#include "spikes/spikes.hpp"
#include "data_source/data_dimensions.hpp"
#include "burst.hpp"
#include "utility/opencl_source_generator.hpp"
#include "utility/opencl_logging.hpp"
#include "config/burst_detection_setup.hpp"

namespace compute = boost::compute;

#ifdef _MSC_VER
   #pragma pack(push, 1)
#endif
struct burst_detector_bin_col
{
   cl_uint active_sites;
   cl_uint spikes;
   cl_uint activity;
};
#ifdef _MSC_VER
   #pragma pack(pop)
#endif

BOOST_COMPUTE_ADAPT_STRUCT(burst_detector_bin_col, burst_detector_bin_col, (active_sites, spikes, activity));
BOOST_COMPUTE_ADAPT_STRUCT(burst, burst, (left_thresh_idx, right_thresh_idx, peak_idx, left_idx, right_idx, spikes, half_width));

template<typename T>
class burst_detector
{
public:
   using bursts_type = std::vector<burst>;
   using bin_statistics_type = std::vector<burst_detector_bin_col>;
   using binned_data_type = std::vector<unsigned>;

private:
   compute::context ctx_;

   burst_detection_setup::options opts_;
   size_t nbins_;
   
   // spike bins (size: bins x channels)
   binned_data_type bins_;
   
   // results from processing each bin   
   bin_statistics_type bins_col_;
     
   // dimensions of the input file
   data_source::data_dimensions dim_;
      
   // OpenCL kernel that processes a bin
   compute::kernel process_bin_kernel_;
   compute::kernel process_burst_kernel_;

public:      
   burst_detector(compute::context ctx, burst_detection_setup::options opts)
      : ctx_{ctx}            
      , nbins_{0}
      , opts_{opts}
   {}

   void reset(data_source::data_dimensions dim)
   {  
      dim_ = dim;               
      nbins_ = static_cast<size_t>(std::ceil(dim_.timespan()*1000. / opts_.bin_size) + 1.e-1);
      if (bins_.size() < nbins_*dim_.channels)
         bins_.resize(nbins_*dim_.channels);
   }

   const bin_statistics_type& bin_statistics() const
   {
      return bins_col_;
   }

   const size_t bins() const
   {
      return nbins_;
   }

   const binned_data_type& binned_data() const
   {
      return bins_;
   }
   
   void operator()(spikes<T>& spks, compute::command_queue& q)
   {
      assert(dim_.valid());
       
      const auto& vals = spks.spikes_list(q);
      q.finish(); // make sure that values are synchronized
      for (auto x = vals.begin(); x != vals.begin() + spks.count(); ++x)
      {
         if (x->status == static_cast<short>(spike_status::valid))
            bins_[(unsigned)(x->sample*1000. / dim_.sampling_rate / opts_.bin_size)*dim_.channels + x->channel] += 1;
      }      
   }

   bursts_type detect_bursts(compute::command_queue& q)
   {
      // check if we need to generate a new kernel
      {
         std::stringstream name;
         name << "burst_detector_process_bin_" << dim_.channels;

         if (!process_bin_kernel_.get() || process_bin_kernel_.name() != name.str())
         {
            process_bin_kernel_ = compute::kernel{ process_bin_program(q.get_context(), name.str(), dim_.channels), name.str() };
         }
      }

      if (bins_col_.size() < nbins_)
         bins_col_.resize(nbins_);

      // results from processing each bin      
      compute::mapped_view<unsigned> bins_map{ &bins_[0], bins_.size(), q.get_context() };
      compute::mapped_view<burst_detector_bin_col> bins_col_map{ &bins_col_[0], bins_col_.size(), q.get_context() };

      process_bin_kernel_.set_arg(0, bins_map.get_buffer());
      process_bin_kernel_.set_arg(1, bins_col_map.get_buffer());

      q.enqueue_1d_range_kernel(process_bin_kernel_, 0, nbins_, 1);

      // synchronize with the OpenCL world
      bins_col_map.map(q);
      q.finish();

      bursts_type bursts;

      size_t dburst = static_cast<size_t>(opts_.min_burst_distance / opts_.bin_size);
      // process this part sequentially      
      size_t ibin = 0;
      while (ibin < nbins_)
      {
         if (bins_col_[ibin].activity > opts_.threshold && bins_col_[ibin].active_sites >= opts_.active_sites)
         {
            // ok, look for the peak            
            burst b;
            b.left_thresh_idx = static_cast<cl_uint>(ibin);
            b.peak_idx = static_cast<cl_uint>(ibin); // peak index
            ++ibin;
            if (opts_.threshold < 1.)
            {
               // peak is considered the bin with the most number of active sites               
               while (ibin < nbins_ && bins_col_[ibin].active_sites > bins_col_[b.peak_idx].active_sites)
               {
                  b.peak_idx = static_cast<cl_uint>(ibin);
                  ++ibin;
               }
            }
            else
            {
               // use the maximum activity for the peak
               while (ibin < nbins_ && bins_col_[ibin].activity > opts_.threshold)
               {
                  if (bins_col_[ibin].activity > bins_col_[b.peak_idx].activity)
                     b.peak_idx = static_cast<cl_uint>(ibin);
                  ++ibin;
               }
            }            
            bursts.push_back(b);            
            ibin += dburst;
         }
         ++ibin;
      }

      {
         std::stringstream name;
         name << "burst_detector_process_burst_" << static_cast<int>(opts_.pelt_factor * 100) << "_" << static_cast<int>(dim_.sampling_rate);

         if (!process_burst_kernel_.get() || name.str() != process_burst_kernel_.name())
         {
            process_burst_kernel_ = compute::kernel{ process_burst_program(q.get_context(), name.str(), opts_.pelt_factor, dim_.sampling_rate), name.str() };
         }
      }

      if (!bursts.empty())
      {
         compute::mapped_view<burst> bursts_map{ &bursts[0], bursts.size(), q.get_context() };

         process_burst_kernel_.set_arg(0, bins_col_map.get_buffer());
         process_burst_kernel_.set_arg<cl_ulong>(1, bins_col_map.size());
         process_burst_kernel_.set_arg(2, bursts_map.get_buffer());         

         q.enqueue_1d_range_kernel(process_burst_kernel_, 0, bursts.size(), 1);

         bursts_map.map(q);  
      }      
      q.finish();
            
      return bursts;
   }
  
   static compute::program process_bin_program(compute::context ctx, const std::string& name, size_t nc)
   {
      opencl_source_generator src;

      // see if program already cached
      auto cache = compute::program_cache::get_global_cache(ctx);
      auto prog = cache->get(name, "");
      if (prog)
         return *prog;

      src << compute::type_definition<burst_detector_bin_col>();

      src.kernel(name, std::is_same<T, double>::value).open("(");
      // arguments
      src.parameter<cl_uint*>("spks");
      src << ", __global burst_detector_bin_col* bins";      
      src.close(")").open("{");
      src.new_line() << "ulong i = (ulong)get_global_id(0);";
      src.new_line() << "bins[i].spikes = 0;";
      src.new_line() << "bins[i].active_sites = 0;";
      src.new_line() << "ulong j;";            
      src.new_line() << "for(j = 0; j < " << nc << "; j++)";
      src.open("{");
      src.new_line() << "if (spks[i*" << nc << " + j] > 0)";
      src.open("{");
      src.new_line() << "bins[i].active_sites += 1;";
      src.close("}");
      src.new_line() << "bins[i].spikes += spks[i*" << nc << " + j];";
      src.close("}"); 
      src.new_line() << "bins[i].activity = bins[i].active_sites*bins[i].spikes;";      
      src.close("}");

      opencl_log_kernel(src.str());
      compute::program prgm;
      try
      {
         prgm = compute::program::create_with_source(src.str(), ctx);
         prgm.build();
         cache->insert(name, "", prgm);
      }
      catch (std::exception &)
      {
         // program failed to compile, print out the build log
         std::cout << prgm.build_log() << std::endl;
      }
      return prgm;
   }

   static compute::program process_burst_program(compute::context ctx, const std::string& name, double pelt_fac, double binw)
   {
      opencl_source_generator src;

      // see if program already cached
      auto cache = compute::program_cache::get_global_cache(ctx);
      auto prog = cache->get(name, "");
      if (prog)
         return *prog;

      src << compute::type_definition<burst_detector_bin_col>() << "\n";
      src << compute::type_definition<burst>();

      src.kernel(name, std::is_same<T, double>::value).open("(");
      // arguments      
      src << "__global burst_detector_bin_col* bins";      
      src << ", ulong n";
      src << ", __global burst* b";
      src.close(")").open("{");
      src.new_line() << "size_t i = (ulong)get_global_id(0);";
      src.new_line() << "b[i].spikes = 0;";
      src.new_line() << "uint thresh = (uint)(bins[b[i].peak_idx].activity*" << pelt_fac << ");";
      src.new_line() << "int j;";
      // search for left boundary
      src.new_line() << "for(j = (int)(b[i].peak_idx) - 1; bins[j].activity > thresh && j >= 0; --j);";
      src.new_line() << "b[i].left_thresh_idx = j;";      
      // search for right boundary
      src.new_line() << "for(j = (int)(b[i].peak_idx) + 1; bins[j].activity > j && j < n; ++j);";
      src.new_line() << "b[i].right_thresh_idx = j;";
      // search for left 50%
      src.new_line() << "size_t totl = 0;";
      src.new_line() << "for(j = b[i].left_thresh_idx; j < b[i].peak_idx; ++j)";
      src.open("{");
      src.new_line() << "totl += bins[j].spikes;";
      src.close("}");     
      src.new_line() << "size_t tmp = 0;";
      src.new_line() << "for(j = b[i].left_thresh_idx; j < b[i].peak_idx; ++j)";
      src.open("{");
      src.new_line() << "if(tmp + bins[j].spikes/2 > totl/2) break;";
      src.new_line() << "tmp += bins[j].spikes;";
      src.close("}");
      src.new_line() << "b[i].left_idx = j;";
      // right 50%
      src.new_line() << "size_t totr = 0;";
      src.new_line() << "for(j = b[i].right_thresh_idx; j > b[i].peak_idx; --j)";
      src.open("{");
      src.new_line() << "totr += bins[j].spikes;";
      src.close("}");
      src.new_line() << "tmp = 0;";
      src.new_line() << "for(j = b[i].right_thresh_idx; j > b[i].peak_idx; --j)";
      src.open("{");
      src.new_line() << "if(tmp + bins[j].spikes/2 > totr/2) break;";
      src.new_line() << "tmp += bins[j].spikes;";
      src.close("}");
      src.new_line() << "b[i].right_idx = min((ulong)(j + 1), n - 1);";
      // others
      src.new_line() << "b[i].half_width = 0.5*(b[i].right_idx - b[i].left_idx)*" << binw << ";";
      src.new_line() << "b[i].spikes = totr + totl + bins[b[i].peak_idx].spikes;";
            
      src.close("}");

      opencl_log_kernel(src.str());
      compute::program prgm;
      try
      {
         prgm = compute::program::create_with_source(src.str(), ctx);
         prgm.build();
         cache->insert(name, "", prgm);
      }
      catch (std::exception &)
      {
         // program failed to compile, print out the build log
         std::cout << prgm.build_log() << std::endl;
      }
      return prgm;
   }
};

#endif /* BURST_DETECTOR_HPP */
