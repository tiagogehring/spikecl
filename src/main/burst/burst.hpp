#ifndef BURST_HPP
#define BURST_HPP

#include <boost/compute/core.hpp>

#ifdef _MSC_VER
   #pragma pack(push, 1)
#endif
struct burst
{
   // extremes of the burst
   cl_uint left_thresh_idx;
   cl_uint right_thresh_idx;
   // centre
   cl_uint peak_idx;
   // boundaries of the burst (where activity < 5% of the peak one)
   cl_uint left_idx;
   cl_uint right_idx;
   // other statistics
   cl_uint spikes;
   double half_width;
};
#ifdef _MSC_VER
   #pragma pack(pop)
#endif

#endif /* BURST_HPP */