#ifndef MEA_DATA_HPP
#define MEA_DATA_HPP

/**
 * @file   mea_data.hpp
 * @author Tiago Gehring
 * @date   January, 2014
 * @brief  Multi-electrode data processing functions
 */

#include <string>
#include <functional>
#include <type_traits>
#include <memory>
#include <future>
#include <map>
#include <chrono>
#include <boost/compute/core.hpp>
#include "multichannel/analog_data.hpp"
#include "spikes/spike_detector.hpp"
#include "dsp/bidirectional_filter.hpp"
#include "utility/type_traits.hpp"
#include "data_source/data_dimensions.hpp"
#include "config/common_setup.hpp"
#include "config/filter_setup.hpp"
#include "burst/burst_detector.hpp"

namespace compute = boost::compute;

using namespace dsp;

/**
 @brief MEA data processing operation
 */
enum class mea_data_operation
{
   read_data,
   filter_data,
   detect_spikes,
   detect_bursts,
   all_operations
};

/**
 @brief MEA data processing operation status
 */
enum class mea_data_operation_status
{
   start, // operation started
   block_start, // processing of data block started
   block_finish, // processing of data block finished
   finish // operation completed (all blocks have been processed)
};

/**
 @brief Implements a paralled processing pipeline where MEA data blocks are read and others processed at the same time

 @tparam T Data type supported
 @tparam DataSource Data source type, provides means to extract/read the data
 @tparam Filter Filter type
 */
template< typename T
        , typename DataSource
        , typename Filter = dsp::bidirectional_filter< typename DataSource::data_type, T >
> class mea_data
{
public:
   using input_type = typename DataSource::data_type;
   using output_type = T;
   using data_source_type = DataSource;
   using filter_type = Filter;
   using filter_kernel_type = dsp::filter_kernel<input_type, output_type>;
   using data_dimensions = data_source::data_dimensions;

   /**
    @brief Data processing options
    */
   struct options
   {
      common_setup::options common; //!< Basic options, such as OpenCL device to be used and channels to be processed
      spike_detection_setup::options spike_detection; //!< Spike detection options      
      burst_detection_setup::options burst_detection; //!< Burst detection options
   };

   ///@ Callback function prototype (operation type, initiial channel, final channel, initial sample, final sample, start flag)
   using notification_callback = std::function<void(mea_data_operation, mea_data_operation_status, const data_dimensions&, size_t)>;

   using input_buffer_type = multichannel::analog_data<input_type>;
   using output_buffer_type = multichannel::analog_data<output_type>;

   using spike_detector_type = spike_detector<T>;
   using burst_detector_type = burst_detector<T>;

   using bursts_type = typename burst_detector<T>::bursts_type;

   ///@ Callback of function prototype to be called when processing of a data block was completed
   using block_finished_callback = std::function<void(spikes<T>*)>;

private:
   // OpenCL objects
   compute::device dev_;
   compute::context ctx_;
   compute::command_queue queue_;

   // Data processing options
   options opt_;
   // maximum number of samples per data chunk
   size_t max_ns_;
   // need 2 input buffers to allow for parallel reading and processing
   input_buffer_type in_buf_;
   input_buffer_type in_buf2_;
   // also need 2 output buffers (if using a two-stage - bidirectional - filter)
   output_buffer_type int_buf_; // buffer with output of 1st stage of the filter
   output_buffer_type out_buf_;
   // filter
   std::unique_ptr<filter_type> filter_;
   // spike and burst detector
   spike_detector_type spk_det_;
   burst_detector_type burst_det_;
   // stores time spent with each operation
   using operation_statistics_type = std::pair<size_t, size_t>; // time in microseconds, number of samples
   std::map<mea_data_operation, operation_statistics_type> op_stat_;
   // callback function for progress updates
   notification_callback stat_f_;

public:
   
   /**
    @brief Constructor

    @tparam Data type to be supported
    @tparam Dev OpenCL Device iniitialization parameter type
    */
   template< typename Dev
           , REQUIRES( is_constructible_<compute::device, Dev>() )
   > mea_data(Dev&& dev, options opt)
      : dev_{std::forward<Dev>(dev)}
      , ctx_{dev}
      , queue_{ctx_, dev_}
      , opt_(opt)
         // maximum number of samples per chunk - the partitioning of all the buffers bellow should be the same
         // so base the size on the number of samples - and not bytes for example
      , max_ns_{dev_.get_info<size_t>(CL_DEVICE_MAX_MEM_ALLOC_SIZE)/sizeof(T)}
      , in_buf_{ctx_, opt_.common.vector_width, max_ns_}
      , in_buf2_{ctx_, opt_.common.vector_width, max_ns_}
      , int_buf_{ctx_, opt_.common.vector_width, max_ns_}
      , out_buf_{ctx_, opt_.common.vector_width, max_ns_}
      , spk_det_{ ctx_
                , opt_.common.vector_width
                , static_cast<T>(opt_.spike_detection.threshold)
                , opt_.spike_detection.refractory_time
                , opt_.spike_detection.spike_sign }
      , burst_det_{ ctx_                
                  , opt_.burst_detection }
   {}

   /**
    @brief Destructor
    */
   ~mea_data()
   {
   }

   const output_buffer_type& output_buffer() const
   {
      return out_buf_;
   }

   /**
    @brief Default option values

    @return Option values
    */
   static options default_options()
   {
      static options def_opt{
         common_setup::default_values,
         spike_detection_setup::default_values
      };

      return def_opt;
   }

   /**
    @return Return current option values
    */
   const options& options_values() const
   {
      return opt_;
   }

   /**
    @brief OpenCL device context

    @return Device context
    */
   compute::context opencl_context()
   {
      return ctx_;
   }

   /**
    @brief OpenCL command queue

    @return OpenCL command queue
    */
   compute::command_queue& opencl_queue()
   {
      return queue_;
   }

   /**
    @brief Sets callback function to be called when processing status was changed

    @param f Callable function object
    */
   template<typename F>
   void on_notification(F&& f)
   {
      stat_f_ = std::forward<F>(f);
   }

   /**
    @brief Enable data filtering

    @param coeff
    @param ds
    */
   void enable_filter(const dsp::filter_kernel_coefficients& coeff, const DataSource& ds)
   {     
      // filter kernel options: these depend on the data source properties; two options are optionally set:
      // i) if the data is not in the "row major" format (@ref sec_data_input_format) then the filter
      //    will "transpose" the output so that the data will be converted to a row major format after the first filter stage
      // ii) if the input data type are raw AD values then the filter will also offset/scale the values to get the value in volts
      const static int kernel_opts =
         ( data_source_type::data_format == data_source::storage_format::row_major ? 0 : static_cast<int>(dsp::filter_kernel_options::transpose_output) ) |
         ( std::is_integral<input_type>::value ? static_cast<int>(dsp::filter_kernel_options::scale_input) : 0 );

      filter_kernel_type kern{coeff, opt_.common.vector_width, kernel_opts, ds.reference_value(), static_cast<T>(ds.scalling_factor())};
      filter_.reset(new filter_type{ctx_, kern});
   }

   void disable_filter()
   {
      filter_.reset();
   }

   /**
    @brief Main processing function; processes all data from a data source

    @tparam F callback function type
    @param ds data source
    @param spks where to store spikes that were found
    @param blk_call_bk callback function to call when a processing of a data block was completed
    */
   template<typename F>
   void process(DataSource& ds, spikes<T>* spks, bursts_type* bursts, F&& blk_call_bk)
   {
      namespace chrono = std::chrono;

      block_finished_callback block_f{std::forward<F>(blk_call_bk)};

      _reset();

      size_t nchan = opt_.common.channels;
      if (nchan == (size_t)-1 || nchan == 0)
      {
         // process until end of file
         nchan = ds.dimensions().channels - opt_.common.channel_offset;
      }

      // data source dimensions and properties
      const auto sr = ds.dimensions().sampling_rate;
      const auto dsf = ds.dimensions().format;
      auto ns = ds.dimensions().samples;
      // see if we have to set the position in the file
      size_t start_off = static_cast<size_t>(opt_.common.start_time*sr);
      if (start_off > 0.)
      {
         if (start_off > ns)
            throw std::runtime_error{"Invalid file offset"};
         ds.position(start_off);
         ns -= start_off;
      }
      // number already read
      size_t nr = 0;
      // number already processed (filtered / spike detected)
      size_t np = 0;
      // input buffers that will be swapped everytime that a buffer is read
      // to allow for parallel readind and processing
      input_buffer_type* in_buf = &in_buf_;
      input_buffer_type* flt_buf = &in_buf2_;
      // number of iterations
      size_t iter = 0;
      
      data_dimensions sel_dim{opt_.common.channel_offset, nchan, 0, ns, dsf, sr};

      burst_det_.reset(sel_dim);

      do
      {
         // read data asynchronously
         auto read_fut = std::async( std::launch::async, [&](){
            /**
             * Read data from file into in_buf
             */
            if (nr == 0)
               _status_notify(mea_data_operation::read_data, mea_data_operation_status::start, ds.dimensions());
            if (nr < ns)
            {
               // number of samples to read - at most one full block
               auto r = std::min(ns - nr, static_cast<size_t>(opt_.common.block_size*sr));
               // progress notification: start block
               data_dimensions dim{0, ds.dimensions().channels, nr + start_off, r, dsf, sr};
               _status_notify(mea_data_operation::read_data, mea_data_operation_status::block_start, dim);
               auto ti = chrono::high_resolution_clock::now();
               // perform actual read
               r = in_buf->read(ds, r, queue_);
               // operation time
               size_t dt = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - ti).count();
               _update_statistics(mea_data_operation::read_data, dt, r*dim.channels);
               // progress notification
               _status_notify(mea_data_operation::read_data, mea_data_operation_status::block_finish, dim, dt);
               nr += r;
               if (nr >= ns)
                  _status_notify(mea_data_operation::read_data, mea_data_operation_status::finish, ds.dimensions());
            }
         });

         // from the second iteration onwards to the filtering/spike detection in parallel with the reading
         if (iter > 0)
         {
            /**
             * Process buffer flt_buf_
             */
            // process blocks of channel at a time
            size_t co = opt_.common.channel_offset;
            bool first = (iter == 1);
            do
            {
               // number of real channels to process
               size_t nc = std::min(opt_.common.parallel_channels, opt_.common.channel_offset + nchan - co);
               // number of work items or blocks of channels to processd (dependent on the SIMD width)
               size_t wi = nc / opt_.common.vector_width;
               if (nc % opt_.common.vector_width != 0)
                  wi += 1;

               // (optionally) filter the data
               if (filter_)
               {
                  if (first)
                     _status_notify(mea_data_operation::filter_data, mea_data_operation_status::start, sel_dim);
                  data_dimensions dim{co, nc, start_off + np, flt_buf->dimensions().samples, dsf, sr};
                  // progress notification - operation startings
                  _status_notify(mea_data_operation::filter_data, mea_data_operation_status::block_start, dim);
                  auto ti = chrono::high_resolution_clock::now();
                  // this does the filtering
                  queue_.finish();
                  _apply_filter<filter_type>(*flt_buf, co, nc, first);
                  queue_.finish();
                  size_t dt = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - ti).count();
                  _update_statistics(mea_data_operation::filter_data, dt, out_buf_.dimensions().samples);
                  _status_notify(mea_data_operation::filter_data, mea_data_operation_status::block_finish, dim, dt);
               }
               else
               {
                  // if not filtering just copy data from input to output buffer, scalling it if necessary
                  flt_buf->scale(out_buf_, queue_);
               }

               // from here on we use the output buffer
               data_dimensions dim{ co, nc, start_off + np, out_buf_.dimensions().samples, dsf, sr };

               if (spks != nullptr)
               {
                  // the fun part: detect spikes

                  if (first)
                     _status_notify(mea_data_operation::detect_spikes, mea_data_operation_status::start, sel_dim);
                  // @TODO: move the code below (processing of chunks) elsewhere
                  // notify that operation is starting
                   _status_notify(mea_data_operation::detect_spikes, mea_data_operation_status::block_start, dim);

                  // first use all the data to estimate the spike threshold
                  auto ti = chrono::high_resolution_clock::now();
                  spk_det_.update_threshold(out_buf_.data()[0], co, nc, true, queue_);
                 
                  queue_.finish();
                  size_t dt = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - ti).count();

                  // now really detect the spikes - process each data chunk separatedly
                  for(auto& x : out_buf_.data())
                  {
                     ti = chrono::high_resolution_clock::now();
                     // pass the filtered (or not) data to the spike detector
                     // output will be saved in the spks variable
                     spk_det_(x, co, nc, *spks, false, queue_);
                     // important: wait for OpenCL kernels to finish running
                     // otherwise we might end up with partial results                     
                     queue_.finish();
                     dt += chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - ti).count();
                     // notify that a block is finished and that we found spikes
                     if (spks->count() > 0)
                     {
                        block_f(spks);
                        if (bursts)
                           burst_det_(*spks, queue_);
                     }
                  }
                
                  _update_statistics(mea_data_operation::detect_spikes, dt, out_buf_.dimensions().samples);
                  _status_notify(mea_data_operation::detect_spikes, mea_data_operation_status::block_finish, dim, dt);                  
               }

               first = false;
               co += opt_.common.parallel_channels;

               // notify that we are done with the block
               _status_notify(mea_data_operation::all_operations, mea_data_operation_status::block_finish, dim);
            } while(co < opt_.common.channel_offset + nchan);
                      
            np += out_buf_.dimensions().samples;            
         }
         // make sure that all OpenCL operations are done
         queue_.finish();
         // synchronize threads - wait for read to finish
         read_fut.get();
         // swap the read/filter buffers for the next iteration
         std::swap(in_buf, flt_buf);
         ++iter;
      } while (np < ns);

      // burst detection: only at the very end
      if (spks->count() > 0)
      {
         _status_notify(mea_data_operation::detect_bursts, mea_data_operation_status::block_start, ds.dimensions());
         if (bursts)
            *bursts = burst_det_.detect_bursts(queue_);
         _status_notify(mea_data_operation::detect_bursts, mea_data_operation_status::block_finish, ds.dimensions());
      }

      // notifiy that processing operations are done
      if (filter_)
         _status_notify(mea_data_operation::filter_data, mea_data_operation_status::finish, sel_dim);
      if (spks)
         _status_notify(mea_data_operation::detect_spikes, mea_data_operation_status::finish, sel_dim);
   }

   operation_statistics_type operation_statistics(mea_data_operation op) const
   {
      auto stat = op_stat_.find(op);
      if (stat == op_stat_.end())
         return operation_statistics_type{};
      else
         return stat->second;
   }

   void clear_statistics()
   {
      op_stat_.clear();
   }

   const spike_detector_type& spike_det() const
   {
      return spk_det_;
   }

   const burst_detector_type& burst_det() const
   {      
      return burst_det_;
   }

private:
   void _status_notify(mea_data_operation op, mea_data_operation_status st, const data_dimensions& dim, size_t tm_us = 0)
   {
      if (stat_f_)
      {
         stat_f_(op, st, dim, tm_us);
      }
   }

   void _update_statistics(mea_data_operation op, size_t us, size_t ns)
   {
      auto stat = op_stat_.find(op);
      if (stat == op_stat_.end())
         op_stat_[op] = std::make_pair(us, ns);
      else
         op_stat_[op] = std::make_pair(stat->second.first + us, stat->second.second + ns);
   }

   template< typename _Filter
           , REQUIRES( is_bidirectional_filter<_Filter>::value )
   > void _apply_filter(input_buffer_type& buf, size_t co, size_t nc, bool first)
   {
      if (first)
      {
         // allocate buffers with same sizes as the input ones
         int_buf_.reshape(buf, nc);
         out_buf_.reshape(buf, nc);
      }
      (*filter_)(buf.data()[0], co, nc, int_buf_.data()[0], out_buf_.data()[0], true, queue_);
   }

   // Default template funciton parameter not suported by all compilers
   // Have to explicitly deactivate one overload otherwise we end up with two functions with the
   // same signature and this is not good indeed
#ifndef DUMMY_REQUIRES
   template< typename _Filter
           , REQUIRES( is_filter<_Filter>::value && !is_bidirectional_filter<_Filter>::value )
   > void _apply_filter(input_buffer_type& buf, size_t co, size_t nc, bool first)
   {
      if (first)
      {
         // allocate buffers (no need for an intermediate buffer here)
         out_buf_.reshape(buf, nc);
      }
      (*filter_)(buf.data()[0], co, nc, out_buf_.data()[0], true, queue_);
   }
#endif

   void _reset()
   {
      if (filter_)
         filter_->reset();

      spk_det_.reset();
   }
};

#endif /* MEA_DATA_HPP */
