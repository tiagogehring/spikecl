#ifndef SPIKES_STORE_HPP
#define SPIKES_STORE_HPP

/**
 * @file   spikes_store.hpp
 * @author Tiago Gehring
 * @date   January, 2014
 * @brief  Spike storing functions
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <map>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <memory>
#include <boost/filesystem.hpp>
#include "spikes/spikes_smoothed_waveform.hpp"
#include "spikes/spike_artifact_detector.hpp"
#include "config/spike_detection_setup.hpp"

namespace fs = boost::filesystem;

/**
 @brief Stores spikes and waveforms in the disk

 @tparam T data type supported
 */
template<typename T>
class spikes_store
{
public:
   using electrode_map_type = std::vector<size_t>;
   using spike_counts_type = std::vector<size_t>;
   using spikes_container_type = spikes_smoothed_waveform<T, spike_artifact_detector<T>>;
   using storage_spike_type = typename spikes_container_type::spike_type;

private:
   // used internally only to temporarily store spikes for later ordering
   struct spike
   {
      double time_ms;
      double peak;
   };

   // path prefix where to store files to
   fs::path prefix_;
   // spike detection/storage options
   spike_detection_setup::options opts_;
   // output file where to store the spikes
   std::ofstream spks_f_;
   // where to store waveforms (single file)...
   std::ofstream spks_wf_f_;
   // ... or individual channels
   using waveforms_files = std::map<size_t, std::ofstream*>;
   waveforms_files spks_wf_chan_;
   // storage for spikes - where spikes land after being detected
   spikes_container_type spks_;
   // this will cache the spikes for a set of channels so that they can be written out afterwards
   using spikes_buffer_type = std::vector< std::vector<spike> >;
   // temporary internal storage for spikes to allow to order them by channel/electrode later
   spikes_buffer_type spk_buf_;
   // time of a sample, in ms
   double ts_ms_;
   // maps channels to electrode numbers
   electrode_map_type elect_map_;
   // number of spikes for each channel
   spike_counts_type nspk_chan_;

public:
   /**
    @brief Constructor

    @param q
    @param nmax
    @param pt_pre
    @param pt_post
    @param max_thresh
    @param ord
    */
   spikes_store(size_t nmax, spike_detection_setup::options opts)
      : opts_(opts)
      , spks_{nmax, opts_.points_pre, opts_.points_post, opts_.smoothing_factor, opts_.spike_sign, static_cast<T>(opts_.max_threshold)}
   {}

   /**
    @brief Destructor
    */
   ~spikes_store()
   {
      _clear();
   }

   /**
    @return Spike storage object (to be passed to the spike detector)
    */
   spikes_container_type* spikes()
   {
      return &spks_;
   }

   /**
    @return Spikes per channel
    */
   const spike_counts_type& spikes_count() const
   {
      return nspk_chan_;
   }

  /**
   @brief Initializes/resets the storage

   @param fn File name where to store spikes
   @param nc Number of channels supported
   @param sr Sampling rate
   @param elec_map Channel to electrode mapping
   */
   void reset(fs::path prefix, size_t nc, double sr, electrode_map_type elec_map)
   {
      // flush pending spikes
      flush();
      _clear();

      ts_ms_ = 1000./sr;
      elect_map_ = std::move(elec_map);
      prefix_ = prefix;
      if (nspk_chan_.size() != nc)
         nspk_chan_.resize(nc);
      else
         std::fill(nspk_chan_.begin(), nspk_chan_.end(), 0);

      if (opts_.spike_order != spike_ordering::none)
      {
         // if we want to sort the spikes by electrode number we have to store them all first
         for(auto& x : spk_buf_)
            x.clear();
         spk_buf_.resize(nc);
      }

      // initialize spikes file if so desired
      if (opts_.spike_format != spike_file_format::none)
      {
         auto fn = prefix;
         fn += "_spikes.txt";
         spks_f_.open(fn.string(), std::ios::trunc);
         if (!spks_f_)
         {
            std::stringstream ss;
            ss << "Could not open '" << fn << "' file";
            throw std::runtime_error{ss.str()};
         }
         spks_f_ << std::fixed << std::setprecision(6);
         if (opts_.spike_format == spike_file_format::standard)
         {
            // add a small header
            spks_f_ << "Time (ms),Electrode name,Channel,Peak (uV)" << std::endl;
         }
      }

      if (opts_.waveforms_storage == spike_waveforms_storage::single_file)
      {
         auto fn = prefix;
         fn += "_waveforms.csv";
         spks_wf_f_.open(fn.string(), std::ios::trunc);
         if (!spks_wf_f_)
         {
            std::stringstream ss;
            ss << "Could not open '" << fn << "' file";
            throw std::runtime_error{ss.str()};
         }
      }
   }

   void operator()(::spikes<T>* spks)
   {
      // the pointer should be the same
      assert(spks == &spks_);

      // process all spikes
      for(auto& spk : spks_)
      {
         if (spk.status() == spike_status::valid)
         {
            nspk_chan_[spk.channel()] += 1;
            if (opts_.spike_format != spike_file_format::none)
            {
               if (opts_.spike_order == spike_ordering::none)
               {
                  // write spike directly to file
                  _write_spike(spk);
               }
               else
               {                 
                  spk_buf_[spk.channel()].push_back(spike{ (spk.sample() + 1)*ts_ms_, spk.peak() } );
               }
            }

            // waveforms
            if (opts_.waveforms_storage != spike_waveforms_storage::none)
            {
               _write_waveform(spk);
            }
         }
      }
   }

   void flush()
   {
      if (opts_.spike_order == spike_ordering::electrode_number)
      {
         // sort by electrode (and not channel) number
         auto comp_f = [&](size_t x, size_t y) { return elect_map_[x] < elect_map_[y]; };
         std::vector<size_t> ordering(elect_map_.size());
         for (size_t i = 0; i < elect_map_.size(); ++i)
            ordering[i] = i;
         std::sort(ordering.begin(), ordering.end(), comp_f);
         for (auto o : ordering)
         {
            for(auto& spk : spk_buf_[o])
               _write_spike(spk, o);
         }
      }
      else
      {
         // already sorted by channel, just write all spikes
         for(size_t i = 0; i < spk_buf_.size(); ++i)
         {
            for(auto& spk : spk_buf_[i])
               _write_spike(spk, i);
         }
      }
   }

private:
   void _write_spike(const storage_spike_type& spk)
   {
      _write_spike(spike{ (spk.sample() + 1)*ts_ms_, spk.peak() }, spk.channel());
   }

   void _write_spike(const spike& spk, size_t chan)
   {
      int sign = spk.peak >= 0. ? 1 : -1;
      assert(spks_f_);
      switch (opts_.spike_format)
      {
      case spike_file_format::standard:
         spks_f_ << spk.time_ms << ","            
            << elect_map_[chan] << ","
            << (chan + 1) << ","
            << spk.peak*1.e6 << "," <<
            std::endl;
         break;
      case spike_file_format::legacy:
         spks_f_ << spk.time_ms << " "
            << sign*static_cast<int>(elect_map_[chan]) << " "
            << sign*(static_cast<int>(chan) + 1) << std::endl;
         break;
      default:
         assert(false);
      }
   }
   
   void _write_waveform(const storage_spike_type& spk)
   {
      switch(opts_.waveforms_storage)
      {
         case spike_waveforms_storage::single_file:
         {
            assert(spks_wf_f_);
            spks_wf_f_ << (spk.channel() + 1) << ","
                       << elect_map_[spk.channel()] << ","
                       << spk.sign()*ts_ms_*(spk.sample() + 1);
            // waveform
            if (opts_.smoothing_factor > 0)
            {
               for (auto it = spk.smoothed_waveform_begin(); it != spk.smoothed_waveform_end(); ++it)
                  spks_wf_f_ << "," << *it;
            }
            else
            {
               for (auto it = spk.waveform_begin(); it != spk.waveform_end(); ++it)
                  spks_wf_f_ << "," << *it;
            }
            spks_wf_f_ << std::endl;
            break;
         }
         case spike_waveforms_storage::one_per_channel:
         {
            // check if we already have a file open for this channel
            auto pf = spks_wf_chan_.find(spk.channel());
            if (pf == spks_wf_chan_.end())
            {
               // open file
               auto fn = prefix_;
               std::stringstream ss;
               ss << "_chan_x" << elect_map_[spk.channel()] << "_spikes.csv";
               fn += ss.str();
               std::ofstream* f = new std::ofstream{};
               f->open(fn.string());
               if (!(*f))
               {
                  ss.str("");
                  ss << "Error opening file '";
                  ss << fn.string();
                  ss << "'";
                  throw std::runtime_error{ ss.str() };
               }
               auto ip = spks_wf_chan_.insert(std::make_pair(spk.channel(), f));
               pf = ip.first;
            }

            *pf->second << (spk.channel() + 1) << ","
                        << elect_map_[spk.channel()] << ","
                        << spk.sign()*ts_ms_*(spk.sample() + 1);
            // waveform
            if (opts_.smoothing_factor > 0)
            {
               for (auto it = spk.smoothed_waveform_begin(); it != spk.smoothed_waveform_end(); ++it)
                  spks_wf_f_ << "," << *it;
            }
            else
            {
               for (auto it = spk.waveform_begin(); it != spk.waveform_end(); ++it)
                  spks_wf_f_ << "," << *it;
            }
            *pf->second << std::endl;
            break;
         }
         default:
            assert(false && "ugh");
      }
   }

   void _clear()
   {
      for (auto p : spks_wf_chan_)
      {
         if (p.second)
            delete p.second;
      }
      spks_wf_chan_.clear();
   }
};

#endif /* SPIKES_STORE_HPP */
