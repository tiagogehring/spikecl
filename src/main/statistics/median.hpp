#ifndef MULTICHANNEL_MEDIAN_HPP
#define MULTICHANNEL_MEDIAN_HPP

/// @addtogroup Statistics
///@{

/**
 * @file   median.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Median computation of multi-channel data
 */

#include <boost/compute/core.hpp>
#include <boost/compute/algorithm/fill.hpp>
#include <boost/compute/container/vector.hpp>
#include <boost/compute/utility/program_cache.hpp>

#include "mean_stddev.hpp"
#include "multichannel/data_block.hpp"
#include "utility/opencl_source_generator.hpp"
#include "utility/opencl_type_name.hpp"
#include "utility/opencl_constant.hpp"
#include "utility/opencl_logging.hpp"

namespace compute = boost::compute;

namespace statistics
{

/**
 @brief Implements a multi-channel median computation

 Median is computed by binning input data into N discrete value ranges.

 @tparam T data type supported
 */
template<typename T = double>
class median
{
   // OpenCL kernels (functions)
   compute::kernel kernel_bindata_;
   compute::kernel kernel_median_;
   // Need to compute also the SD for estimate of the bin width
   mean_stddev<T> m_;
   // Number of bins for each channel
   compute::vector<cl_ulong> bins_;
   // Median estimate
   compute::vector<T> md_;
   // Number of points for each channel
   compute::vector<cl_ulong> n_;
   // Total number of input channels
   size_t nci_;
   // Number of channels per block
   size_t ncb_;
   // Number of bins
   size_t nbin_;
   // If true then absolute values are taken before computing any statistics
   bool abs_;
   // SIMD vector width
   size_t vw_;
   // Update flag - if true median estimate has to be recomputed
   bool upd_;
   // Reset flag
   bool reset_;

public:
   using data_block_type = multichannel::data_block<T>;

   /**
    @brief Constructor

    @param q OpenCL context
    @param vw SIMD vector width
    @param nbin Number of bins to use for the median computation
    @param abs If true absolute values are taken before computing the mean
    */
   median(compute::context ctx, size_t vw, size_t nbin = 100000, bool abs = false)
      : m_{ctx, vw, abs}
      , bins_(ctx)
      , md_(ctx)
  	   , n_(ctx)
      , nci_{(size_t)-1}
      , ncb_{(size_t)-1}
      , nbin_{nbin}
      , abs_{abs}
      , vw_{vw}
      , upd_{true}
      , reset_{true}
   {}

   /**
    @brief Resets internalt state and median estimate
    */
   void reset()
   {
      reset_ = true;
      m_.reset();
   }
   ///@{

   /**
    @brief Processes a data buffer and updates median estimate

    @param x Input data buffer
    @param all If true all linked data blocks will be processed
    @param upd_mean True if standard deviation and mean estimates are to be updated as well
    @param co First channel offset
    @param nc Number of channels to process
    */
   void operator()(data_block_type& x, bool all, compute::command_queue& q, bool upd_mean = true)
   {
      const auto& dim = x.dimensions();
      (*this)(x, dim.channel_offset, dim.channels, upd_mean, q);
   }

   /**
    @brief Updates median value based on an inpute vector

    @param x Input data block
    @param co Channel offset
    @param nc Number of channels
    @param all If true all linked data blocks will be processed
    @param q OpenCL command queue
    @param upd_mean If true mean value will be updated
    */
   void operator()(data_block_type& x, size_t co, size_t nc, bool all, compute::command_queue& q, bool upd_mean = true)
   {
      // if we have multiple blocks first update the mean using all of them
      // this is to make the results consistent ovr different data partitioning
      if (all && upd_mean)
      {
         m_(x, co, nc, q);
         auto blk = x.next();
         while(blk != nullptr)
         {
            m_(*blk, co, nc, q);
            blk = blk->next();
         }
         upd_mean = false;
      }

      _process_impl(x, co, nc, upd_mean, q);
      if (all)
      {
         auto blk = x.next();
         while(blk != nullptr)
         {
            _process_impl(*blk, co, nc, upd_mean, q);
            blk = blk->next();
         }
      }
   }
   ///@}

   /**
    @brief Returns the computed median

    @return boost compute vector of median values (one for each channel)
    */
   const compute::vector<T>& median_value(compute::command_queue& q)
   {
      if (upd_)
      {
         // update median
         kernel_median_.set_arg(0, n_);
         kernel_median_.set_arg(1, bins_);
         kernel_median_.set_arg(2, m_.mean());
         kernel_median_.set_arg(3, m_.standard_deviation(q));
         kernel_median_.set_arg(4, md_);
         // note: no explicit SIMD vectorization for the median computation kernel, too much branching
         auto ev = q.enqueue_1d_range_kernel(kernel_median_, 0, nci_, 1);
         ev.wait();
         upd_ = false;
      }
      return md_;
   }

private:
   // low-level processing function
   compute::event _process_impl(data_block_type& x, size_t co, size_t nc, bool upd_mean, compute::command_queue& q)
   {
      const auto ns = x.dimensions().samples;
      // total number of channels
      const auto nci = x.parent_dimensions().channels;
      // channels per block
      const auto ncb = x.dimensions().channels;

      assert(nc % vw_ == 0 && "SIMD instruction width not compatible with number of channels");
      assert( (co == 0 || co % vw_ == 0) && "SIMD instruction width not compatible with channel offset");

      if (reset_)
      {
         if (ncb_ != ncb)
         {
            std::stringstream name;
            bool partial = nc < ncb; // partial input if we are taking only a subset of data
            name << "median_bin_" << ncb << "_" << vw_ << "_" << nbin_ << (abs_ ? "_abs" : "") << (partial ? "_partial" : "");
            kernel_bindata_ = compute::kernel{bindata_program(q.get_context(), name.str(), ncb, partial, 1 /*vw_*/, nbin_, abs_), name.str()};
            ncb_ = ncb;
         }
         if (nci_ != nci)
         {
            bins_.resize(nci*nbin_, q);
            n_.resize(2*nci, q);
            md_.resize(nci, q);
            // median kernel needs to be generated only once as it does not depend on other input parameters
            if (nci_ == (size_t)-1)
            {
               std::stringstream name_med;
               name_med << "median_" << vw_ << "_" << nbin_;
               kernel_median_ = compute::kernel{median_program(q.get_context(), name_med.str(), nbin_), name_med.str()};
            }
            nci_ = nci;
         }
         compute::fill(n_.begin(), n_.end(), 0, q);
         compute::fill(md_.begin(), md_.end(), 0, q);
         compute::fill(bins_.begin(), bins_.end(), 0, q);
         reset_ = false;
      }
      // update mean and SD if not done already
      if (upd_mean)
         m_(x, co, nc, q);

      // update values
      kernel_bindata_.set_arg<cl_ulong>(0, ns);
      kernel_bindata_.set_arg(1, x.buffer().get_buffer());
      kernel_bindata_.set_arg(2, n_);
      kernel_bindata_.set_arg(3, bins_);
      kernel_bindata_.set_arg(4, m_.mean());
      kernel_bindata_.set_arg(5, m_.standard_deviation(q));
      upd_ = true;
      return q.enqueue_1d_range_kernel(kernel_bindata_, co, nc, 1);
   }

   /**
    @brief Generates OpenCL kernel that computes the median from the binned data

    @param ctx OpenCL context
    @param name OpenCL kernel name
    @param nci Total number of input channels
    @param mult_blk If false it is assumed that the offset of data in the input buffer is always zero
    @param vw SIMD vector width
    @param abs If true absolute values are taken before processing the buffer

    @return boost compuite program object
    */
   static compute::program bindata_program(compute::context ctx, const std::string& name, size_t ncb, bool partial, size_t vw, size_t nbin, bool abs)
   {
      assert(ncb % vw == 0 && "SIMD instruction width not compatible with number of channels");

      opencl_source_generator src;
      src.vector_width_qualifier<T>(vw);
      src.kernel(name, std::is_same<T, double>::value).open("(");

      auto cache = compute::program_cache::get_global_cache(ctx);
      auto prog = cache->get(name, "");
      if (prog)
         return *prog;

      // arguments
      src.parameter<cl_ulong>("ns");
      src.parameter<T*>("x", vw);
      src.parameter<cl_ulong*>("n", vw);
      src.parameter<cl_ulong*>("bins");
      src.parameter<T*>("m", vw);
      src.parameter<T*>("sd", vw);
      src.close(")").open("{");
      src.new_line() << "ulong i = (ulong)get_global_id(0);";
      src.new_line() << "ulong co = (ulong)get_global_offset(0);";
      src.new_line() << opencl_type_name<T>(vw) << " w = sd[i]*" << opencl_constant<T>(vw, 2./nbin) << ";";
      src.new_line() << "ulong j;";
      src.new_line() << opencl_type_name<cl_long>(vw) << " b;";
      src.new_line() << "for(j = 0; j < ns; ++j)";
      src.open("{");
      std::stringstream xval;
      if (abs)
         xval << "fabs(";
      xval << "x[j*" << ncb/vw << " + i";
      if (!partial)
         xval  << " - co";
      if (abs)
         xval << "])";
      else
         xval << "]";
      if (vw > 1)
      {
         src.new_line() << "b = convert_" << opencl_type_name<cl_long>(vw) << "_sat((" << xval.str() << " - m[i] + sd[i])/w);";
      }
      else
      {
         src.new_line() << "b = (long)((" << xval.str() << " - m[i] + sd[i])/w);";
      }
      if (vw > 1)
      {
         for (int c = 0; c < vw; ++ c)
         {
            src.new_line() << "if (b.s" << c << " >= 0)";
            src.open("{");
            src.new_line() << "if (b.s" << c << " < " << nbin << ")";
            src.open("{");
            src.new_line() << "bins[i*" << nbin << " + b.s" << c << "] += 1;";
            src.close("}");
            src.new_line() << "n[2*i + 1].s" << c << " += 1;";
            src.close("}");
            src.new_line() << "else n[2*i].s" << c << " += 1;";
         }
      }
      else
      {
         src.new_line() << "if (b >= 0)";
         src.open("{");
         src.new_line() << "if (b < " << nbin << ")";
         src.open("{");
         src.new_line() << "bins[i*" << nbin << " + b] += 1;";
         src.close("}");
         src.new_line() << "n[2*i + 1] += 1;";
         src.close("}");
         src.new_line() << "else n[2*i] += 1;";
      }
      src.close("}");
      src.close("}");

      opencl_log_kernel(src.str());
      compute::program prgm;
      try
      {
         prgm = compute::program::create_with_source(src.str(), ctx);
         prgm.build();
         cache->insert(name, "", prgm);
      }
      catch(std::exception &)
      {
         // program failed to compile, print out the build log
         std::cout << prgm.build_log() << std::endl;
      }
      return prgm;
   }

   /**
    @brief Generates OpenCL kernel that computes the median from the binned data

    @param ctx OpenCL context
    @param name OpenCL kernel name

    @return boost compuite program object
    */
   static compute::program median_program(compute::context ctx, const std::string& name, size_t nbin)
   {
      auto cache = compute::program_cache::get_global_cache(ctx);
      auto prog = cache->get(name, "");
      if (prog)
         return *prog;

      opencl_source_generator src;
      src.kernel(name, std::is_same<T, double>::value).open("(");
      // arguments
      src.parameter<cl_ulong*>("n");
      src.parameter<cl_ulong*>("bins");
      src.parameter<T*>("m");
      src.parameter<T*>("sd");
      src.parameter<T*>("md");
      src.close(")").open("{");
      src.new_line() << "ulong i = (ulong)get_global_id(0);";
      src.new_line() << "ulong j;";
      src.new_line() << opencl_type_name<cl_ulong>() << " s = n[2*i];";
      src.new_line() << "for(j = 0; j < " << nbin << "; ++j)";
      src.open("{");
      src.new_line() << "if (s > (n[2*i] + n[2*i + 1])/2.) break;";
      src.new_line() << "s += bins[i*" << nbin << " + j];";
      src.close("}");
      src.new_line() << "md[i] = (m[i] - sd[i]) + (j + .5)*2*sd[i]/" << nbin << ";";
      src.close("}");

      opencl_log_kernel(src.str());
      compute::program prgm;
      try
      {
         prgm = compute::program::create_with_source(src.str(), ctx);
         prgm.build();
         cache->insert(name, "", prgm);
      }
      catch(std::exception &)
      {
         // program failed to compile, print out the build log
         std::cout << prgm.build_log() << std::endl;
      }
      return prgm;
   }
};

} // namespace statistics

#endif /* MULTICHANNEL_MEDIAN_HPP */
