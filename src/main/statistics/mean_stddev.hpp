#ifndef MULTICHANNEL_MEAN_STDDEV_HPP
#define MULTICHANNEL_MEAN_STDDEV_HPP

/// @defgroup Statistics Statisitical computations for multi-channel data
///@{

/**
 * @file   mean_stddev.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Mean and standard deviation computation of multi-channel data
 */

#include <cassert>
#include <string>
#include <boost/compute/core.hpp>
#include <boost/compute/algorithm/fill.hpp>
#include <boost/compute/algorithm/transform.hpp>
#include <boost/compute/container/vector.hpp>
#include <boost/compute/utility/program_cache.hpp>

#include "multichannel/data_block.hpp"
#include "utility/opencl_source_generator.hpp"
#include "utility/opencl_type_name.hpp"
#include "utility/opencl_logging.hpp"

#include <iostream>

namespace compute = boost::compute;

namespace statistics
{

/**
 @brief Multi-channel online computation of the mean and standard deviation of a signal.

 @tparam T Data type of the signal
 */
template<typename T = double>
class mean_stddev
{
   // OpenCL kernel (function)
   compute::kernel kernel_;
   // Mean for each channel
   compute::vector<T> xm_;
   // Square of the deviation from the mean for each channel
   compute::vector<T> m2_;
   // Standard devitaion (lazily updated)
   compute::vector<T> sd_;
   // Number of samples processed
   compute::vector<cl_ulong> ns_;
   // Number of input channels to keep track of
   size_t nci_;
   // Number of input channels per block
   size_t ncb_;
   // SIMD vector width
   size_t vw_;
   // If true then absolute values are taken before computing any statistics
   bool abs_;
   // Standard deviation update flag
   bool upd_;
   // Reset flag
   bool reset_;

public:
   using data_block_type = multichannel::data_block<T>;

   /**
    @brief Constructor

    @param ctx OpenCL context
    @param nchan Number of channels
    @param abs True if absolute value is to be taken before computing the statistics (to be removed)
    */
   mean_stddev(compute::context ctx, size_t vw, bool abs = false)
      : xm_(ctx)
      , m2_(ctx)
      , sd_(ctx)
      , ns_(ctx)
      , nci_{(size_t)-1}
      , ncb_{(size_t)-1}
      , vw_{vw}
      , abs_{abs}
      , upd_{true}
      , reset_{true}
   {}

   /**
    @brief Reset state
    */
   void reset()
   {
      reset_ = true;
   }

   /**
    @brief Processes a buffer

    The data has to consist of the same number of channels as specified in the contructor.

    @param x Buffer reference
    */
   ///@{
   void operator()(data_block_type& x, compute::command_queue& q)
   {
      const auto& dim = x.dimensions();
      _process_impl(x, dim.channel_offset, dim.channels, q);
   }

   void operator()(data_block_type& x, size_t co, size_t nc, compute::command_queue& q)
   {
      _process_impl(x, co, nc, q);
   }

   ///@}

   /**
    @brief Number of samples processed

    @return Number of samples
    */
   size_t samples(size_t chan) const
   {
      return ns_[chan];
   }

   /**
    @brief Mean value of each channel

    @return Vector with the mean of each channel
    */
   const compute::vector<T>& mean()
   {
      return xm_;
   }

   /**
    @brief Standard deviation of each channel

    @return Vector with the SD of each channel
    */
   const compute::vector<T>& standard_deviation(compute::command_queue& q)
   {
      return sd_;
   }

   /**
    @brief OpenCL kernel source

    @param ctx OpenCL context
    @param name OpenCL kernel name
    @param nci Total number of input channels
    @param mult_blk If false it is assumed that the input offset is always zero
    @param vw SIMD vector width
    @param abs If true abolute value of the input is taken before processing

    @return boost compute program
    */
   static compute::program program(compute::context ctx, const std::string& name, size_t ncb, bool partial, size_t vw, bool abs)
   {
      auto cache = compute::program_cache::get_global_cache(ctx);
      auto prog = cache->get(name, "");
      if (prog)
         return *prog;

      assert(ncb % vw == 0 && "SIMD instruction width not compatible with number of channels");

      opencl_source_generator src;
      src.vector_width_qualifier<T>(vw);
      src.kernel(name, std::is_same<T, double>::value).open("(");
      // arguments
      src.parameter<cl_ulong>("n");
      src.parameter<T*>("x", vw);
      src.parameter<cl_ulong*>("ns");
      src.parameter<T*>("xm", vw);
      src.parameter<T*>("m2", vw);
      src.parameter<T*>("sd", vw);
      src.close(")").open("{");
      src.new_line() << "ulong i = (ulong)get_global_id(0);";
      src.new_line() << "ulong co = (ulong)get_global_offset(0);";
      src.new_line() << opencl_type_name<T>(vw) << " m = xm[i];";
      src.new_line() << "ulong j;";
      src.new_line() << "for(j = 0; j < n; ++j)";
      src.open("{");
      std::stringstream xval;
      if (abs)
         xval << "fabs(";
      xval << "x[j*" << ncb/vw << " + i";
      if (!partial)
         xval  << " - co"; // input offset is always 0 --> subtract the offset
      if (abs)
         xval << "])";
      else
         xval << "]";
      src.new_line() << "xm[i] += (" << xval.str() << " - xm[i]) / (ns[i] + j + 1);";
      src.new_line() << "m2[i] += (m - " << xval.str() << ")*(xm[i] - " << xval.str() << ");";
      src.new_line() << "m = xm[i];";
      src.close("}");
      src.new_line() << "ns[i] += n;";
      src.new_line() << "sd[i] = sqrt(m2[i] / ns[i]);";
      src.close("}");

      opencl_log_kernel(src.str());
      compute::program prgm;
      try
      {
         prgm = compute::program::create_with_source(src.str(), ctx);
         prgm.build();
         cache->insert(name, "", prgm);
      }
      catch(std::exception &)
      {
         // program failed to compile, print out the build log
         std::cout << prgm.build_log() << std::endl;
      }
      return prgm;
   }

private:
   /**
    @brief Processes a buffer

    The data has to consist of the same number of channels as specified in the contructor.

    @param x Buffer reference
    */
   compute::event _process_impl(data_block_type& x, size_t co, size_t nc, compute::command_queue& q)
   {
      const auto ns = x.dimensions().samples;
      // total number of channels
      const auto nci = x.parent_dimensions().channels;
      // channels per block
      const auto ncb = x.dimensions().channels;

      assert(nc % vw_ == 0 && "SIMD instruction width not compatible with number of channels");
      assert( (co == 0 || co % vw_ == 0) && "SIMD instruction width not compatible with channel offset");

      if (reset_)
      {
         if (ncb_ != ncb)
         {
            bool partial = nc < ncb; // partial input if we are taking only a subset of data
            std::stringstream name;
            name << "mean_stddev_" << ncb << "_" << vw_ << (abs_ ? "_abs" : "_") << (partial ? "_partial" : "");
            kernel_ = compute::kernel{program(q.get_context(), name.str(), ncb, partial, vw_, abs_), name.str()};
            ncb_ = ncb;
         }
         if (nci_ != nci)
         {
            // make sure that allocation is a multiple of the vector width
            size_t n = nci / vw_;
            if (nci % vw_ != 0)
               n += 1;
            xm_.resize(n*vw_, q);
            m2_.resize(n*vw_, q);
            sd_.resize(n*vw_, q);
            ns_.resize(n*vw_, q);
            nci_ = nci;
         }
         compute::fill(xm_.begin(), xm_.end(), 0, q);
         compute::fill(m2_.begin(), m2_.end(), 0, q);
         compute::fill(sd_.begin(), sd_.end(), 0, q);
         compute::fill(ns_.begin(), ns_.end(), 0, q);
         reset_ = false;
      }

      // update values
      kernel_.set_arg<cl_ulong>(0, ns);
      kernel_.set_arg(1, x.buffer().get_buffer());
      kernel_.set_arg(2, ns_); // note that this is the old number of samples (or the current offset)
      kernel_.set_arg(3, xm_);
      kernel_.set_arg(4, m2_);
      kernel_.set_arg(5, sd_);
      upd_ = true;
      return q.enqueue_1d_range_kernel(kernel_, co/vw_, nc/vw_, 1);
   }

};

} // namespace statistics

///@}

#endif /* MULTICHANNEL_MEAN_STDDEV_HPP */
