#include "common_setup.hpp"
#include <vector>

namespace compute = boost::compute;
namespace po = boost::program_options;

common_setup::options common_setup::default_values = {
   -1, // OpenCL device index (-1 = first non-GPU)
   0, // first channel
   0, // channels to process
   0., // start time offset
   300., // Block size (in seconds) to process
   0, // channels to proces in parallel (set automatically)
   0, // channels to proces in parallel (set by the user)
   0, // SIMD vector width (0 == auto)
   true, // enable vectorization,
   false, // non-verbose output
   false // don't merge files
};

common_setup::common_setup()
   : opts_(common_setup::default_values)
{}

void common_setup::add_options(po::options_description& opts)
{
   opts.add_options()
      ("device,d", po::value<int>(&opts_.device_index)->default_value(opts_.device_index), "OpenCL device to use")
      ("first-channel,c", po::value<size_t>(&opts_.channel_offset)->default_value(opts_.channel_offset), "index of first channel to be processed")
      ("channels,n", po::value<size_t>(&opts_.channels)->default_value(opts_.channels), "number of channels to be processed; 0 = (# of channels) - (initial channel)")
      ("block-size,t", po::value<double>(&opts_.block_size)->default_value(opts_.block_size), "block size in seconds to process at a time")
      ("start-time,T", po::value<double>(&opts_.start_time)->default_value(opts_.start_time), "file offset in seconds (samples before this offset will be discarded)")
      ("parallel-channels,l", po::value<size_t>(&opts_.user_parallel_channels)->default_value(opts_.user_parallel_channels), "number of channels to process in parallel (0 = number of CPU cores / GPU compute units)")
      ("no-vectorization", po::value<bool>(&opts_.vectorize)->default_value(opts_.vectorize)->implicit_value(false)->zero_tokens(), "disable explicit code vectorization")
      ("verbose,V", po::value<bool>(&opts_.verbose)->default_value(opts_.verbose)->implicit_value(true)->zero_tokens(), "show verbose output")
      ("merge,m", po::value<bool>(&opts_.merge)->default_value(opts_.merge)->implicit_value(true)->zero_tokens(), "merge files");
}

const common_setup::options& common_setup::values() const
{
   return opts_;
}

const boost::compute::device& common_setup::device() const
{
   return dev_;
}

void common_setup::parallel_channels(size_t nc)
{
   opts_.user_parallel_channels = nc;
}

void common_setup::channels(size_t co, size_t nc)
{
   opts_.channel_offset = co;
   opts_.channels = nc;
}

size_t common_setup::maximum_vector_width(size_t nci) const
{
   // check what is the maximum vector width that we can use
   size_t vw = 1;
   if (opts_.vectorize)
   {
      size_t nc = opts_.channels;
      if (nc == 0)
         nc = nci;
      // find maximum width
      vw = opts_.vector_width;
      while (vw > 1 && (nc % vw) > 0)
         vw /= 2;
   }
   // if we have a channel offset and it is not aligned to our vector width we are out of luck
   if ( (opts_.channel_offset > 0) && (opts_.channel_offset % vw > 0) )
      vw = 1;
   if (opts_.user_parallel_channels > 0)
   {
      while (vw > 1 && (opts_.user_parallel_channels % vw) > 0)
         vw /= 2;
   }
   return vw;
}

size_t common_setup::optimal_parallel_channels(size_t vw) const
{
   size_t pc = 0;
   if (opts_.user_parallel_channels == 0)
   {
      // use the maximum sensible number of parallel channels
      pc = vw*dev_.compute_units();
   }
   else
   {
      pc = opts_.user_parallel_channels;
      // if number of channels to process is not divisible by our SIMD width
      // do it one channel at a time
      while (pc > 1 && (pc % vw) > 0)
         pc /= 2;
   }
   if (pc == 0)
      pc = 1;
   return pc;
}