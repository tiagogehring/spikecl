/* #undef __STRICT_ANSI__ */
//#define BOOST_ALL_DYN_LINK 1
#include "logging.hpp"

#include <boost/log/expressions.hpp>
#include <boost/log/attributes/timer.hpp>
#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/expressions/formatters/date_time.hpp>
#include <boost/log/expressions/formatters/named_scope.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/support/date_time.hpp>

namespace expr = boost::log::expressions;
namespace attrs = boost::log::attributes;
namespace keywords = boost::log::keywords;

// single instance of the logger
logging::sources::severity_logger<logging_severity_level> g_log;

void init_logging(const std::string& fn, logging_severity_level minConsole, logging_severity_level minFile)
{
    logging::add_console_log( std::clog
                             , logging::keywords::format = "%TimeStamp%: %_%"
                             , logging::keywords::filter = expr::attr< logging_severity_level >("Severity") >= minConsole
                             );

    if (!fn.empty())
    {
       // One can also use lambda expressions to setup filters and formatters
       logging::add_file_log
       (
           fn.c_str(),
           keywords::filter = expr::attr< logging_severity_level >("Severity") >= minFile,
           keywords::format = expr::stream
               << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%Y-%m-%d, %H:%M:%S.%f")
               /* << " [" << expr::format_date_time< attrs::timer::value_type >("Uptime", "%O:%M:%S") */
               /* << "] [" << expr::format_named_scope("Scope", keywords::format = "%n (%f:%l)") */
               << "] <" << expr::attr< logging_severity_level >("Severity")
               << "> " << expr::message
       );
    }

    // Also let's add some commonly used attributes, like timestamp and record counter.
    logging::add_common_attributes();
}
