#ifndef BURST_DETECTION_SETUP_HPP
#define BURST_DETECTION_SETUP_HPP

/**
 * @file   burst_detection_setup.hpp
 * @author Tiago Gehring
 * @date   February, 2014
 * @brief  Processing of options related to the busrt detection features
 */

#include <string>
#include <boost/program_options.hpp>

/**
 @brief Processes commmand line arguments related to the burst detection
 */
class burst_detection_setup
{
public:
   struct options
   {
      // bin size for computation of active electrodes, in ms
      double bin_size;
      // detection threshold (in spikes*# of active electrodes)
      double threshold;
      // minimum active number of sites to be considered a burst (0 == not used)
      size_t active_sites;
      // write out extended data such as the # of spikes per bin
      bool detailed_output;
      // "pelt" factor (see van Pelt, J., Wolters, P. S., Corner, M. a, Rutten, W. L. C., & Ramakers, G. J. a. (2004). Long-term characterization of firing dynamics of spontaneous bursts in cultured neural networks. IEEE Transactions on Bio-Medical Engineering, 51(11), 2051�62. doi:10.1109/TBME.2004.827936)
      double pelt_factor;
      // minimum distance between a burst peak and the beginning of the next one
      double min_burst_distance;
   };

private:
   options opts_;

public:
   // default values
   static options default_values;

   /**
    @brief Constructor
    */
   burst_detection_setup();

   /**
    @brief Adds command line options to the list

    @param opts Command line options list to which options are to be appended
    */
   void add_options(boost::program_options::options_description& opts);

   /**
    @brief Processes command line options

    @param vm Command line arguments
    */
   void process_options(const boost::program_options::variables_map& vm);

   /**
    @brief Returns options values

    @return Options values
    */
   const options& values() const;
};

#endif /* BURST_DETECTION_SETUP_HPP */
