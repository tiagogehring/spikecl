#ifndef FILTER_SETUP_HPP
#define FILTER_SETUP_HPP

/**
 * @file   filter_setup.hpp
 * @author Tiago Gehring
 * @date   February, 2014
 * @brief  Processing of options related to the data filtering
 */

#include <unordered_map>
#include <boost/program_options.hpp>
#include <boost/optional.hpp>
#include "dsp/filter_kernel_coefficients.hpp"

/**
 @brief Processes command line arguments related to the filtering of raw data;

 Filtering coefficients are returned by this class and can be passed as command line arguments
 or as a file containing a list of pre-computed filtering coefficients. If none of those are provided
 default pre-computed filtering coefficients will be used.
 */
class filter_setup
{
private:
   using filter_coefficients_list = std::unordered_map<size_t, dsp::filter_kernel_coefficients>;
   // explicit coefficients set by the user
   boost::optional<dsp::filter_kernel_coefficients> filter_coeff_;
   // multiple coefficients, stored by sampling frequency
   filter_coefficients_list coeff_list_;

public:
   /**
    @brief Adds command line options to the list

    @param opts Command line options list to which options are to be appended
    */
   void add_options(boost::program_options::options_description& opts);

   /**
    @brief Processes command line options

    @param vm Command line arguments
    */
   void process_options(const boost::program_options::variables_map& vm);

   /**
    @brief Returns filter coefficients for a given sampling rate

    @param sr Sampling rate

    @return Filter coefficients
    */
   dsp::filter_kernel_coefficients& filter_coefficients(double sr);
};

#endif /* FILTER_SETUP_HPP */
