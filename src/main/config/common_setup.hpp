#ifndef COMMON_SETUP_HPP
#define COMMON_SETUP_HPP

/**
 * @file   common_setup.hpp
 * @author Tiago Gehring
 * @date   February, 2014
 * @brief  Initialization of common aspects of the program such as the OpenCL runtime
 */

#include <boost/compute/core.hpp>
#include <boost/program_options.hpp>
#include "logging.hpp"


#if USE_DOUBLE_PRECISION
   using fp_type = double;
#else
   using fp_type = float;
#endif

/**
 @brief Processes command line options related to basic aspects of the program such as the OpenCL device
 */
class common_setup
{
public:
   using device_type = boost::compute::device;

   struct options
   {
      // Device index
      int device_index;
      // Initial channel index
      size_t channel_offset;
      // Number of channels to process
      size_t channels;
      // starting time offset (in seconds)
      double start_time;
      // Maximum block size (in seconds) to process
      double block_size;
      // Number of parallel channels to process in parallel
      size_t parallel_channels;
      // Number of parallel channels defined by the user
      size_t user_parallel_channels;
      // SIMD vector width to use in calculations
      size_t vector_width;
      // enable explicit code vectorization
      bool vectorize;
      // show verbose output (for debugging)
      bool verbose;
      // merge files
      bool merge;
   };

private:
   // OpenCL device
   device_type dev_;
   options opts_;

public:
   static options default_values;

   /**
    @brief Constructor
    */
   common_setup();

   /**
    @brief Adds command line options to the list

    @param opts Command line options list to which options are to be appended
    */
   void add_options(boost::program_options::options_description& opts);

   /**
    @brief Processes command line options. Initializes OpenCL sub-system.

    @param vm Command line arguments
    */
   template<typename Log = dummy_logger>
   void process_options(const boost::program_options::variables_map& vm, Log&& log = Log{})
   {
      namespace compute = boost::compute;
      /**
       * Initialize OpenCL
       */
      if (compute::system::platform_count() == 0)
         throw std::runtime_error{"No OpenCL runtime found. Please install a OpenCL runtime (e.g. from Intel or AMD) and try again."};

      // show available OpenCL platforms
      {
         std::stringstream ss;
         size_t idx = 0;
         if (compute::system::platform_count() > 1)
            ss << "OpenCL Platforms: " << std::endl;
         else
            ss << "OpenCL Platform: ";
         for(auto& plat : compute::system::platforms())
         {
            if (compute::system::platform_count() > 1)
            {
               if (idx > 0)
                  ss << std::endl;
               ss << "[" << idx << "] ";
            }
            ss <<  plat.name() << ", " <<  plat.version() << ", " << plat.vendor();
            ++idx;
         }
         log(ss.str());
      }

      if (compute::system::device_count() == 0)
         throw std::runtime_error{"No OpenCL devices found. Please install a OpenCL runtime (e.g. from Intel or AMD) and try again."};

      // show available OpenCL devices
      {
         std::stringstream ss;
         size_t idx = 0;
         if (compute::system::device_count() > 1)
            ss << "Devices: " << std::endl;
         else
            ss << "Device: ";

         for(const auto& dev : compute::system::devices())
         {
            if (compute::system::device_count() > 1)
            {
               if (idx > 0)
                  ss << std::endl;
               ss << "[" << idx << "] ";
            }
            ss << dev.name() << ", " << dev.compute_units() << " compute units, " << dev.global_memory_size()/(1024*1024) << " MiB";
          
			   // check if no explicit device was specified and if it is a CPU; in that case use it
			   if (opts_.device_index == -1 && dev.type() == compute::device::cpu)
				   opts_.device_index = static_cast<int>(idx);

			   if (idx == opts_.device_index)
				   ss << " (*)"; // selected mark

            ++idx;
         }
         log(ss.str());
      }

      if (opts_.device_index >= compute::system::device_count())
         throw std::runtime_error{"Invalid OpenCL device!"};

      dev_ = compute::system::devices()[opts_.device_index];

      // hardware vector width (depending if 128 bit SSE or 256 bit AVX instructions are supported, for example)
      const auto hw_vw = std::is_same<fp_type, double>::value ?
                      dev_.get_info<cl_uint>(CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE) :
                      dev_.get_info<cl_uint>(CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT);
      if (opts_.vectorize)
      {
         if (opts_.vector_width == 0)
            opts_.vector_width = hw_vw;
      }
      else
         opts_.vector_width = 1;

      if (opts_.user_parallel_channels == 0)
      {
         // use the maximum sensible number of parallel channels
         opts_.parallel_channels = hw_vw*dev_.compute_units();
      }
      else
      {
         // if number of channels to process is not divisible by our SIMD width
         // do it one channel at a time
         if (opts_.user_parallel_channels % opts_.vector_width != 0)
            opts_.parallel_channels = 1;
      }
   }

   /**
    @brief OpenCL device

    @return Reference to OpenCL device
    */
   const device_type& device() const;

   /**
    @brief Returns options values

    @return Options values
    */
   const options& values() const;

   /**
    @brief Sets number of parallel_channels directly, overriding command line options

    @param nc Number of parallel channels
    */
   void parallel_channels(size_t nc);
   void channels(size_t co, size_t nc);

   /**
    @brief Computes the maximum vector width that can be used for processing based on the current settings

    @param nci Number of input channels

    @return Maximum vector width
    */
   size_t maximum_vector_width(size_t nci) const;

   /**
    @brief Computes the ideal number of parallel channels that can be processed based on the current settings

    @param vw Vector width to be used

    @return Optimal number of parallel channels
    */
   size_t optimal_parallel_channels(size_t vw) const;
};

#endif /* COMMON_SETUP_HPP */
