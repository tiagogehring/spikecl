#include "filter_setup.hpp"
#include <string>
#include <fstream>
#include <boost/tokenizer.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/utility/in_place_factory.hpp>

#include "default_filter_coefficients.cpp"

namespace po = boost::program_options;
namespace fs = boost::filesystem;

void filter_setup::add_options(po::options_description& opts)
{
   opts.add_options()
     ("a", po::value<std::string>(), "a (recursive) coefficients of the filter transfer function")
     ("b", po::value<std::string>(), "b coefficients of the filter transfer function")
     ("coeff-file,F", po::value<std::string>(), "filter coefficients file");
}

void filter_setup::process_options(const po::variables_map& vm)
{
   coeff_list_.clear();
   filter_coeff_ = boost::none;

   if (vm.count("coeff-file") > 0 && vm.count("a") > 0)
      throw std::runtime_error{"Both a filter coefficients file and explicit values provided"};
   // check if filter coefficients were specified
   if (vm.count("a") || vm.count("b"))
   {
      std::vector<double> a, b;
      boost::tokenizer<> tok_a{vm["a"].as<std::string>()};
      for( const auto& s : tok_a)
         a.push_back(boost::lexical_cast<double>(s));

      boost::tokenizer<> tok_b{vm["b"].as<std::string>()};
      for( const auto& s : tok_b)
         b.push_back(boost::lexical_cast<double>(s));
      if (a.size() < 3)
         throw std::runtime_error{"At least 3 a and b coefficients have to be provided (for a 1st order filter)"};

      if (a.size() != b.size())
         throw std::runtime_error{"Number of filter a and b coefficients has to be the same"};
      if (a.size() % 2 != 1)
         throw std::runtime_error{"Number of a and b coefficients has to be odd"};

      filter_coeff_ = boost::in_place(a, b);
   }
   else if (vm.count("coeff-file"))
   {
      auto fn = vm["coeff-file"].as<std::string>();
      if (!fs::exists(fn))
         throw std::runtime_error{"Filter coefficient file not found"};
      std::ifstream fin{fn};
      std::string line;
      std::vector<double> vals;
      std::vector<double> a, b;
      while(std::getline(fin, line))
      {
         boost::tokenizer<boost::escaped_list_separator<char>> tok{line, boost::escaped_list_separator<char>()};
         auto p = tok.begin();
         if (p == tok.end())
            continue;
         size_t freq = static_cast<size_t>(boost::lexical_cast<double>(*p));
         ++p;

         while(p != tok.end())
         {
            vals.push_back(boost::lexical_cast<double>(*p));
            ++p;
         }

         // sanity check
         if (vals.size() % 2 != 0)
            throw std::runtime_error{"Invalid coefficients file: number of coefficients has to be even"};
         a.resize(vals.size() / 2);
         b.resize(vals.size() / 2);
         std::copy(vals.begin(), vals.begin() + vals.size()/2, b.begin());
         std::copy(vals.begin() + vals.size()/2, vals.end(), a.begin());
         coeff_list_.insert(std::make_pair(freq, dsp::filter_kernel_coefficients{b, a}));
         vals.clear();
      }
   }
}

dsp::filter_kernel_coefficients& filter_setup::filter_coefficients(double sr)
{
   // -> look for appropriate filter coefficients
   //
   // i) use command line filter coefficients if provided
   if (filter_coeff_)
      return *filter_coeff_;

   size_t freq = static_cast<size_t>(round(sr));

   // ii) use coefficients from a file or cached ones
   if (coeff_list_.find(freq) != coeff_list_.end())
   {
      return coeff_list_[freq];
   }

   // iii) look for built in, default, coefficients
   for (int i = 0; i < sizeof(g_default_filter_coefficients)/sizeof(g_default_filter_coefficients[0]); ++i)
   {
      if (static_cast<size_t>(g_default_filter_coefficients[i][0]) == freq)
      {
         std::vector<double> a, b;
         size_t ncoeff = sizeof(g_default_filter_coefficients[0]) / sizeof(g_default_filter_coefficients[0][0]) - 1;
         assert(ncoeff % 2 == 0);
         for(size_t j = 0; j < ncoeff ; ++j)
         {
            if (j < ncoeff / 2)
               b.push_back(g_default_filter_coefficients[i][j + 1]);
            else
               a.push_back(g_default_filter_coefficients[i][j + 1]);
         }

         // cache coefficients for next time
         coeff_list_.insert( std::make_pair(freq, dsp::filter_kernel_coefficients{b, a}) );
         return coeff_list_[freq];
      }
   }

   // well, nothing really to do, no coefficients found
   std::stringstream ss;
   ss << "No known filter coefficients for file sampling frequency (" << freq << "Hz). Please provide a filter coefficients file (-coeff-file) or manually.";
   throw std::runtime_error{ss.str()};
}
