#ifndef LOGGING_HPP
#define LOGGING_HPP

#include <iostream>
#include <boost/log/common.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/attributes/timer.hpp>
#include <boost/log/sources/logger.hpp>

namespace logging = boost::log;

// Here we define our application severity levels.
enum class logging_severity_level
{
   none,
   trace,
   debug,
   info,
   warning,
   error
};

// The formatting logic for the severity level
template< typename CharT, typename TraitsT >
inline std::basic_ostream<CharT, TraitsT>& operator << (std::basic_ostream<CharT, TraitsT>& strm, logging_severity_level lvl)
{
    static const char* const str[] =
    {
        "TRACE",
        "DEBUG",
        "INFO",
        "WARN",
        "ERROR"
    };
    if (static_cast< std::size_t >(lvl) < (sizeof(str) / sizeof(*str)))
        strm << str[static_cast<size_t>(lvl)];
    else
        strm << static_cast< int >(lvl);
    return strm;
}

// inits the logging functionality - has to be called once
void init_logging(const std::string& fn, logging_severity_level minConsole, logging_severity_level minFile);

extern logging::sources::severity_logger<logging_severity_level> g_log;

struct dummy_logger
{
   void operator ()(const std::string&)
   {}
};

struct logger
{
   logging_severity_level sev_;

   logger(logging_severity_level sev)
      : sev_{sev}
   {}

   void operator()(const std::string& s)
   {
      BOOST_LOG_SEV(g_log, sev_) << s;
   }
};

#endif /* LOGGING_HPP */
