#include "spike_detection_setup.hpp"

namespace po = boost::program_options;

// setup default values for the options
spike_detection_setup::options spike_detection_setup::default_values = {
   5., // spike threshold
   1000., // maximum spike threshold
   2.5, // refractory time (ms)
   20, // points pre-spike to store
   44, // ponts post-spike to store
   0, // points no-spike store; just kidding, spike sign
   0, // spike smoothing factor
   spike_ordering::none, // spike ordering (well, this one was obvious)
   true, // generate _spikes.txt file
   spike_file_format::standard,
   spike_waveforms_storage::none // generate one waveforms file per channel
};

spike_detection_setup::spike_detection_setup()
   : opts_(spike_detection_setup::default_values)
{}

void spike_detection_setup::add_options(po::options_description& opts)
{
   std::string def_sign = (opts_.spike_sign > 0 ? "pos" : (opts_.spike_sign < 0 ? "neg" : "both"));
   std::string def_ord = (opts_.spike_order == spike_ordering::electrode_number ? "elec" :
                           opts_.spike_order == spike_ordering::channel_number ? "chan" : "none");
   std::string def_spks = (opts_.spike_format == spike_file_format::standard ? "standard" :
      opts_.spike_format == spike_file_format::legacy? "legacy" : "none");
   std::string def_wf = (opts_.waveforms_storage == spike_waveforms_storage::one_per_channel ? "individual" :
                           opts_.waveforms_storage == spike_waveforms_storage::single_file ? "single" : "none");

   opts.add_options()
     ("spikes", po::value<std::string>()->default_value(def_spks), "spike storage format: 'standard', 'legagy' (format used by QSpike tools), or 'none'")
     ("waveforms", po::value<std::string>()->default_value(def_wf), "spike waveforms storage option: 'individual', one file per channel will be created ([input]_chan_x##_spikes.csv); 'single' one single waveforms file will be created ([input]_spikes.csv); 'none': don't store waveforms")
     ("spikes-ordering,R", po::value<std::string>()->default_value(def_ord), "order spikes by electrode number [elec] or channel index [chan] (note that sorting increases memory requirements since all detected spikes have to be kept in memory)")
     ("threshold,h", po::value<double>(&opts_.threshold)->default_value(opts_.threshold), "spike detection threshold in standard deviations")
     ("threshold-max,T", po::value<double>(&opts_.max_threshold)->default_value(opts_.max_threshold), "spike detection maximum threshold in standard deviations (to remove artifacts)")
     ("sign,s", po::value<std::string>()->default_value(def_sign), "filter spikes by sign: 'pos'/'neg'/'both':  select only positive/negative/all spikes")
     ("refractory-time,r", po::value<double>(&opts_.refractory_time)->default_value(opts_.refractory_time), "detector refractory time in ms")
     ("points-pre,p", po::value<size_t>(&opts_.points_pre)->default_value(opts_.points_pre), "number of pre-event data points to store")
     ("points-post,P", po::value<size_t>(&opts_.points_post)->default_value(opts_.points_post), "number of post-event data points to store")
     ("spike-smoothing,S", po::value<bool>()->default_value(opts_.smoothing_factor > 0 ? true : false), "smooth spike waveforms using a cubic spline interpolation")
     ("smoothing-factor,K", po::value<size_t>(&opts_.smoothing_factor)->default_value(opts_.smoothing_factor), "smoothing factor (how many points to add for each sample in the spike)");
}

void spike_detection_setup::process_options(const po::variables_map& vm)
{
   // process spike options
   assert(vm.count("sign") > 0);
   int sign;
   if (vm["sign"].as<std::string>() == "pos")
      sign = 1;
   else if (vm["sign"].as<std::string>() == "neg")
      sign = -1;
   else if (vm["sign"].as<std::string>() == "both")
      sign = 0;
   else
   {
      throw std::runtime_error{"Invalid value for option 'sign'"};
   }
   opts_.spike_sign = sign;

   assert(vm.count("spikes-ordering") > 0);
   opts_.spike_order = spike_ordering::none;
   if (vm["spikes-ordering"].as<std::string>() == "elec")
      opts_.spike_order = spike_ordering::electrode_number;
   else if (vm["spikes-ordering"].as<std::string>() == "chan")
      opts_.spike_order = spike_ordering::channel_number;
   else if (vm["spikes-ordering"].as<std::string>() != "none")
   {
      throw std::runtime_error{"Invalid value for option 'spikes-ordering'"};
   }

   assert(vm.count("spikes") > 0);
   opts_.spike_format = spike_file_format::none;
   if (vm["spikes"].as<std::string>() == "standard")
      opts_.spike_format = spike_file_format::standard;
   else if (vm["spikes"].as<std::string>() == "legacy")
      opts_.spike_format = spike_file_format::legacy;
   else if (vm["spikes"].as<std::string>() != "none")
      throw std::runtime_error{ "Invalid value for option 'spike-storage'" };

   assert(vm.count("waveforms") > 0);
   opts_.waveforms_storage = spike_waveforms_storage::none;
   if (vm["waveforms"].as<std::string>() == "single")
      opts_.waveforms_storage = spike_waveforms_storage::single_file;
   else if (vm["waveforms"].as<std::string>() == "individual")
      opts_.waveforms_storage = spike_waveforms_storage::one_per_channel;
   else if (vm["waveforms"].as<std::string>() != "none")
      throw std::runtime_error{"Invalid value for option 'waveforms'"};
}

const spike_detection_setup::options& spike_detection_setup::values() const
{
   return opts_;
}
