#include "burst_detection_setup.hpp"

namespace po = boost::program_options;

// setup default values for the options
burst_detection_setup::options burst_detection_setup::default_values = {  
  25., // bin size (ms)
  0, // burst threshold
  2, // min # of active sites
  false, // don't write detailed bin statistics
  0.05, // "pelt" factor
  450 // minimum burst interval (ms)
};

burst_detection_setup::burst_detection_setup()
   : opts_(burst_detection_setup::default_values)
{}

void burst_detection_setup::add_options(po::options_description& opts)
{
   opts.add_options()
      ("burst-bin-width", po::value<double>(&opts_.bin_size)->default_value(opts_.bin_size), "Burst bin widht (in ms)")
      ("burst-threshold", po::value<double>(&opts_.threshold)->default_value(opts_.threshold), "Burst threshold (in spikes*active electrodes)")
      ("burst-active-sites", po::value<size_t>(&opts_.active_sites)->default_value(opts_.active_sites), "Minimum # of active sites for a burst")
      ("bin-statistics", po::value<bool>(&opts_.detailed_output)->default_value(opts_.detailed_output)->implicit_value(true)->zero_tokens(), "Write also the number of spikes per bin and bin statistics to separate files")
      ("pelt-factor", po::value<double>(&opts_.pelt_factor)->default_value(opts_.pelt_factor), "(van) Pelt factor (used to determine the width of the bursts)")
      ("min-burst-interval", po::value<double>(&opts_.min_burst_distance)->default_value(opts_.min_burst_distance), "Minimum distance between a burst peak and the beginning of the next one (in ms)");
}

void burst_detection_setup::process_options(const po::variables_map& vm)
{
}

const burst_detection_setup::options& burst_detection_setup::values() const
{
   return opts_;
}
