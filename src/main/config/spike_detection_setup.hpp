#ifndef SPIKE_DETECTION_SETUP_HPP
#define SPIKE_DETECTION_SETUP_HPP

/**
 * @file   filter_setup.hpp
 * @author Tiago Gehring
 * @date   February, 2014
 * @brief  Processing of options related to the spike detection sub-system
 */

#include <string>
#include <boost/program_options.hpp>

/**
 @brief Specified how spikes should be sorted (ordered to not create confusion)
 */
enum class spike_ordering
{
   electrode_number, //!< sort (order) by electrode number
   channel_number, //!< sort (order) by channel number
   none //!< don't sort it
};

/**
@brief How (if) should spike waveforms be stored
*/
enum class spike_file_format
{
   none,
   legacy, //!< Generate files that are compatible with the QSpike tools
   standard //!< Standard storage format
};

/**
 @brief How (if) should spike waveforms be stored
 */
enum class spike_waveforms_storage
{
   single_file, //!< Generate one waveforms file for all spikes
   one_per_channel, //!< Generate one file per channel
   none //!< Don't store waveforms at all
};

/**
 @brief Processes commmand line arguments related to the spike detection
 */
class spike_detection_setup
{
public:
   struct options
   {
      // spike detection threshold in standard deviations
      double threshold;
      // spike detection max threshold
      double max_threshold;
      // refractory time [ms]
      double refractory_time;
      // number of points preceeding a spike to store
      size_t points_pre;
      // number of points after a spike to store
      size_t points_post;
      // spike sign filter (-1 = negative spikes, +1 positive spikes, 0 all spikes)
      int spike_sign;
      // spike smoothing factor
      size_t smoothing_factor;
      // how to order the spikes when storing them
      spike_ordering spike_order;
      /// generate spikes file
      bool spikes_file;
      /// legagy or standard spike storage format
      spike_file_format spike_format;
      /// single or separate file for storing waveforms
      spike_waveforms_storage waveforms_storage;
   };

private:
   options opts_;

public:
   // default values
   static options default_values;

   /**
    @brief Constructor
    */
   spike_detection_setup();

   /**
    @brief Adds command line options to the list

    @param opts Command line options list to which options are to be appended
    */
   void add_options(boost::program_options::options_description& opts);

   /**
    @brief Processes command line options

    @param vm Command line arguments
    */
   void process_options(const boost::program_options::variables_map& vm);

   /**
    @brief Returns options values

    @return Options values
    */
   const options& values() const;
};

#endif /* SPIKE_DETECTION_SETUP_HPP */
