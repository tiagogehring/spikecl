#ifndef MULTICHANNEL_DATA_BUFFERS_ITERATOR_HPP
#define MULTICHANNEL_DATA_BUFFERS_ITERATOR_HPP

/// @addtogroup Buffers
///@{

/**
 * @file   iterator.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Multi-channel data iterator
 */

#include <vector>
#include <boost/iterator/iterator_facade.hpp>
#include "data_block.hpp"

namespace multichannel
{

/**
 @brief Multi-channel and multi-buffer data iterator

 This iterator will move through multiple data chunks (data blocks) if necessary.

 @tparam T data type supported
 */
template<typename T>
class iterator
   : public boost::iterator_facade< iterator<T>
                                  , const T
                                  , boost::random_access_traversal_tag>
{
   using type = iterator<T>;
   using base = boost::iterator_facade< iterator<T>
                                      , const T
                                      , boost::random_access_traversal_tag>;

   using data_block = multichannel::data_block<T>;

   // parent data block
   const data_block* buf_;
   // channel number
   size_t chan_;
   // sample number
   size_t s_;
   // OpenCL command queue
   compute::command_queue* queue_;

public:
   friend class boost::iterator_core_access;
   using difference_type = typename base::difference_type;

   /**
    @brief Empty constructor (used to create the terminating iterator)
    */
   iterator()
      : buf_{nullptr}
      , chan_{(size_t)-1}
      , s_{(size_t)-1}
      , queue_{&compute::system::default_queue()}
   {}

   /**
    @brief Constructor

    @param buf parrent data block
    @param chan channel number
    @param s0 sample number
    @param q OpenCL command queue (for mapping/unmapping buffers)
    */
   iterator(const data_block* buf, size_t chan, size_t s0, compute::command_queue* q)
      : buf_{buf}
      , chan_{chan}
      , s_{s0}
      , queue_{q}
   {
      // look for the buffer index
      if (!buf->has_sample(s0))
      {
         if (s0 < buf_->dimensions().sample_offset)
         {
            // search backwards
            while( buf_ != nullptr && !buf_->has_sample(s0))
               buf_ = buf_->prev();
         }
         else
         {
            while( buf_ != nullptr && !buf_->has_sample(s0))
               buf_ = buf_->next();
         }
      }

      assert(buf_ != nullptr && "invalid sample");
   }

   /**
    @brief Advances iterator a given number of positions

    @param n number of positions to advance
    */
   void advance(difference_type n)
   {
      s_ += n;
      if (n > 0)
      {
         while(s_ - buf_->dimensions().sample_offset >= buf_->dimensions().samples)
         {
            assert(buf_->next() != nullptr);
            buf_ = buf_->next();
         }
      }
      else if (n < 0)
      {
         while(s_ < buf_->dimensions().sample_offset)
         {
            assert(buf_->prev() != nullptr);
            buf_ = buf_->prev();
         }
      }
   }

   /**
    @brief Copy assignment operator
    */
   iterator& operator = (const iterator& other)
   {
      if (&other != this)
      {
         buf_ = other.buf_;
         chan_ = other.chan_;
         s_ = other.s_;
         queue_ = other.queue_;
      }
      return *this;
   }

   /**
    @brief Iterator distance

    @return Relative distance to another iterator
    */
   difference_type distance_to(const type& other) const
   {
      return s_ - other.s_;
   }

   /**
    @brief Moves iterator forward by one position
    */
   void increment()
   {
      ++s_; // just increment position
      if (s_ - buf_->dimensions().sample_offset == buf_->dimensions().samples)
      {
         // move to next buffer
         buf_ = buf_->next();
      }
   }

   /**
    @brief Moves iterator backward by one position
    */
   void decrement()
   {
      assert(s_ > 0);
      if (s_ == buf_->offset())
      {
         // move to previous buffer
         buf_ = buf_->prev();
      }
      --s_;
   }

   /**
    @brief Compares iterators

    @return True if the two iterators point to the same sample
    */
   bool equal(const iterator<T>& other) const
   {
      return ( (buf_ == other.buf_) && (chan_ == other.chan_) && (s_ == other.s_) );
   }

   /**
    @return Value at current position
    */
   T dereference() const
   {
      return buf_->sample_value(chan_, s_, *queue_);
   }
};

} // namespace multichannel

#endif /* MULTICHANNEL_DATA_BUFFERS_ITERATOR_HPP */
