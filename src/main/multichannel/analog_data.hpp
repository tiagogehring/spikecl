#ifndef MULTICHANNEL_ANALOG_DATA_HPP
#define MULTICHANNEL_ANALOG_DATA_HPP

/// @addtogroup Buffers
///@{

/**
 * @file   analog_data.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Provides functions to read and processes multi-channel analog data
 */

#include <memory>
#include <vector>
#include <ostream>
#include <boost/compute/core.hpp>
#include <boost/compute/algorithm/transform.hpp>
#include <boost/functional/hash.hpp>
#include "data_block.hpp"
#include "iterator.hpp"
#include "data_source/data_dimensions.hpp"
#include "utility/type_abbreviation.hpp"
#include "utility/opencl_type_name.hpp"

namespace compute = boost::compute;

namespace multichannel
{

/**
 @brief Provides storage and transformation functions for multi-channel data

 Analog data is read from a data source in chunks (necessary for large files due to
 a maximum buffer size limitation in OpenCL). A data filter can be applied to the data
 and the output can be writen to a file or an array or given back in the form of OpenCL
 memory buffers for further processing.

 @tparam T Data type supported (double is the default but, e.g., float could also be used for faster processing)
 */
/// @todo change linked buffers to a true linked list (without storing them also in a separate vector);
/// @todo get rid of the "reshape" function; fills misplaced here
template<typename T = double>
class analog_data
{
public:
   /**
    @brief Data-chunk type (@ref multichannel_data_buffer)
    */
   using data_block_type = data_block<T>;
   /**
    @brief Storage for multiple data-chunks
    */
   using data_blocks_type = std::vector<data_block_type>;
   /**
    @brief Iterator type (@ref multichannel_iterator)
    */
   using iterator_type = iterator<T>;

   using data_dimensions = data_source::data_dimensions;

private:
   // OpenCL command queue
   compute::context ctx_;
   // Storage for the data buffers (chunks)
   data_blocks_type data_;
   // Vector width for SIMD instructions
   size_t vw_;
   // Total buffer dimensions
   data_dimensions dim_;
   // Sampling rate
   double sr_;
   // Analog zero (AD reference)
   T xoff_;
   // Analog units (mV/AD units)
   double xfac_;
   // row/column-major flag
   data_source::storage_format dsf_;
   // maximum number of samples per chunk
   size_t max_ns_;

public:
   /**
    @brief Constructor

    @param ds Data source instance
    @param chani First channel number
    @param chanf Last channel number
    @param vw SIMD Vector width (to contro data alignment)
    @param max_bs Maximum size of combined data chunks in bytes( all channels included). Larger files will be split into multiple chunks.
    @param max_cs Maximum size of data chunks in bytes (all channels included). Larger files will be split into multiple chunks.
   */
   analog_data(compute::context ctx, size_t vw = 1, size_t max_ns = 512*1024*1024 / sizeof(T))
      : ctx_{std::move(ctx)}
      , vw_{vw}
      , max_ns_{max_ns}
   {}

   analog_data(compute::context ctx, data_dimensions dim, size_t vw = 1, size_t max_ns = 512*1024*1024 / sizeof(T))
      : ctx_{ctx}
      , vw_{vw}
      , max_ns_{max_ns}
   {
      _init(dim);
   }

   /**
    @brief Destructor
    */
   ~analog_data()
   {
      data_.clear()  ;
   }

   T input_offset() const
   {
      return xoff_;
   }

   double input_scalling() const
   {
      return xfac_;
   }

   template<typename U>
   void reshape(const analog_data<U>& other, size_t nc = (size_t)-1)
   {
      if (data_.size() < other.data().size())
      {
         data_.clear();
         data_.reserve(other.data().size());
      }
      for (size_t i = 0; i < other.data().size(); ++i)
      {
         auto dim = other.data()[i].dimensions();
         // explicit number of channels specified?
         if (nc != (size_t)-1)
            dim.channels = nc;

         if (i < data_.size())
         {
            // resize buffer
            data_[i].reshape(dim);
         }
         else
         {
            // allocate new buffer
            data_.emplace_back(
                  data_block_type{ ctx_
                                 , dim
                                 , other.dimensions()
                                 , i > 0 ? &data_[i - 1] : nullptr }
                           );
         }
      }
      for (size_t i = 1; i < data_.size(); ++i)
      {
         if (i < other.data().size())
            data_[i].link_prev(&data_[i - 1]);
         else
         {
            data_[i].clear();
            data_[i].link_prev(nullptr);
         }
      }
      dim_ = other.dimensions();
      if (nc != (size_t)-1)
         dim_.channels = nc;
   }

   /**
    @brief True if buffer is empty

    @return True if empty
    */
   bool empty() const
   {
      return dim_.empty();
   }

   /**
    @brief Data dimensions (channels x samples) and other properties (format, sampling rate)

    @return Dimensions struct
    */
   const data_dimensions& dimensions() const
   {
      return dim_;
   }

   /**
    @brief Const iterator to beginning of a single data channel

    @param chan Channel number

    @return Iterator pointing to beginning of channel data
    */
   iterator_type cbegin(size_t chan, compute::command_queue& q) const
   {
     return iterator_type(&data_[0], chan, 0, &q);
   }

   /**
    @brief Const iterator to the end of a single data channel

    @param chan Channel number

    @return Iterator pointing to the end of the channel data
    */
   iterator_type cend(size_t chan) const
   {
      return iterator_type();
   }

   /**
    @brief Iterator pointing to a given sample number

    @param chan Channel number
    @param s Sample number

    @return Iterator pointing to given channel and sample
    */
   iterator_type at(size_t chan, size_t s) const
   {
      return iterator_type(&data_[0], chan, s);
   }

   /**
    @brief Reads data from a data source

    All samples for the given range of channels are read. If necessary, multiple data chunks will be created.

    @param chani First channel to be read
    @param nchan Number of channels to read
    */
   template<typename DataSource>
   void extract_channels(DataSource& ds, size_t chani = 0, size_t nchan = (size_t)-1)
   {
      auto dim = ds.dimensions();
      dim.channel_offset = chani;
      // always column_major here
      dim.format = data_source::storage_format::column_major;
      if (nchan == (size_t)-1)
         dim.channels = dim.channels - dim.channel_offset;
      else
         assert(dim.channel_offset + nchan <= dim.channels);

      // allocate buffers if necessary
      _init(dim);

      // read the data from the file
      for (size_t i = 0; i < data_.size() ; ++i)
      {
         const size_t n = data_[i].dimensions().samples;
         size_t nc = 0;
         for (size_t ic = dim.channel_offset; ic < dim.channel_offset + dim.channels; ++ic, ++nc)
         {
            ds.read_channel(ic, data_[i].dimensions().sample_offset, n, data_[i].host_ptr() + nc*n);
         }
      }
      // conversion factors
      xoff_ = ds.reference_value();
      xfac_ = ds.scalling_factor();
   }

   /**
    @brief Reads data from a source in row-major format

    In this version of the function n samples of all data channels are read from the source.

    @tparam DataSource Data source type
    @param ds Data source reference
    @param ns Number of samples to read

    @return Number of samples read
    */
   template<typename DataSource>
   size_t read(DataSource& ds, size_t ns, compute::command_queue& q)
   {
      // always read all the channels here
      auto dim = ds.dimensions();

      dim.samples = ns;
      dim.sample_offset = ds.position();

      // allocate and reshape buffers if necessary
      _init(dim);

      // offset if the same as the first chunk
      dim.samples = 0;
      // read the data from source for the multiple - non-empty- chunks
      size_t r = 0;
      size_t off = ds.position();
      for (size_t i = 0; i < data_.size() ; ++i)
      {
         if (data_[i].empty())
            break; // we are done
         r += ds.read(data_[i].dimensions().samples, data_[i].host_ptr());
      }
      dim_ = dim;
      dim_.sample_offset = off;
      dim_.samples = r;

      // conversion factors
      xoff_ = ds.reference_value();
      xfac_ = ds.scalling_factor();
      return r;
   }

   /**
    @brief Converts data type and scales contents of buffers copying them to another buffer

    @tparam U Target data type
    @param y Target buffer
    */
   template<typename U>
   void scale(analog_data<U>& y, compute::command_queue& q)
   {
      auto src = _scale_source<U>(xoff_, static_cast<U>(xfac_));

      static compute::function<U(T)> func = compute::make_function_from_source<U(T)>(
            src.first, src.second
      );

      assert(!data_.empty());
      y.reshape(*this);

      // transform individual chunks
      int i;
      for (i = 0; i < data_.size(); ++i)
      {
         compute::transform(
               data_[i].buffer().begin()
             , data_[i].buffer().end()
             , y.data()[i].buffer().begin()
             , func
             , q
         );
      }
   }

   /**
    @brief Provides direct access to the analog samples

    @return Reference to a list of data buffers
    */
   data_blocks_type& data()
   {
      return data_;
   }

   const data_blocks_type& data() const
   {
      return data_;
   }

   /**
    @brief Write bufffer to a strem
    
    @param os Output stream where data will be written
    */ 
   void output(std::ostream& os) const
   {
      for (const auto& buf : data_)
      {  
         buf.output(os);        
      }
   }

   /**
   @brief Write bufffer to a strem

   @param os Output stream where data will be written
   */
   template<typename _Strm>
   void output_channels(_Strm* strm) const
   {
      auto* cur = &data_[0];
      do
      {
         cur->output_channels(strm);
         cur = cur->next();
      } while(cur != nullptr);      
   }

   /**
    @brief Writes filtered data to array in column major or row major formats

    @tparam ColumnMajor true for coliumn major format (the default)
    @param out Pointer to buffer where to write the data. Has to be pre-allocated.
    */
   template<bool ColumnMajor = true>
   void copy(T* out)
   {
      assert(!data_.empty());
      size_t off;
      size_t off_out = 0;
      size_t i, j, n;
      // number of channels
      for(i = 0; i < dim_.channels; ++i)
      {
         for(j = 0; j < data_.size(); ++j)
         {
            n = data_[j].samples;
            off = i*n;
            if (ColumnMajor)
            {
               std::copy(&data_[j].host_ptr()[off], &data_[j].host_ptr()[off + n], &out[off_out]);
               off_out += n;
            }
            else
            {
               // TODO: row major (column major is what MATLAB uses)
            }
         }
      }
   }

private:
   // initialize buffers
   void _init(data_dimensions dim)
   {
      if (dim.channels > 0 && dim.samples > 0)
      {
         if (data_.empty())
         {
            // number of samples ber chunk
            size_t ns_buf = max_ns_ / dim.channels;
            // number of required buffers
            size_t nbuf = 1;
            if (ns_buf < dim.samples)
            {
               nbuf = (dim.samples / ns_buf);
               if (dim.samples % ns_buf != 0)
                  ++nbuf;
            }
            data_.reserve(nbuf);
            // reserve space
            size_t off = 0;
            // and allocate the buffers
            for (size_t i = 0; i < nbuf; ++i)
            {
               // might end up slightly larger than the others
               size_t n = (i == nbuf - 1 ? dim.samples - off : ns_buf);
               data_.emplace_back( ctx_
                                 , data_dimensions{ 0, dim.channels, dim.sample_offset + off, n, dim.format, dim.sampling_rate}
                                 , dim
                                 , i > 0 ? &data_[i - 1] : nullptr);
               off += n;
            }
         }
         else
         {
            // check that we have space for the data
            size_t ns = dim.samples;
            size_t off = 0;

            for(size_t i = 0; i < data_.size(); ++i)
            {
               if (ns == 0)
               {
                  // unlink remaining buffers
                  data_[i].clear();
                  data_[i].link_prev(nullptr);
               }
               else
               {
                  // allocate as many samples as possible to current buffer
                  const size_t n = std::min(data_[i].capacity() / dim.channels, ns);
                  // change dimensions of buffer
                  data_dimensions dim_buf = dim;
                  dim_buf.samples = n;
                  dim_buf.sample_offset = dim.sample_offset + off;
                  data_[i].reshape(dim_buf);
                  // number of samples remaining to be allocated
                  ns -= n;
                  off += n;
                  // make sure that buffers are linked
                  if (i > 0)
                     data_[i - 1].link_next(&data_[i]);
               }
            }
            // still any data left to be allocated ?
            if (ns > 0)
            {
               /*
                * allocate more chunks
                */
               // max samples per chunk
               const size_t max_ns_buf = max_ns_ / dim.channels;
               while (ns > 0)
               {
                  size_t ns_buf = std::min(max_ns_buf, ns);
                  data_.push_back(data_block_type{
                                      ctx_
                                    , data_dimensions{ 0, dim.channels, dim.sample_offset + off, ns_buf, dim.format, dim.sampling_rate}
                                    , dim
                                    , &data_[data_.size() - 1]
                                 });
                  if (data_.size() > 1)
                     data_[data_.size() - 2].link_next(&data_[data_.size() - 1]);
                  off += ns_buf;
                  ns -= ns_buf;
               }
            }
         }
      }
      dim_ = dim;
   }

   template<typename U>
   static std::pair<std::string, std::string> _scale_source(T xoff, U xfac)
   {
      size_t hash = 0;
      boost::hash_combine(hash, xoff);
      boost::hash_combine(hash, xfac);
      std::stringstream name;
      name << "scale_" << TYPE_ABBREVIATION(T) << "_" << TYPE_ABBREVIATION(U) << "_" << hash;
      std::stringstream ss;
      ss << opencl_type_name<U>() << " " << name.str()  <<
            "(" << opencl_type_name<T>() << " x) { return (" << opencl_type_name<U>() << ")((x - " << xoff << ")*" << xfac << "); }";
      return std::make_pair(name.str(), ss.str());
   }

};

} // namespace multichannel

#endif /* MULTICHANNEL_ANALOG_DATA_HPP */
