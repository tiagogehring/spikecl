#ifndef MULTICHANNEL_DATA_BLOCK_HPP
#define MULTICHANNEL_DATA_BLOCK_HPP

/// @defgroup Buffers
///@{

/**
 * @file   data_block.hpp
 * @author Tiago Gehring
 * @date   December, 2014
 * @brief  Basic data buffer for storing multi-channel data
 */

#include <vector>
#include <ostream>
#include <boost/compute/core.hpp>
#include <boost/compute/container/mapped_view.hpp>
#include <boost/compute/algorithm/copy.hpp>
#include "data_source/data_dimensions.hpp"

namespace compute = boost::compute;

namespace multichannel
{

template<typename T>
class iterator;

/**
 @brief Provides storage for multi-channel data

 Data is stored in a contiguous buffer. Multiple buffers can be linked for splitting data into chunks.

 @tparam T data type to be stored
 */
template<typename T>
class data_block
{
public:
   using data_dimensions = data_source::data_dimensions;
   using iterator_type = multichannel::iterator<T>;

private:
   compute::context ctx_;
   // OpenCL's directing mapping for host/device buffers is used to speed up data transfers
   // (on a pure CPU configuration no transfer is necessary)
   using mapped_view = typename compute::mapped_view<T>;
   // The actual data buffer
   std::vector<T> data_;
   // Maps the data buffer into OpenCL memory space
   mapped_view view_;
   // Buffer dimensions
   data_dimensions dim_;
   // Parent buffer dimensions
   data_dimensions dim_par_;
   // True if data buffer is currently mapped
   bool mapped_;
   // Prev/Next buffers with older/newer data chunks
   data_block<T>* next_;
   data_block<T>* prev_;

public:
   ///@{
   /**
    @brief Constructor

    @param prev Previous data buffer in the linked list
    */
   data_block(compute::context ctx, data_dimensions dim, data_dimensions dim_par, data_block<T>* prev = nullptr)
      : ctx_{ctx}
      , data_(dim.channels*dim.samples)
      , view_{&data_[0], data_.size(), ctx_}
      , dim_{std::move(dim)}
      , dim_par_{std::move(dim_par)}
      , mapped_{false}
      , next_{nullptr}
      , prev_{prev}
   {
      if (prev)
         prev->next_ = this;
   }

   // @todo: check data alignment
   // check data buffer size is compatible with the required alignment
   //ns_buf = vw_*static_cast<size_t>(ns_buf / vw_);
   // number of required buffers
   //size_t nbuf = 1;
   //if (ns_buf < nsamples)
   //{
    //  nbuf = (ns / ns_buf) + 1;
   //}

   // construct using dimensions the same as the parent
   data_block(compute::context ctx, data_dimensions dim, data_block<T>* prev = nullptr)
      : data_block(ctx, dim, dim, prev)
   {}

   // initialize wih an existing memory block
   data_block(compute::context ctx, data_dimensions dim, T* data, data_block<T>* prev = nullptr)
      : data_block(ctx, dim, dim, prev)
   {
      compute::copy(data, data + dim_.samples*dim_.channels, host_ptr());
   }

   // creates an empty object
   data_block()
      : mapped_{false}
      , next_{nullptr}
      , prev_{nullptr}
   {}

   ///@}
   data_block(const data_block& orig) = delete;

   data_block(data_block&& orig)
      : ctx_{std::move(orig.ctx_)}
      , data_(std::move(orig.data_))
      , view_{&data_[0], data_.size(), ctx_}
      , dim_{std::move(orig.dim_)}
      , dim_par_{std::move(orig.dim_par_)}
      , mapped_{false}
      , next_{orig.next_}
      , prev_{orig.prev_}
   {}

   data_block& operator=(const data_block& orig) = delete;

   data_block& operator=(data_block&& orig)
   {
      if (&orig != this)
      {
         ctx_ = orig.ctx_;
         data_ = std::move(orig.data_);
         view_ = mapped_view{&data_[0], data_.size(), ctx_};
         dim_ = std::move(orig.dim_);
         dim_par_ = std::move(orig.dim_par_);
         mapped_ = false;
         next_ = orig.next_;
         prev_ = orig.prev_;
      }
      return *this;
   }

   /**
    @brief Destructor
    */
   ~data_block()
   {}

   void clear()
   {
      dim_ = data_dimensions{0, 0, 0, 0, dim_.format, dim_.sampling_rate};
   }

   bool empty() const
   {
      return dim_.empty();
   }

   bool operator!() const
   {
      return empty();
   }

   /**
    @brief Buffer dimensions

    @return Dimensions
    */
   const data_dimensions& dimensions() const
   {
      return dim_;
   }

   /**
    @brief Capacity of allocated memory (in samples)

    @return Number of samples that can fit in the buffer
    */
   size_t capacity() const
   {
      return dim_.samples*dim_.channels;
   }

   /**
    @brief Buffer dimensions

    @return Dimensions
    */
   const data_dimensions& parent_dimensions() const
   {
      return dim_par_;
   }

   /**
    @brief Next buffer pointer (for data that is divided into multiple chunks)

    @return Next buffer address (or nullptr if not available)
    */
   data_block<T>* next() const
   {
      return next_;
   }

   /**
    @brief Previous buffer (for muli-chunk data)

    @return Previous buffer address (or nullptr if not available)
    */
   data_block<T>* prev() const
   {
      return prev_;
   }

   void link_prev(data_block<T>* prev)
   {
      if (prev)
         prev->next_ = this;
      prev_ = prev;
   }

   void link_next(data_block<T>* next)
   {
      if (next)
         next->prev_ = this;
      next_ = next;
   }

   void reshape(data_dimensions dim)
   {
      assert(dim.samples * dim.channels <= data_.size());
      dim_ = dim;
   }

   /**
    @brief Returns the value of a single sample

    Note: Not very efficient, use other access methods if multple samples are to be accesses

    @param chan Channel number
    @param s Sample number (relative to the original data source, @see sample_offset)

    @return Sample value
    */
   T sample_value(size_t chan, size_t s, compute::command_queue& q) const
   {
      assert(has_sample(s));
      // call map() to make sure that buffer data is synchronized and accesible by the host
      //map(q);
      q.finish();
      return (dim_.format ==
            data_source::storage_format::column_major ? data_[chan*dim_.samples + (s - dim_.sample_offset)]
                                                      : data_[dim_.channels*(s - dim_.sample_offset) + chan]);
   }

   /**
    @brief Returns true if a given sample number is contained in this data chunk

    @param s Sample number (relative to the original data source, @see sample_offset)

    @return True if sample is in this buffer
    */
   bool has_sample(size_t s) const
   {
      return ( (dim_.sample_offset <= s) && (dim_.sample_offset + dim_.samples > s) );
   }

   /**
    @brief Returns a raw-data pointer to the buffer that might be accessed by the host (e.g. CPU)

    @return Pointr to buffer
    */
   T* host_ptr()
   {
      return &data_[0];
   }

   const T* host_ptr_const() const
   {
      return &data_[0];
   }

   /**
    @brief Returns a reference to the OpenCL mapped view. This might be used by the OpenCL device to access the buffer.

    @return Mapped view reference.
    */
   const mapped_view& buffer()
   {
      return view_;
   }

   /**
    @brief Maps buffer so that OpenCL <-> host data are synchronized
    */
   void map(compute::command_queue& q)
   {
      if (!mapped_)
      {
         view_.map(q);
         mapped_ = true;
      }
   }

   /**
    @brief Unmaps memory (no guarantee that host <-> OpenCL memory is synchronized)
    */
   void unmap(compute::command_queue& q)
   {
      if (mapped_)
      {
         view_.unmap(q);
         mapped_ = false;
      }
   }

   /**
   @brief Write bufffer to a strem

   @param os Output stream where data will be written
   */
   void output(std::ofstream& os) const
   {
      // write to stream
      size_t sz = dim_.channels*dim_.samples*sizeof(T);
      os.write(reinterpret_cast<const char*>(&host_ptr_const()[0]), sz);
      if (os.bad())
      {
         throw std::runtime_error{ "Error writing data to file (disk full?)" };
      }      
   }

   /**
   @brief Write bufffer to a strem

   @param os Output stream where data will be written
   */
   template<typename _Strm>
   void output_channels(_Strm* strm) const
   {      
      if (dim_.format == data_source::storage_format::column_major)
      {
         for (auto c = 0; c < dim_.channels; ++c)
         {
            for (auto i = 0; i < dim_.samples; ++i)
               strm[c].write(reinterpret_cast<const char*>(&data_[c*dim_.channels + i]), sizeof(T));               
         }
      }
      else
      {
         const size_t n = dim_.samples*dim_.channels;
         for (size_t i = 0; i < n; ++i)
            strm[i % dim_.channels].write(reinterpret_cast<const char*>(&data_[i]), sizeof(T));
      }      
   }

   /**
   @brief Write bufffer to a strem

   @param os Output stream where data will be written
   */
   void output(std::ostream& os) const
   {
      // write to stream
      size_t sz = dim_.channels*dim_.samples*sizeof(T);
      os.write(reinterpret_cast<const char*>(&host_ptr_const()[0]), sz);
      if (os.bad())
      {
         throw std::runtime_error{ "Error writing data to file (disk full?)" };
      }
   }
};

template<typename T>
std::ostream& operator << (std::ostream& out, data_block<T>& blk)
{
   T* p = blk.host_ptr();
   const auto& dim = blk.dimensions();
   for (size_t i = 0; i < dim.channels*dim.samples; ++i)
      out << p[i] << ", ";
   return out;
}

} // namespace multichannel

///@}

#endif /* MULTICHANNEL_DATA_BUFFER_HPP */
