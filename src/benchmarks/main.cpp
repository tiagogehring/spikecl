#include <iostream>
#include <iomanip>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include "data_source/mcs_file_analog.hpp"
#include "config/common_setup.hpp"
#include "config/filter_setup.hpp"
#include "config/spike_detection_setup.hpp"
#include "mea_data.hpp"
#include "spikes_store.hpp"
#include "utility/filename_match.hpp"

namespace fs = boost::filesystem;
namespace po = boost::program_options;

using data_source_type = data_source::mcs_file_analog;
using mea_data_type = mea_data<fp_type, data_source_type>;
// using mea_data_type = mea_data<fp_type, data_source_type, dsp::filter< typename data_source_type::data_type, float>>;


int main(int argc, char* argv[])
{
   using namespace std;

   init_logging("", logging_severity_level::info, logging_severity_level::none);

   /********
    * Process command line arguments
    */
   mea_data_type::options opts = mea_data_type::default_options();

   // the next objects process command line arguments and initialize different
   // aspects of the program (OpenCL device, filtering, spike detection, etc)
   common_setup cs;
   filter_setup fs;
   spike_detection_setup sds;

   std::vector<size_t> nchan { {1, 2, 4, 8, 16, 32} };
   size_t nrep = 4;
   size_t nchan_max = 32;

   try
   {
	   po::options_description desc("General", 120);
	   desc.add_options()
		  ("input", po::value<vector<string>>(), "input file names");
      // common options
      cs.add_options(desc);

	   po::positional_options_description pos_desc;
	   pos_desc.add("input", -1);

	   po::variables_map vm;
	   po::store( po::command_line_parser(argc, argv)
				 .options(desc)
				 .positional(pos_desc)
				 .run(), vm);
	   po::notify(vm);

	   if (vm.count("input") == 0)
	   {
		  cerr << "No input file specified!";
		  cout << endl << desc << endl;
		  return 1;
	   }
	   vector<string> files = vm["input"].as<vector<string>>();
	   vector<fs::path> files_p;

	   // process input files
	   for(auto& fn : files)
	   {
		  // is it a file mask?
		  if (fn.find('*') == string::npos)
		  {
			 // regular file - just check if it exists
			 if (!fs::exists(fn))
			 {
				BOOST_LOG_SEV(g_log, logging_severity_level::error) << "File " << fn << " not found!" << endl;
				return 1;
			 }
			 if (fs::is_directory(fn))
			 {
				BOOST_LOG_SEV(g_log, logging_severity_level::error) << fn << " is a directory!" << endl;
				return 1;
			 }
			 files_p.push_back(fn);
		  }
		  else
		  {
			 // file mask -> match files
			 fs::path p(fn);

			 fs::directory_iterator end_itr; // Default ctor yields past-the-end
			 for( fs::directory_iterator i(p.parent_path()); i != end_itr; ++i )
			 {
				 // Skip if not a file
				 if( !fs::is_regular_file( i->status() ) )
					continue;

				 try
				 {
					 if (!filename_match(fn, i->path().string()))
						 continue;
				 }
				 catch (exception&)
				 {
					 throw runtime_error{ "Invalid file mask!" };
				 }

				 files_p.push_back(i->path());
			 }
		  }
	   }

      // process other command line options, initialize different sub-systems
      cs.process_options(vm); // OpenCL and basic options
      opts.common = cs.values();
      opts.spike_detection = spike_detection_setup::default_values;

      // file instance
      data_source::mcs_file_analog f{};

      size_t fi = 0;
      for(const auto& fpath : files_p)
      {
         ++fi;
         f.load_file(fpath.string());
         size_t ns = f.dimensions().samples*f.dimensions().channels;
         float mb = static_cast<float>(ns*sizeof(data_source_type::data_type)/1024/1024);

         BOOST_LOG_SEV(g_log, logging_severity_level::info) << "** Running benchmarks for file " << fpath.string()
            << " [" << mb << "MiB] " << fi << "/" << files_p.size() << "**";

         for (auto nc : nchan)
         {
            // main object that does the processing of files
            // set number of channels directly
            size_t nci = std::min(nchan_max, f.dimensions().channels);
            cs.parallel_channels(nc);
            opts.common.channels = nci;
            auto max_vw = cs.maximum_vector_width(nci);
            std::cerr << "\nVW: " << max_vw;
            opts.common.vector_width = max_vw;
            opts.common.parallel_channels = cs.optimal_parallel_channels(max_vw);
            mea_data_type mea_dt{cs.device(), opts};

            // construct filter
            mea_dt.enable_filter(fs.filter_coefficients(f.dimensions().sampling_rate), f);

            size_t nspk = 0;
            size_t per_read = 0;
            for (int nr = 0; nr < nrep; ++nr)
            {
               // buffer used to store spikes
               spikes<fp_type> spks{ 500000 };

               BOOST_LOG_SEV(g_log, logging_severity_level::info) << "Repetition: " << nr + 1;
               // process file
               nspk = 0;
               f.position(0);
               mea_dt.process(f, &spks, nullptr, [&](spikes<fp_type>* new_spks) { nspk += new_spks->count(); });
               // reading perfomance: use only first iteration because of caching
               if (nr == 0)
               {
                  per_read = mea_dt.operation_statistics(mea_data_operation::read_data).first;
                  mea_dt.clear_statistics();
               }
            }

            BOOST_LOG_SEV(g_log, logging_severity_level::info)
               << "Performance (" << nc << " parallel channels), "
               << "Reading: " << per_read << "us"
               << "\tFilter: " << mea_dt.operation_statistics(mea_data_operation::filter_data).first/(nrep - 1) << "us"
               << "\tDetect spikes: " << mea_dt.operation_statistics(mea_data_operation::detect_spikes).first/(nrep - 1) << "us";
         }
      }
   }
   catch(exception& e)
   {
      BOOST_LOG_SEV(g_log, logging_severity_level::error) << e.what();
      return 1;
   }

   return 0;
}

