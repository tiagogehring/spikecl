#ifndef MCSTREAM_WRAPPER_HPP
#define MCSTREAM_WRAPPER_HPP

#include <ctime>
#include <cstdint>

void* mcstream_create_file();
void mcstream_delete_file(void*);
short mcstream_load_file(void* f, const char* name);
double mcstream_sampling_rate(void* f);
time_t mcstream_start_time(void* f);
time_t mcstream_stop_time(void* f);
uint64_t mcstream_timespan_ns(void* f);
size_t mcstream_stream_count(void* f);
void* mcstream_stream(void* f, size_t n);

double mcstream_units_per_ad(void* s);
const char* mcstream_buffer_id(void* s);
int mcstream_ad_zero(void* s);
size_t mcstream_channel_count(void* s);
const char* mcstream_channel_identification(void* s, int n);
size_t mcstream_raw_read(void* pf, size_t is, size_t pos, size_t n, unsigned short* out, size_t bs);

#endif
