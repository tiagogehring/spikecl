#include "mcstream_wrapper.hpp"
#include "MCSAStreamFile.h"
#include "MCSAStream.h"
#include "MCSAChannel.h"

time_t to_time_t(CMCSATimeStamp* ts)
{
   struct tm t;
   t.tm_sec = ts->GetSecond();
   t.tm_min = ts->GetMinute();
   t.tm_hour = ts->GetHour();
   t.tm_mday = ts->GetDay();
   t.tm_mon = ts->GetMonth();
   t.tm_year = ts->GetYear();
   return mktime(&t);
}

void* mcstream_create_file()
{
   return new CMCSAStreamFile();
}

void mcstream_delete_file(void* f)
{
   CMCSAStreamFile* pfile = reinterpret_cast<CMCSAStreamFile*>(f);
	delete pfile;
}

short mcstream_load_file(void* f, const char* name)
{
   return reinterpret_cast<CMCSAStreamFile*>(f)->OpenFile(name);
}

double mcstream_sampling_rate(void* f)
{
   return (reinterpret_cast<CMCSAStreamFile*>(f))->GetMillisamplesPerSecond() / 1000.;
}

time_t mcstream_start_time(void* f)
{
	return to_time_t((reinterpret_cast<CMCSAStreamFile*>(f))->GetStartTime());
}

time_t mcstream_stop_time(void* f)
{
   return to_time_t((reinterpret_cast<CMCSAStreamFile*>(f))->GetStopTime());
}

size_t mcstream_stream_count(void* f)
{
   return reinterpret_cast<CMCSAStreamFile*>(f)->GetStreamCount();
}

void* mcstream_stream(void* f, size_t n)
{
   return reinterpret_cast<CMCSAStreamFile*>(f)->GetStream(static_cast<long>(n));
}

uint64_t mcstream_timespan_ns(void* f)
{
   CMCSATimeStamp* ts = reinterpret_cast<CMCSAStreamFile*>(f)->GetStopTime();
   long second = ts->GetSecondFromStart();
   short milli = ts->GetMillisecondFromStart();
   short micro = ts->GetMicrosecondFromStart();
   short nano = ts->GetNanosecondFromStart();

   // timespan (probabaly don't need the nano-precision but then again let's be precise)
   uint64_t ns = static_cast<uint64_t>(second*1.e9 + milli*1.e6 + micro*1.e3 + nano);
   return ns;
}

double mcstream_units_per_ad(void* s)
{
   return (reinterpret_cast<CMCSAStream*>(s))->GetUnitsPerAD();
}

int mcstream_ad_zero(void* s)
{
   return (reinterpret_cast<CMCSAStream*>(s))->GetADZero();
}

const char* mcstream_buffer_id(void* s)
{
   return (reinterpret_cast<CMCSAStream*>(s))->GetBufferID();
}

const char* mcstream_channel_identification(void* s, int n)
{
   return (reinterpret_cast<CMCSAStream*>(s))->GetChannel(n)->GetName();
}

size_t mcstream_channel_count(void* s)
{
   return (reinterpret_cast<CMCSAStream*>(s))->GetChannelCount();
}

size_t mcstream_raw_read(void* pf, size_t is, size_t pos, size_t n, unsigned short* out, size_t bs)
{
   CMCSAStreamFile* f = reinterpret_cast<CMCSAStreamFile*>(pf);
   CMCSAStream* strm = f->GetStream(static_cast<long>(is));
   CMCSATimeStamp t1 = *f->GetStartTime();
   auto sr = mcstream_sampling_rate(pf);

   static const uint64_t sec_factor = 1000000000ll;
   static const uint64_t milli_factor = 1000000ll;
   static const uint64_t micro_factor = 1000ll;

   if (pos > 0)
   {
      // move to current position in the file - we do this by goin in micro-second time steps
      uint64_t ns = static_cast<uint64_t>(pos*1000000000. / sr);

      // convert first to smallest time slice (nano-seconds)
      long sec = static_cast<long>(ns / sec_factor);
	  short milli = static_cast<short>((ns - sec_factor*sec) / milli_factor);
	  short micro = static_cast<short>((ns - sec_factor*sec - milli_factor*milli) / micro_factor);
	  short nano = static_cast<short>(ns - sec_factor*sec - milli_factor*milli - micro_factor*micro);

      t1.SetSecondFromStart(sec);
      t1.SetMillisecondFromStart(milli);
      t1.SetMicrosecondFromStart(micro);
      t1.SetNanosecondFromStart(nano);
   }

   CMCSATimeStamp t2 = *f->GetStartTime();
   uint64_t ns = static_cast<uint64_t>(sec_factor*(pos + n)/((uint64_t)sr));
   long sec = static_cast<long>(ns / sec_factor);
   short milli = static_cast<short>((ns - sec_factor*sec) / milli_factor);
   short micro = static_cast<short>((ns - sec_factor*sec - milli_factor*milli) / micro_factor);
   short nano = static_cast<short>(ns - sec_factor*sec - milli_factor*milli - micro_factor*micro);

   t2.SetSecondFromStart(sec);
   t2.SetMillisecondFromStart(milli);
   t2.SetMicrosecondFromStart(micro);
   t2.SetNanosecondFromStart(nano);

   return strm->GetRawData(reinterpret_cast<short*>(out), static_cast<long>(bs), &t1, &t2);
}
