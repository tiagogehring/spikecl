classdef process_mcd_main < handle
    %MAIN_WINDOW Summary of this class goes here
    %   Detailed explanation goes here    
    properties(GetAccess = 'public', SetAccess = 'protected')  
         window = [];        
         param = [];
         filename_edit = [];
         channel_combo = [];
         raw_axis = []
         spikes_grid = [];     
         spikes_axis = [];
         t1_edit = [];
         t2_edit = [];
         
         spikes = [];
         raw_data = [];         
         freq = [];
         
         filename = '';
         channel = 1;
         % file name used to store data between sessions
         persist_fn = [];  
         data_dir = [];
         home_directory = [];         
    end
    
    methods
        function inst = process_mcd_main(varargin)         
            if ispc
                inst.home_directory = [getenv('APPDATA') '\'];
            else
                inst.home_directory = [getenv('HOME') '/'];
            end
            inst.data_dir = iff(ispc, fullfile(inst.home_directory, 'spikecl'), fullfile(inst.home_directory, ['.spikecl']));
            if ~exist(inst.data_dir, 'dir')                
                mkdir(inst.data_dir)
            end            
        
            inst.persist_fn = [inst.data_dir '/' 'process_mcd.mat'];                                     
            if exist(inst.persist_fn, 'file')
                load(inst.persist_fn);       
                inst.filename = fn;
                inst.channel = chan;
            end
                        
            inst.param = varargin;
            
            % create main window
            inst.window = figure('Visible','off', 'name', 'Process MCD file', ...
                'Position', [200, 200, 1280, 800], 'Menubar', 'none', 'Toolbar', 'none', 'resize', 'on');
            
            % GUI
            vbox = uiextras.VBox('Parent', inst.window, 'Padding', 5);
            hbox = uiextras.HBox('Parent', vbox);
            rawbox = uiextras.VBox('Parent', vbox, 'Padding', 5);
            mainbox = uiextras.VBox('Parent', vbox, 'Padding', 5);
                                    
            set(vbox, 'Sizes', [25 200 -1]);           
            
            uicontrol('Parent', hbox, 'Style', 'text', 'String', 'MCD file: ');
            inst.filename_edit = uicontrol('Parent', hbox, 'Style', 'edit', 'String', inst.filename);
            uicontrol('Parent', hbox, 'Style', 'pushbutton', 'String', '...', 'Callback', {@inst.select_file});  
            inst.raw_axis = axes('Parent', uicontainer('Parent', rawbox));
            
            uicontrol('Parent', hbox, 'Style', 'text', 'String', 'Channel: ');
            tmp = {};
            for i = 1:60
                tmp = [tmp, num2str(i)];
            end
            inst.channel_combo = uicontrol('Parent', hbox, 'Style', 'popupmenu', 'String', tmp);
            set(inst.channel_combo, 'Value', inst.channel);
            
            uicontrol('Parent', hbox, 'Style', 'pushbutton', 'String', 'Process', 'Callback', {@inst.process});       
            
            uicontrol('Parent', hbox, 'Style', 'text', 'String', 'T1:', 'HorizontalAlignment', 'left');            
            inst.t1_edit = uicontrol('Parent', hbox, 'Style', 'edit', 'String', '0', 'HorizontalAlignment', 'left', ...
                'Callback', {@inst.update_callback});                        
            
            uicontrol('Parent', hbox, 'Style', 'text', 'String', 'T2:', 'HorizontalAlignment', 'left');            
            inst.t2_edit = uicontrol('Parent', hbox, 'Style', 'edit', 'String', '2', 'HorizontalAlignment', 'left', ...                        
                'Callback', {@inst.update_callback});                        
            
            set(hbox, 'Sizes', [60 -1 40 60 40 100 40 40 40 40]);           
         
            inst.spikes_grid = uiextras.Grid('Parent', mainbox);               
             
            for i = 1:48                 
                panel = uiextras.Panel('Parent', inst.spikes_grid, 'BorderType', 'etchedout');                        
                % create sub-grid
                inst.spikes_axis = [inst.spikes_axis, axes('Parent', uicontainer('Parent', panel))];

            end                                                           
            
            set(inst.spikes_grid, 'RowSizes', -1*ones(1, 6), 'ColumnSizes', -1*ones(1, 8));                                                                              
        end    
        
        function select_file(inst, source, eventdata)
            [fn, path] = uigetfile({'*.mcd', 'MCD files'}, 'Select MCD file', inst.filename);
            set(inst.filename_edit, 'String', [path fn]);
        end
        
        function process(inst, source, eventdata)
            inst.filename = get(inst.filename_edit, 'String');
            inst.channel = get(inst.channel_combo, 'Value');
            
            if ~exist(inst.filename)
                msgbox('Invalid file name!');                
                return;
            end
            
            fn = inst.filename;
            chan = inst.channel;            
            save(inst.persist_fn, 'fn', 'chan');
            
            % process MCD file
            [res, ret] = process_mcd(inst.filename, 'Overwrite', 1, 'FirstChannel', chan - 1, 'Channels', 1, 'ExtractChannels', 1, inst.param{:});     
            
            if res == 0
                [inst.spikes, inst.freq, inst.raw_data] = process_options(ret, 'Spikes', [], 'SamplingRate', 0, 'RawData', []);                
%                 fid = fopen('E:\A_0002.raw','r');
%                 inst.raw_data = fread(fid, 'float');
%                 fclose(fid);
            end
            
            inst.update_callback;
        end        
        
        function show(inst)
            set(inst.window, 'Visible', 'on');  
        end
        
        function update_callback(inst, source, eventdata)
            t1 = str2num(get(inst.t1_edit, 'String'));
            t2 = str2num(get(inst.t2_edit, 'String'));
            s1 = max(floor(t1*inst.freq), 1);
            s2 = min(ceil(t2*inst.freq) + 1, length(inst.raw_data));
                
            if ~isempty(inst.raw_data) && s2 > s1
                % plot raw data
                set(inst.window, 'currentaxes', inst.raw_axis);
                hold off;
                plot(inst.raw_data(s1:s2), 'k', 'LineWidth', 2);                
                
                % see if we have another set of spikes
                % output directory is the same as the file name
                out_dir = fileparts(inst.filename);                
                [~, stem] = fileparts(inst.filename);
                path = fullfile(out_dir, [stem, '_spikes.x']);
                if exist(path, 'file')
                    spks = dlmread(path, ' ');
                    sel = spks(spks(:, 1) >= t1*1000. & spks(:, 1) <= t2*1000. & spks(:, 2) == inst.channel, 1);
                    sp = floor(sel*inst.freq/1000.);
                    hold on;
                    plot(sp - s1, inst.raw_data(sp, :), 'g*');                                         
                end
                
                if ~isempty(inst.spikes)
                    sel = inst.spikes(inst.spikes(:, 1) >= t1*1000. & inst.spikes(:, 1) <= t2*1000., 1);
                    sp = floor(sel*inst.freq/1000.);
                    hold on;
                    plot(sp - s1, inst.raw_data(sp, :), 'r*'); 
                    % plot spikes
                    for i = 1:length(inst.spikes_axis)
                        set(inst.window, 'currentaxes', inst.spikes_axis(i));
                        if i < length(sp)
                            plot(inst.raw_data(sp(i) - 50:sp(i) + 50), 'k', 'LineWidth', 2);     
                            ylim([-8.e-5 8.e-5]);
                        else
                            cla;
                        end
                    end
                end
            end
        end
    end
end