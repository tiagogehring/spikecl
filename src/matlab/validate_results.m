function res = validate_results( fn, varargin )
%VALIDATE_RESULTS Summary of this function goes here
%   Detailed explanation goes here
    [channel] = process_options(varargin, 'Channel', 0);
    
    % QSpikeTool results have to be avaiable in the sub-directory with 
    % the same name as the file
    [dir1, stem] = fileparts(fn);
    dir1 = fullfile(dir1, stem);
    if ~exist(dir1, 'dir')
        error('QSpikeTools results directory %s not found', dir);
    end
    
    % pre-process the data and store the results in a temporary folder
    if channel > 0 % if one channel was specified ...
        nchannels = 1; % ... process single channel
    else
        nchannels = 0; % 0 = all channels
    end
    [res, dir2] = process_mcd(fn, 'OutputDir', tempdir, 'FirstChannel', channel, ...
                                  'Channels', nchannels, 'Overwrite', 1, 'Waveforms', 'single', ...
                                  'SmoothingFactor', 0);
    if res ~= 0
        return;
    end

    res = 0;
    
    %% Compare results
    %% 1 - info file
    disp('Comparing info files...');
    path1 = fullfile(dir1, [stem, '_info.txt']);
    path2 = fullfile(dir2, [stem, '_info.txt']);
    if ~exist(path1, 'file') || ~exist(path2, 'file')
        disp('Info file not found!');
        return;
    end
    diff = 0;
    % compare it line by line
    f1 = fopen(path1, 'r');
    f2 = fopen(path2, 'r');
    l1 = fgetl(f1); l2 = fgetl(f2);
    while ischar(l1) && ischar(l2)
        if ~strcmp(l1, l2)
            diff = 1;
            break;
        end
        l1 = fgetl(f1); l2 = fgetl(f2);    
    end
    if ischar(l1) || ischar(l2)
        diff = 1;
    end
    fclose(f1);
    fclose(f2);
    if diff
        disp('Differences in info files detected!');
        return;
    end
    
    
    %% 2 - Spikes file
    path1 = fullfile(dir1, [stem, '_spikes.txt']);
    path2 = fullfile(dir2, [stem, '_spikes.txt']);
    if ~exist(path1, 'file') || ~exist(path2, 'file')
        disp('Spikes file not found!');
        return;
    end
    spks1 = dlmread(path1);
    spks2 = dlmread(path2);
    if channel > 0
        spks1 = spks1(abs(spks1(:, 3)) == channel, :);
        spks2 = spks2(abs(spks2(:, 3)) == channel, :);
    end
    fprintf('Comparing %d (%d) spikes...\n', size(spks1, 1), size(spks2, 1) );    
    spks1(:,2) = abs(spks1(:,2));
    spks1(:,3) = abs(spks1(:,3));
    spks2(:,2) = abs(spks2(:,2));
    spks2(:,3) = abs(spks2(:,3));
    figure;
    plot(spks1(:,1), spks1(:, 3), 'o');
    hold on;
    plot(spks2(:,1), spks2(:, 3), 'x');
    
    % spikes not detected by the new tool
    s = setdiff(spks1, spks2, 'rows', 'stable');
    if ~isempty(s)       
        disp(sprintf('%d spikes not detected by the new code!', size(s, 1)));
        arrayfun( @(i) fprintf('Channel %d (Electrode %d), %.4f\n', abs(s(i, 3)), abs(s(i, 2)), s(i, 1)), 1:size(s, 1));
        diff = 1;
        
        figure('name', 'spikes that were not detected');
        m = 5;
        n = 5;
        for i = 1:(m*n)
            if i > size(s, 1)
                break;
            end
            subplot(m, n, i);
            % spike number in this channel
            spks_chan = spks1(spks1(:, 2) == abs(s(i, 2)), :);
            pos = find(spks_chan(:, 1) == s(i, 1));
            spk_num = pos(1);
            plot_spike_1(stem, dir1, abs(s(i, 2)), spk_num);            
        end
    end
    % spikes detected by the new tool not in the original set
    s = setdiff(spks2, spks1, 'rows', 'stable');
    if ~isempty(s)
        disp(sprintf('%d spikes not in the original results!', size(s, 1)));
        arrayfun( @(i) fprintf('Channel %d (Electrode %d), %.4f\n', abs(s(i, 3)), abs(s(i, 2)), s(i, 1)), 1:size(s, 1));        
        diff = 1;
    end
    % compare waveforms
    %
    if (diff == 0)
        % from the new code, read the single
        path2 = fullfile(dir2, [stem, '_waveforms.csv']);
        if ~exist(path2, 'file')
            disp('Waveforms file not found!');
            return;
        end
        wf = dlmread(path2);
        % show N waveforms at random
        p = 1:size(wf, 1); % randperm(size(wf, 1));
        figure('name', 'sample spikes');
        m = 5;
        n = 5;
        for i = 1:(m*n)
            subplot(m, n, i);
            % electrode number - second value in the list
            elect = wf(p(i), 2);
            % spike number in this channel
            spk_num = sum(wf(1:p(i), 2) == elect);
            plot_spike_1(stem, dir1, elect, spk_num);
            hold on;
            plot(wf(p(i), 4:end), 'r');            
        end
    end
    
    if (diff == 0)
        disp('Done. Results are identical.');
    else
        disp('Done. Differences found in the results.');
    end
end

function plot_spike_1(stem, dir, elect, spk_num)
    % load QSpikeTools file
    path1 = fullfile(dir, [stem, '_chan_x' num2str(floor(elect)) '_spikes.mat']);
    if ~exist(path1, 'file')
        disp('QSpikeTool spikes file for channel not found!');
        return;
    end
    load(path1);
    plot(spikes(spk_num, :), 'b', 'LineWidth', 2);            
end