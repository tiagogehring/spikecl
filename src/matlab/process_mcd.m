function [ ret, out ] = process_mcd( fn, varargin )
%PROCESS_MCD Calls the external spiketool code to process a MCD file
% Please refer to spiketool help (run it without arguments) for a description of the 
% different parameters;
% RETURNS: program exit value (0 = success) and directory where data was
% stored
    out = {};
    % parse options
    [ exe_path, out_dir, info_file, filter, spikes, waveforms, ...
      first_chan, channels, coeff_file, spk_thresh, spk_sign, refr_time, ...
      points_pre, points_post, smooth_fac, overwrite, device, extract_chan, ...
        block_sz, bursts, other_files, output_bins, burst_thresh, burst_active] ...
      = process_options(varargin, ...
        'SpikeToolPath', '../../build/bin', ...
        'OutputDir', '', ...
        'InfoFile', 1, ...
        'Filter', 1, ...        
        'Spikes', 1, ...
        'Waveforms', 'none', ...
        'FirstChannel', 0, ...
        'Channels', 0, ...
        'CoeffFile', '', ...
        'SpikeThreshold', 5, ...
        'SpikeSign', 'both', ...
        'RefractoryTime', 2.5, ...
        'PointsPre', 20, ...
        'PointsPost', 44, ...
        'SmoothingFactor', 0, ...
        'Overwrite', 0, ...
        'Device', 0, ...
        'ExtractChannels', 0, ...
        'BlockSize', 0, ...        
        'Bursts', 0, ...
        'AdditionalFiles', {}, ... 
        'BinsStatistics', 0, ...
        'BurstThreshold', 0, ...
        'BurstActiveSites', 2 ...
    );
    
    % look for spiketool executable
    if exe_path(1) == '.' % relative path?
        exe_path = fullfile(fileparts(mfilename('fullpath')), exe_path);
    end 
    if ~exist(exe_path, 'dir')
        error('Spiketool executable directory %s not found', exe_path);
    end
    if isunix
        exe_path = fullfile(exe_path, 'spiketool');
    elseif ispc
        exe_path = fullfile(exe_path, 'Release/spiketool.exe');
    else
        error('What is this thing running on?');
    end
    if ~exist(exe_path, 'file')
        error('Spiketool executable %s not found', exe_path);
    end    
    if ~exist(fn, 'file')
        error('File %s not found', fn);
    end

    if isempty(out_dir)
        % use same directory where the file is located
        out_dir = fileparts(fn);
    end
    
    % assemble command string
    cmd = [exe_path, ' --output ', out_dir,  ' --first-channel ', num2str(first_chan), ...
          ' --channels ', num2str(channels), ' --threshold ', num2str(spk_thresh), ...
          ' --refractory-time ', num2str(refr_time), ' --sign ', spk_sign, ...
          ' --points-pre ', num2str(points_pre), ' --points-post ', num2str(points_post), ...
          ' --smoothing-factor ', num2str(smooth_fac) ...
          ' --waveforms ', waveforms, ' --device ', num2str(device) ];
    if (~info_file)
        cmd = [cmd, ' --no-info'];
    end    
    if (~filter)
        cmd = [cmd, ' --no-filter'];
    end
    if (~spikes)
        cmd = [cmd, ' --no-spikes'];
    end
    if (overwrite)
        cmd = [cmd, ' --overwrite'];
    end
    if ~isempty(coeff_file)
        cmd = [cmd, ' --coeff-file ', coeff_file];
    end    
    if extract_chan
        cmd = [cmd, ' --extract-channels'];        
    end
    if block_sz > 0
        cmd = [cmd, ' --block-size=', num2str(block_sz)];
    end
    if bursts
        cmd = [cmd, ' --bursts'];
        cmd = [cmd, ' --burst-threshold=', num2str(burst_thresh)]; 
        cmd = [cmd, ' --burst-active-sites=', num2str(burst_active)]; 
    end
    if ~isempty(other_files)
        cmd = [cmd, ' --merge'];
    end
    if output_bins
        cmd = [cmd, ' --bin-statistics'];
    end
    cmd = [cmd, ' ', fn];
    for i = 1:length(other_files)
        cmd = [cmd ' ' other_files{i}];       
    end
    
    disp(['Command line: ' cmd]);
              
    %% Horrible hack: we have to change the library path manually before running the external command 
    %% since there seems to be no way of doing this directly
    % save library paths
    % prev_ld_path = getenv('LD_LIBRARY_PATH');
    % change library path in Mallab
    % setenv('LD_LIBRARY_PATH', '/lib:/usr/lib64:/usr/lib:/lib64:/usr/local/lib')
    % run spiketool    
    ret = system(cmd, '-echo');
    % system('set', '-echo');
    % set old path back
    % setenv('LD_LIBRARY_PATH', prev_ld_path);
    
    % output directory is the same as the file name
    if isempty(out_dir)
        out_dir = fileparts(fn);
    end
    [~, stem] = fileparts(fn);
    dir = fullfile(out_dir, stem);
    if ~exist(dir, 'dir')
        error('Output directory %s not found', dir);
    end
    
    % extract general information about the file
    path = fullfile(dir, [stem, '_info.txt']);
    if ~exist(path, 'file')
        error('Info file not found!');
    end

    fid = fopen(path, 'r');
    out = [out, 'RecordingTime', str2num(fgetl(fid))];                    
    out = [out, 'Samples', str2num(fgetl(fid))];        
    out = [out, 'SamplingRate', str2num(fgetl(fid))];    
    tot_channels = sscanf(fgetl(fid), 'Nelec%d');
    out = [out, 'Channels', tot_channels];
    fclose(fid);
    
    % read spikes file
    path = fullfile(dir, [stem, '_spikes.txt']);
    if exist(path, 'file')   
        spks = {};
        try
            spks = dlmread(path, ',', 1, 0);
        catch
            % ignore
        end
        if ~isempty(spks)
            out = [out, 'Spikes', spks];
        end
    end
    
    % read bursts file
    if bursts
        path = fullfile(dir, [stem, '_bursts.txt']);
        if exist(path, 'file')        
            tmp = {};
            try
                tmp = dlmread(path, ',', 1, 0);
            catch
                % ignore
            end
            if ~isempty(tmp)
                out = [out, 'Bursts', tmp];
            end
        end
    end
    
    if output_bins
        % other bursts statistics file
        path = fullfile(dir, [stem, '_bursts_bins.txt']);
        if exist(path, 'file')        
            tmp = {};
            try            
                tmp = dlmread(path, ',', 1, 0);
            catch
            end
            if ~isempty(tmp)
                out = [out, 'BurstsBins', tmp];
            end
        end

        path = fullfile(dir, [stem, '_bursts_bins_stat.txt']);
        if exist(path, 'file')        
            tmp = [];
            try        
                tmp = dlmread(path, ',', 1, 0);
            catch
                % ignore
            end
            if ~isempty(tmp)
                out = [out, 'BurstsBinsStatistics', tmp];
            end
        end
    end
    
    if extract_chan
        raw = [];
        for c = first_chan:first_chan + channels - 1
            path = fullfile(dir, [stem, '_chan' num2str(c) '.dat']);
            if ~exist(path, 'file')
                error('Channel data file not found!');
            end
            fid = fopen(path,'r');
            raw = [raw, fread(fid, 'float')];
            fclose(fid);
        end 
        out = [out, 'RawData', raw];            
    end
end