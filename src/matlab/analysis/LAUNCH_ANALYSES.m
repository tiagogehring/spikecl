function LAUNCH_ANALYSES(mypath, analdir)
%LAUNCH_ANALYSES implements all the analyses performed in the QSpike Tools
% package. All the implemented scripts are expected to reside in the
% /data1/WWWINTERFACE/QSpikeTool/matlab directory. Most of the analyses are
% expected to accept the spike time stamp file *_spikes.txt as an input.
% This function accepts the following two inputs:
%       mypath    : the directory containing all the *_spikes.mat files
%       analdir   : the output directory
%                   /data1/WWWINTERFACE/OUTPUT_PREPROCESSED_FILES
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 01:33)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 01:33:50 $
% 
%
% All the implemented analysis
%

global INFO FIGS OUTPATH SHOW

SHOW = 0;
addpath(pwd);
if (mypath(end) == '/') | (mypath(end) == '\')
    mypath = mypath(1:end-1);
end

disp(sprintf('the path provided is: %s',mypath)); 

disp(sprintf('the output path provided is: %s',analdir)); 

mypath_old = mypath;

% remove the first . present in the path for creating the file
if (mypath(1) == '.') & (mypath(2) == '/')
  updatedPath = mypath(3:end);
%elseif strfind(mypath,'./')
%    updatedPath = strrep(mypath,'./', '/');
else
    updatedPath = mypath;
end

% go to the output directory

oldPath = cd(analdir);

mypathForFileName = regexprep(updatedPath,'/','_');
%mypath = [mypath '/spk_waves'];

OUTPATH = mypath;

%plot_waves(mypath); 

% TG: check if spikes file already exist as it is generated automatically now in the pre-processing stage    
spikes_fname = sprintf('%s/*_spikes.txt', mypath);
P = dir(spikes_fname);
N = length(P);
if N == 0
% </TG>
    % when using the spiketool
    simple_spike_sorting(mypath); %this function is commented at the moment
    %becuase when the program runs in parallel all the channels try to create a
    %spike file, eventually causing an error on file access. if you intend to
    %run the analyses serially, please enable this and disable the call of the
    %function in "sortSpikesMM" from the "mcd_new.cgi" script 
end

P = dir(spikes_fname);
N = length(P);
if (N==0)
  disp(sprintf('no spikes.txt files at location: %s',mypath)); 
  return; 
end;

fname = sprintf('%s/%s',mypath,P(1).name);
disp(sprintf('file to be loaded is: %s',fname));

try % check if the spike file is loaded properly or not
  A = load(fname);
  disp(sprintf('spike information file ''%s'' loaded successfully!!',fname));
  % sort the spikes as per electrodes
  [~,indx] = sort(abs(A(:,3)));
  A = A(indx,:);
  
catch exception1 
   errorString = sprintf(['something went wrong during loading %s \n %s \n at %s \n in line %s \n %s'],...
                  fname, exception1.identifier, exception1.stack(1,1).name, num2str(exception1.stack(1,1).line), exception.message);
  disp(sprintf('encountered error during processing: \n %s',errorString));
end

out = info_file(fname);
if (out==-1)
  disp(sprintf('info_file ''%s'' not found!',fname)); 
  return; 
else
  disp(sprintf('%s_info.txt file loaded successfully',fname(1:end-11)));
end

if (size(A,2)<2), return; end;
C = remove_artifact(A);
if size(C,2)<2, return; end;

try
improved_plot_waves(mypath, C);
disp('improved_plot_waves executed successfully!');
catch exception1 
  errorString = sprintf(['something went wrong during execution of the function improved_plot_waves \n %s \n at %s \n in line %s \n %s'],...
                  exception1.identifier, exception1.stack(1,1).name, num2str(exception1.stack(1,1).line), exception1.message);
  disp(sprintf('encountered error during processing: \n %s',errorString));
end

try
active_electrodes(C);
disp('active_electrodes executed successfully!');
catch exception1 
  errorString = sprintf(['something went wrong during execution of the function active_electrodes \n %s \n at %s \n in line %s \n %s'],...
                  exception1.identifier, exception1.stack(1,1).name, num2str(exception1.stack(1,1).line), exception1.message);
  disp(sprintf('encountered error during processing: \n %s',errorString));
end


try
% pelt_matrix = burst_analysis(C);
burst_analysis_MM(C);
disp('burst_analysis_MM executed successfully!');
catch exception1 
  errorString = sprintf(['something went wrong during execution of the function burst_analysis \n %s \n at %s \n in line %s \n %s'],...
                  exception1.identifier, exception1.stack(1,1).name, num2str(exception1.stack(1,1).line), exception1.message);
  disp(sprintf('encountered error during processing: \n %s',errorString));
end

% try
% vanpelt_plot_complete(pelt_matrix);
% disp('vanpelt_plot_complete executed successfully!');
% catch exception1 
%   errorString = sprintf(['something went wrong during execution of the function vanpelt_plot_complete \n %s \n at %s \n in line %s \n %s'],...
%                   exception1.identifier, exception1.stack(1,1).name, num2str(exception1.stack(1,1).line), exception1.message);
%   disp(sprintf('encountered error during processing: \n %s',errorString));
% end

try
largest_burst(C);
disp('largest_burst executed successfully!');
catch exception1 
  errorString = sprintf(['something went wrong during execution of the function largest_burst \n %s \n at %s \n in line %s \n %s'],...
                  exception1.identifier, exception1.stack(1,1).name, num2str(exception1.stack(1,1).line), exception1.message);
  disp(sprintf('encountered error during processing: \n %s',errorString));
end


try
largest_burst_raw(C);
disp('largest_burst_raw executed successfully!');
catch exception1 
  errorString = sprintf(['something went wrong during execution of the function largest_burst_raw\n %s \n at %s \n in line %s \n %s'],...
                  exception1.identifier, exception1.stack(1,1).name, num2str(exception1.stack(1,1).line), exception1.message);
  disp(sprintf('encountered error during processing: \n %s',errorString));
end


try
sample_raster_and_psth(C);
disp('sample_raster_and_psth executed successfully!');
catch exception1 
  errorString = sprintf(['something went wrong during execution of sample_raster_and_psth\n %s \n at %s \n in line %s \n %s'],...
                  exception1.identifier, exception1.stack(1,1).name, num2str(exception1.stack(1,1).line), exception1.message);
  disp(sprintf('encountered error during processing: \n %s',errorString));
end


try
frequency_distribution(C);
disp('frequency_distribution executed successfully!');
catch exception1 
  errorString = sprintf(['something went wrong during execution of the function frequency_distribution\n %s \n at %s \n in line %s \n %s'],...
                  exception1.identifier, exception1.stack(1,1).name, num2str(exception1.stack(1,1).line), exception1.message);
  disp(sprintf('encountered error during processing: \n %s',errorString));
end


try

save([mypath filesep 'GLOBAL_INFO.mat'], 'INFO')
write_global_info_to_text();
disp('Activity Information successfully saved')
cd(oldPath)
catch exception1 
  errorString = sprintf(['something went wrong during saving the GLOBAL_INFO.mat file\n %s \n at %s \n in line %s \n %s'],...
                  exception1.identifier, exception1.stack(1,1).name, num2str(exception1.stack(1,1).line), exception1.message);
  disp(sprintf('encountered error during processing: \n %s',errorString));
end

%generateLatexFile(OUTPATH, INFO.BURSTS.largest_burst_bin, INFO.BURSTS.largest_burst_plot_zoomed_bin);

%marom_burst_detect(A);
%pelt_matrix = my_burst_detect(A);
%vanpelt_plot(pelt_matrix);
end
