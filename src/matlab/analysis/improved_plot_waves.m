function improved_plot_waves(mypath,A)
%improved_plot_waves plots the waveforms of activities detected by each
% electrodes during a recording session. The plotting of negative and 
% positive activities are plotted separately in two subplots with means of 
% the individual waveforms detected by each electrodes in black and the 
% standard deviation in grey. The number of spikes detected by each
% electrode is also shown. 
%
% inputs: 
%       mypath    : full path of the directory where the files are
%                   located. this directory is usually located at the
%                   /data1/WWWINTERFACE/OUTPUT_PREPROCESSED_FILES
%       A         : nx3 matrix
%                   first column is the spike time stamp
%                   second column is the electrode name
%                   third column is the electrode number
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 00:08)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 00:08:09 $
%  

global SHOW

if (mypath(end) == '/') | (mypath(end) == '\')
    mypath = mypath(1:end-1);
end



fullpath = sprintf('%s/*_chan_x*_spikes.mat', mypath);
P        = dir(fullpath);
N        = size(P,1);
csv = 0;

% TG: added support for spikes in the CSV format
if N == 0
    fullpath = sprintf('%s/*_chan_x*_spikes.csv', mypath);
    P        = dir(fullpath);
    N        = size(P,1);
    csv = 1;
end
% </TG>

figure(1);
if ~SHOW
    set(gcf, 'Visible', 'off')
end

set(gcf, 'renderer', 'zbuffer');
set(gcf, 'Color', [1 1 1]); 
%set(gcf, 'Visible', 'off');
HH1 = mysubplot(8,8,0.01,0.01,0.01);

figure(2);
if ~SHOW
    set(gcf, 'Visible', 'off')
end

set(gcf, 'renderer', 'zbuffer');
set(gcf, 'Color', [1 1 1]); 
%set(gcf, 'Visible', 'off');
HH2 = mysubplot(8,8,0.01,0.01,0.01);


MAX = -999;
MIN = 999;

for k=1:N,                  % For each of them..
    myerror = 0;
    try
        % TG: added support for spikes in the CSV format
        if csv
            if P(k).bytes > 0
                spikes = csvread(sprintf('%s/%s',mypath,P(k).name));
                % first column is the sample time
                index = spikes(:,1);
                spikes = spikes(:, 2:size(spikes, 2));
            else
                spikes = zeros(0, 64);
                index = [];
            end
        else
            load(sprintf('%s/%s',mypath,P(k).name));
        end
        % </TG>
    catch err
        myerror = 1;
        disp('plot_wave() try-catch error!');
    end
    if (myerror==0)	
        tmp = P(k).name(end-12:end-11);
        row = str2num(tmp(2));
        col = str2num(tmp(1));

%         row = str2num(tmp(1));
%         col = str2num(tmp(2));

        indp = []; indn = [];
        for m=1:size(spikes,1),
            isartifact = isempty(find(index(m)==A(:,1)));
                tmq = spikes(m,:) - mean(spikes(m,50:64));
            if (~isartifact),
                if (tmq(20) < 0),  
                    indp = [indp, m]; %plot(tmq, 'k');
                else
                    indn = [indn, m]; %plot(tmq, 'r');
                end
            end
        end

        mwave = mean(spikes(indp,:),1);
        mwave = mwave - mean(mwave(50:64));
        swave = std(spikes(indp,:),1);
        MAX   = max([MAX max(mwave)]);
        MIN   = min([MIN min(mwave)]);

        axes(HH1(col + 8 * (row-1))); 
        if ~SHOW
            set(gcf, 'Visible', 'off')
        end
        hold on;
        Q = confplot(1:size(spikes, 2), mwave, swave);
        set(Q(1), 'Color', [0 0 0], 'LineWidth', 2)
        T1 = text(55,-5e-6, tmp);
        T2 = text(0,5e-6, sprintf('[%d]',length(indp)));

        mwave = mean(spikes(indn,:),1);
        mwave = mwave - mean(mwave(50:64));
        swave = std(spikes(indn,:),1);
        MAX   = max([MAX max(mwave)]);
        MIN   = min([MIN min(mwave)]);

        axes(HH2(col + 8 * (row-1))); 
        if ~SHOW
            set(gcf, 'Visible', 'off')
        end
        hold on;
        Q = confplot(1:size(spikes, 2), mwave, swave);
        set(Q(1), 'Color', [0 0 0], 'LineWidth', 2)
        T3 = text(55,-5e-6, tmp);
        T4 = text(0,-5e-6, sprintf('[%d]',length(indn)));
        set([T1 T2 T3 T4], 'FontName', 'Arial', 'FontSize', 5)
    end
end


if (MIN~=999 & MAX ~=-999)
set([HH1 HH2], 'XTick', [], 'XLim', [-40 104], 'YLim', [MIN*0.95 MAX*1.05], 'Visible', 'off');

axes(HH1(end));
if ~SHOW
    set(gcf, 'Visible', 'off')
end
L1 = line([0 25], [0 0] + MIN + 10e-6);
L2 = line([0 0], [MIN MIN + 10e-6]);
set([L1 L2], 'Color', [0 0 0], 'LineWidth', 2)
Ta = text(0, MIN + 15e-6, '1 msec');
Tb = text(-10, MIN, '{10 \mu V}'); set(Tb, 'Rotation', 90);

axes(HH2(end));
if ~SHOW
    set(gcf, 'Visible', 'off')
end
L1 = line([0 25], [0 0] + MIN + 10e-6);
L2 = line([0 0], [MIN MIN + 10e-6]);
set([L1 L2], 'Color', [0 0 0], 'LineWidth', 2)
Tc = text(0, MIN + 15e-6, '1 msec');
Td = text(-10, MIN, '{10 \mu V}'); set(Td, 'Rotation', 90);

set([Ta Tb Tc Td], 'FontName', 'Arial')

expname = P(k).name(1:end-20);
tmq = strfind(expname, '_');
expname(tmq)= '-';

axes(HH1(1));
if ~SHOW
    set(gcf, 'Visible', 'off')
end
%T = text(-40, 0, expname);
T = text(-40, 0, 'Negative Spikes');
set(T, 'FontName', 'Arial')

axes(HH2(1));
if ~SHOW
    set(gcf, 'Visible', 'off')
end
%T = text(-40, 0, expname);
T = text(-40, 0, 'Positive Spikes');
set(T, 'FontName', 'Arial')


figure(1);
if ~SHOW
    set(gcf, 'Visible', 'off')
end
set(gcf, 'renderer', 'zbuffer');
print(gcf, '-dpdf', '-loose', sprintf('%s/negative_waves.pdf', mypath))
print(gcf, '-dpng', '-loose',  sprintf('%s/negative_waves.png', mypath))
saveas(gcf, sprintf('%s/negative_waves', mypath), 'fig')

figure(2); 
if ~SHOW
    set(gcf, 'Visible', 'off')
end
set(gcf, 'renderer', 'zbuffer');
print(gcf, '-dpdf', '-loose', sprintf('%s/positive_waves.pdf', mypath))
print(gcf, '-dpng', '-loose',  sprintf('%s/positive_waves.png', mypath))
saveas(gcf, sprintf('%s/positive_waves', mypath), 'fig')
end
