function out = load_bin(fname)
%load_bin It reads the binary data produced by "channel_extract.cpp"
% contained in the file specified by (fullpath) 'filename'
% and it makes it available in MATLAB as the vector output.
% input:
%       fname     : the binary data file *.dat
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 01:35)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 01:35:53 $
%
% output = load_bin(filename)
%
%
 fp = fopen(fname, 'r');		% File is open to be read.
 %tic
 out  = fread(fp, inf, 'double');	% All the file is acquired, type is 'double'.
 %toc
 fclose(fp);				% File is closed.
end
