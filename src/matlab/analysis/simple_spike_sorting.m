function simple_spike_sorting(mypath)
%simple_spike_sorting performs simple spike sorting from the activities
% detected by the electrodes. Based on previously stored *_spikes.mat files
% a global *_spikes.txt file is created with 3 columns with the number of
% spikes rows, where the columns are:
%   first column is the spike time stamp
%   second column is the electrode name
%   third column is the electrode number
% Based on the shape of the spike (positive or negative) a negative sign is
% added infront of the electrode name and number.
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 02:05)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 02:05:17 $
%
% Elementary spike-sorting...
%

global INFO

if (mypath(end) == '/') || (mypath(end) == '\')
    mypath = mypath(1:end-1);
end

if strcmpi(computer,'pcwin')
    fs = '\\';
else
    fs = '/';
end

disp(sprintf('The path provided is: %s',mypath));

fullpath = sprintf(['%s' fs '*_chan_x*_spikes.mat'], mypath);
P        = dir(fullpath);
N        = size(P,1);

infFileName = dir(sprintf(['%s' fs '*_info.txt'],mypath));
fid = fopen(sprintf(['%s' fs infFileName.name],mypath),'r');

ElectrodeMap = zeros(N,2);
while ~feof(fid)
    junk = fgetl(fid);
    if strfind(junk,'Nelec60')
        for i=1:N
            ElectrodeMap(i,1) = str2num(fgetl(fid));
            ElectrodeMap(i,2) = i;
        end
    end
end
fclose(fid);

INFO.ElectrodeMap = ElectrodeMap;

SPIKESp = {};
SPIKESn = {};
CHANn_alt = 0;

% remove the first . present in the path for creating the file
if (mypath(1) == '.') & (mypath(2) == '/')
    updatedPath = mypath(3:end);
else
    sl = strfind(mypath, './');
    if (~isempty(sl))
        updatedPath = strrep(mypath,'./','');
    else
        updatedPath = mypath;
    end
end


mypathForFileName = regexprep(updatedPath,'/','_');

spFileName = sprintf(['%s' fs '%s_spikes.txt'], mypath, mypathForFileName);

[fid, mssg] = fopen(spFileName,'w');
if (fid > 2)
    disp(sprintf('a spike file with name ''%s'' is created',spFileName));
else
    disp(sprintf('there was an error creating the %s file\n aborted with error %s\n\n%s',spFileName, mssg, pwd));
end

disp(sprintf('Preparing to analyze %d spikes files', N));
%hh = waitbar(0,'Please wait...');
for k=1:N,                  % For each of them..
    %   waitbar(k / N)
    myerror = 0;
    try
        load(sprintf(['%s' fs '%s'],mypath,P(k).name));
        disp(sprintf(['%s' fs '%s loaded!'],mypath,P(k).name));
    catch err
        myerror = 1;
        disp('simple_spike_sorting() try-catch error!');
    end
    if (myerror==0)
        CHANn = str2num(P(k).name(end-12:end-11));
%         CHANn_alt = CHANn_alt + 1;
        CHANn_alt = ElectrodeMap(ElectrodeMap(:,1)==CHANn,2);        
        
        SPIKES{k} = [];
        
        indp = []; indn = [];
%         disp(sprintf('Found %d spikes in chan %d!', size(spikes,1), k));
        disp(sprintf('Found %d spikes in chan %d!', size(spikes,1), CHANn_alt));
        for m=1:size(spikes,1),
            tmq = spikes(m,:) - mean(spikes(m,50:64));
            if (tmq(20) > 0.),
                indp = [indp, m];
            else
                indn = [indn, m];
            end
        end
        
        if (~isempty(indp))
            SPIKESp{k}(:,1) = CHANn * ones(size(indp));
            SPIKESp{k}(:,2) = index(indp);
            SPIKESp{k}(:,3) = CHANn_alt * ones(size(indp));
            for h=1:size(SPIKESp{k},1)
                fprintf(fid, '%f %d %d\n', SPIKESp{k}(h,2), SPIKESp{k}(h,1), SPIKESp{k}(h,3));
            end
        end
        
        if (~isempty(indn))
            SPIKESn{k}(:,1) = CHANn * ones(size(indn));
            SPIKESn{k}(:,2) = index(indn);
            SPIKESn{k}(:,3) = CHANn_alt * ones(size(indn));
            for h=1:size(SPIKESn{k},1)
                fprintf(fid, '%f -%d -%d\n', SPIKESn{k}(h,2), SPIKESn{k}(h,1), SPIKESn{k}(h,3));
            end
        end
        
    end
end
%close(hh)
fclose(fid);

end
