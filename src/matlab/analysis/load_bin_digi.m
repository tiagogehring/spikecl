function out = load_bin_digi(fname)
%load_bin_digi is to be used for the 'digi' data stream!
% It reads the binary data produced by "channel_extract.cpp"
% contained in the file specified by (fullpath) 'filename'
% and it makes it available in MATLAB as the vector output.
% input:
%       fname     : the binary data file *.dat for the digital data
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 01:37)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 01:37:13 $
%
% output = load_bin(filename)
%
%
% output = load_bin_digi(filename)
%
% To be used for the 'digi' data stream!
% It reads the binary data produced by "channel_extract.cpp"
% contained in the file specified by (fullpath) 'filename'
% and it makes it available in MATLAB as the vector output.
%
%
 fp = fopen(fname, 'r');                % File is open to be read.
 %tic
 out  = fread(fp, inf, 'uint16');       % All the file is acquired, type is 'digital'.
 %toc
 fclose(fp);                            % File is closed.
end
