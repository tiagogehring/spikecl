function frequency_distribution(A)
%frequency_distribution plots a frequency distribution histogram based on spikes.mat
%
%	input A: nx3 matrix
%			 first column is the spike time stamp
%			 second column is the electrode name
% 			 third column is the electrode number
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 00:08)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 00:08:09 $
%  

global INFO FIGS OUTPATH SHOW;

% plot a frequency distribution histogram based on spikes.mat

% ---------------------------------------------------------- %
%                                                            %
% Luca Gambazzi, september 2009                              %
%                                                            %
% scripts for analysis and MCS'mcds files data extraction    %
% based on Michele Giugliano visions and ideas :)            %
%                                                            %
% Contact: luca.gambazzi@epfl.ch                             %
% Contact: michele.giugliano@ua.ac.be                        %
%                                                            %
% ---------------------------------------------------------- %

% Settings ----------------------------------------------------------------

TRUNCATE_AFTER = 30*60*1000; % [ms] truncate spikes, 0 to not truncate

SET_X_AXIS_LOG   = 1;        % set 1 to plot w/ log-x

SET_X_AXIS_STEPS = 20;       % histogram intervals on x axis
LOWER_DISTANCE   = 0.1;      % [ms] lower considered interval 
UPPER_DISTANCE   = 5000;     % [ms] upper considered interval 

NORMALIZE_COUNT  = 1;        % set 1 for yes
Y_LIMIT          = 1;
LABELS_EVERY_COL = 2;

% do not edit below this line ---------------------------------------------

%load(sprintf('%s/spikes.mat',foldername)); 
% Brief data conversion, into a 2-column style of the older analysis.
spikes              = A;
spikes(:,3)         = abs(spikes(:,3));
[spikes, tmpspikeI] = sortrows(spikes, 1);
spikes              = [spikes(:,1) , spikes(:,3)];

if TRUNCATE_AFTER,
    spikes = spikes(spikes(:,1)<=TRUNCATE_AFTER,:);
end


% we discard the channel column
spikes = spikes(:,1);

% sort and calculate the distances between events (in ms)
dist = diff(sort(spikes));

maximal_distance = max(dist);
% round at uppper 10th 
maximal_distance = round(maximal_distance/10)*10;


FIGS = [FIGS, figure]; clf; cla;
if ~SHOW
    set(gcf, 'Visible', 'off')
end
set(gcf, 'renderer', 'zbuffer');
set(gcf, 'Color', [1 1 1]);


title(sprintf('Interspike interval distribution [%2.2fms ; %2.2fms]',LOWER_DISTANCE,UPPER_DISTANCE));

if SET_X_AXIS_LOG,
    e_scale = log10(LOWER_DISTANCE):(log10(UPPER_DISTANCE)-log10(LOWER_DISTANCE))/SET_X_AXIS_STEPS:log10(UPPER_DISTANCE);
    x_scale = zeros(1,length(e_scale));
    for i=1:length(e_scale),
        x_scale(i) = 10^e_scale(i);
    end
    %set(gca, 'XScale', 'log');
else 
    x_scale = LOWER_DISTANCE:(UPPER_DISTANCE-LOWER_DISTANCE)/SET_X_AXIS_STEPS:UPPER_DISTANCE;
end    

y_value = zeros(1,length(x_scale)-1);
for i=1:length(x_scale)-1,
    y_value(i) = length ( find( dist(:) >= x_scale(i) & dist(:) < x_scale(i+1) ));
end


if NORMALIZE_COUNT,
    y_value = y_value./sum(y_value);
    ylabel('Normalized interspikes number');
    ylim([0 Y_LIMIT]);
else
    ylabel('Interspikes number');
end

hold on;
for i=1:length(x_scale)-1,
    %plot(x_scale(i),y_value(i),'rs')
    bar(i,y_value(i),'FaceColor',[0 0 0]);
end
hold off;

xlabel('Interval [ms]');

labels = strread(sprintf('%2.2f\n',x_scale), '%s');
if LABELS_EVERY_COL,
    c=1;
    for i=1:length(labels),
        if mod(i,LABELS_EVERY_COL) == 0,
            c=c+1;
            label(c,1) = labels(i);
        end
    end
    set(gca, 'XTickLabel', label(:,1), 'XTick', 0:LABELS_EVERY_COL:length(x_scale)-1 );
end

%mTextBox = uicontrol('style','text');
%set(mTextBox,'BackgroundColor',[1 1 1]);
%set(mTextBox,'FontSize',24);
%set(mTextBox,'String','');
%%set(mTextBox,'String',sprintf('DIV %s',foldername(28:29)));
%set(mTextBox,'Position',[340 320 100 50]);


%print(gcf, '-loose', '-depsc', sprintf('%s/f5_frequency_distribution.eps',foldername));

print(gcf, '-dpng', '-loose',  sprintf('%s/frequency_distribution.png', OUTPATH))
saveas(gcf, sprintf('%s/frequency_distribution', OUTPATH), 'fig')
print(gcf, '-dpdf', '-loose',  sprintf('%s/frequency_distribution.pdf', OUTPATH))


end

 
