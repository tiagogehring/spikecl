%
% locate_spikes_quiroga.m detects individual spike times.
%
% Script derived from the Quiroga's package Wave_clus.
%
% It assumes that xf_detect (i.e., n x 1) is the band-pass filtered data vector
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 01:39)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 01:39:27 $
%  
%
% locate_spikes_quiroga.m
%
% Script derived from the Quiroga's package, to perform detection of individual spike times
%
% It assumes that xf_detect (i.e., n x 1) is the band-pass filtered data vector
%

index = [];			% output data structure, containing spike times

switch detect			% set within parameters.txt, can be 'pos', 'neg', or 'both'
    case 'pos'			% Detect only positive threshold crossings...
        nspk = 0;
        xaux = find(xf_detect(w_pre+2:end-w_post-2) > thr) +w_pre+1;
        xaux0 = 0;
        for i=1:length(xaux)
            if xaux(i) >= xaux0 + ref
                [maxi iaux]=max((xf(xaux(i):xaux(i)+floor(ref/2)-1)));    %introduces alignment
                nspk = nspk + 1;
                index(nspk) = iaux + xaux(i) -1;
                xaux0 = index(nspk);
            end
        end

    case 'neg'			% Detect only negative threshold crossings...
        nspk = 0;
        xaux = find(xf_detect(w_pre+2:end-w_post-2) < -thr) +w_pre+1;
        xaux0 = 0;
        for i=1:length(xaux)
            if xaux(i) >= xaux0 + ref
                [maxi iaux]=min((xf(xaux(i):xaux(i)+floor(ref/2)-1)));    %introduces alignment
                nspk = nspk + 1;
                index(nspk) = iaux + xaux(i) -1;
                xaux0 = index(nspk);
            end
        end

    case 'both'			% Detect both positive and negative threshold crossings...
        nspk = 0;
        xaux = find(abs(xf_detect(w_pre+2:end-w_post-2)) > thr) +w_pre+1;
        xaux0 = 0;
        for i=1:length(xaux)
            if xaux(i) >= xaux0 + ref
                [maxi iaux]=max(abs(xf(xaux(i):xaux(i)+floor(ref/2)-1)));    %introduces alignment
                nspk = nspk + 1;
                index(nspk) = iaux + xaux(i) -1;
                xaux0 = index(nspk);
            end
        end
end
