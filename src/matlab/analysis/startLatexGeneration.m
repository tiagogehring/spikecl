function startLatexGeneration(mypath, analdir)
%startLatexGeneration calls the generateLatexFile to generate a latex file
% for automated report generation.
% This function accepts the following two inputs:
%       mypath    : the directory containing all the required files
%       analdir   : the output directory
%                   /data1/WWWINTERFACE/OUTPUT_PREPROCESSED_FILES
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 10:24)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 10:24:42 $
%  
load([analdir filesep mypath filesep 'GLOBAL_INFO.mat'])

generateLatexFile(mypath, INFO.BURSTS.largest_burst_bin, INFO.BURSTS.largest_burst_plot_zoomed_bin);

