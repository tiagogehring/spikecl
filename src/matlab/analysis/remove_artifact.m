function C = remove_artifact(A)
%remove_artifact removes artifacts from the detected spike time stamps. An
% artifact is defined as very high number of spikes (50) within a single
% bin of 3 ms.
% inputs: 
%       A         : nx3 matrix
%                   first column is the spike time stamp
%                   second column is the electrode name
%                   third column is the electrode number
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 01:56)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 01:56:49 $
% 
global FIGS INFO OUTPATH;

T     = INFO.T;
D     = 3.;
edges = 0:D:T;
psth  = histc(A(:,1), edges);

ARTIFACTS = edges(find(psth>50));

B = A;
n = 0;
for k=1:length(ARTIFACTS),
 indx = find(abs(A(:,1)-ARTIFACTS(k)-0.5*D)<(0.5*D));
 B(indx, 1) = nan;
 n = n + length(indx); 
end

C = zeros(size(B,1)-n,3);
m = 1;
for k=1:size(B,1),
 if ~isnan(B(k,1)),
   C(m,:) = B(k,:);
   m = m + 1;
 end
end




end
