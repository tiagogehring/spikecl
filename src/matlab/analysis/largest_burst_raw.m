function largest_burst_raw(A)
%largest_burst_raw calculates the largest population burst present at the
% recording file. It also plots the raw trace of the burst centering the 
% detected largest burst for 10 seconds and 1 second.
% 
% inputs: 
%       A         : nx3 matrix
%                   first column is the spike time stamp
%                   second column is the electrode name
%                   third column is the electrode number
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 01:19)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 01:19:53 $
% 
global FIGS INFO OUTPATH SHOW;
%--------------------------------------------
% Sample raster and PSTH for 10s and 1s, centered
% around the largest burst
%--------------------------------------------

T     = INFO.T;
D     = 3.;
edges = 0:D:T;
psth  = histc(A(:,1), edges);
%B = bar(edges, psth, 'histc');

M     = find(psth==max(psth));
M=M(1);
while (max(psth)>50),
 psth(M) = 0;
 M = find(psth==max(psth));
 M = M(1);
end

electrodes = unique(abs(A(:,3))); 
Nspikes    = zeros(length(electrodes),1);
for h=1:length(electrodes), Nspikes(h) = length(find(A(:,3)==electrodes(h))); end
[tmpnil index] = sort(Nspikes, 1, 'descend');

INFO.t1a    = edges(M) - 5000.;
INFO.t1b    = edges(M) + 5000.;
INFO.t2a    = edges(M) - 500;
INFO.t2b    = edges(M) + 500;

%--------

ii1a = max([INFO.t1a * INFO.sr * 0.001; 1]);
ii1b = min([INFO.t1b * INFO.sr * 0.001; INFO.sr * T-1]);
ii2a = max([INFO.t2a * INFO.sr * 0.001; 1]);
ii2b = min([INFO.t2b * INFO.sr * 0.001; INFO.sr * T - 1]);

P = dir(sprintf('%s/*_chan_x*.dat', OUTPATH));
N = length(P);
% TG: added support for spikes in the CSV format
if N == 0    
    P        = dir(sprintf('%s/*_chan_x*.csv', OUTPATH));
    N        = size(P,1);    
end
% </TG>

FIGS = [FIGS, figure]; clf; cla;
if ~SHOW
    set(gcf, 'Visible', 'off')
end
set(gcf, 'renderer', 'zbuffer');
set(gcf, 'Color', [1 1 1]);
set(gca, 'FontName', 'Arial', 'FontSize', 15, 'Box', 'on');
hold on;

time = (ii1a:ii1b)/INFO.sr;
for h=1:min([10 N]),
 kk = electrodes(index(h));
 tmp = load_bin(sprintf('%s/%s',OUTPATH,P(kk).name))*1000000;
 [b,a]     = ellip(2,0.1,40,[400 3000]*2./INFO.sr);
 tmp = filtfilt(b,a,tmp);

 K = 2.5e-5 * 1000000;
 tmp = tmp(ii1a:ii1b);
 plot(time, 2*K*(h-1) + tmp, 'k'); 
 drawnow
end
hold off;

ylim([-2*K 2*K*10]);
xlim([INFO.t1a INFO.t1b]/1000.);
xlabel('time [s]');
ylabel('Raw voltages [uV]');
title('Largest population burst (raster).');
print(gcf, '-dpng', '-loose',  sprintf('%s/largest_burst_raw.png', OUTPATH))
saveas(gcf, sprintf('%s/largest_burst_raw', OUTPATH), 'fig')
print(gcf, '-dpdf', '-loose',  sprintf('%s/largest_burst_raw.pdf', OUTPATH))


xlim([INFO.t2a INFO.t2b]/1000.);
print(gcf, '-dpng', '-loose',  sprintf('%s/largest_burst_rawzoom.png', OUTPATH))
saveas(gcf, sprintf('%s/largest_burst_rawzoom', OUTPATH), 'fig')
print(gcf, '-dpdf', '-loose',  sprintf('%s/largest_burst_rawzoom.pdf', OUTPATH))

end
