function processchanMG(cwd, channel, type, fname, ofname, Nsamples, sr)
%processchanMG performs the basic preprocessing of binary data file
% extracted from the .mcd file which is acausal filtering of the data,
% locating spikes in the datastream, and storing the individual spikes for
% each channel. It performs the preprocessing for one channel at a time and
% stores all detected spikes in a chan_x*_spikes.mat file which is lateron
% used for further processing and analyses.
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 01:50)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 01:50:43 $
% 
% processchanMG(cwd, channel, type, fname, ofname, Nsamples, sr)
%
% e.g.
% channel=1;
% type='elec';
% fname='/data1/Analysis/13772_DIV20_Stim800mV1Hz20x.mcd';
% ofname='/data1/Analysis/13772_DIV20_Stim800mV1Hz20x_chan_x57.dat';
% Nsamples=15000;
% sr=25000;
%
% Part of QSpikeTool (called within the shell script chan_elabMG)
%
% April 2013, Michele Giugliano
%
fname
channel
delete_dat_files = 0;				% if set to 1, removes automatically the binary files

%-----------------------------------------------------------------------------------------
% Lets acquire all the filtering and peak-detection settings, contained in the
% ASCII text file 'parameters.txt'.
%
% An example of such a file is given below
%
% 400   ---> cut-off freq (Hz) for high-pass filter, used in spike-detection
% 3000  ---> cut-off freq (Hz) for low-pass filter, used in spike-detection
% 400	---> cut-off freq (Hz) for the high-pass filter, used in spike-sorting
% 3000  ---> cut-off freq (Hz) for the low-pass filter, used in spike-sorting
% 8     ---> minimum threshold for event detection
% 10000 ---> maximum threshold for event detection
% both	---> type of detection threshold ('neg', 'pos', 'both')
% 20	---> for each event, store 20 samples before the crossing
% 44	---> for each event, store 44 sample following the crossing
% 2.5	---> spike detector dead (or refractory) time in msec
% 2		---> interpolation factor
% y		---> yes, perform the interpolation with cubin splines
% y		---> yes, save the filtered data
% y		---> yes, filter the data!

fp = fopen(sprintf('%s/parameters.txt',cwd), 'r');	% The file is open to be read.

detect_fmin = str2num(fgetl(fp));     % high pass filter for detection
detect_fmax = str2num(fgetl(fp));     % low pass filter for detection (default 1000)
sort_fmin   = str2num(fgetl(fp));     % high pass filter for sorting 
sort_fmax   = str2num(fgetl(fp));     % low pass filter for sorting (default 3000)
stdmin      = str2num(fgetl(fp));     % minimum threshold for detection
stdmax      = str2num(fgetl(fp));     % maximum threshold for detection
detect      = fgetl(fp);              % type of threshold ('neg', 'pos', 'both')
w_pre       = str2num(fgetl(fp));     % number of pre-event data points stored (default 20)
w_post      = str2num(fgetl(fp));     % number of post-event data points stored (default 44))
ref         = str2num(fgetl(fp));     % detector dead time (in ms)
int_factor  = str2num(fgetl(fp));     % interpolation factor
interpolation = fgetl(fp);            % interpolation with cubic splines ('n','y')
savingxf    = fgetl(fp);              % saving xf files? ('n','y')
filterxf    = fgetl(fp);              % filter the data or not at all? ('n','y')

fclose(fp);							  % The file is now closed.
%-----------------------------------------------------------------------------------------


ref = ref * sr * 1e-3;				 % spike detector dead time in #samples (see above)

%-----------------------------------------------------------------------------------------
if type=='elec'
  %disp('processchanMG: Importing channel into Matlab...');
  x = load_bin(ofname);		% Loads the *.dat file (i.e., binary channel waveform)
  x = x(:);					% Makes it by default a column vector (i.e., n x 1)  
  %disp('done!');

  %disp('processchanMG: Filtering the data...');
  filter_quiroga;			% Band-pass filtering + evaluation of detection threshold
  %disp('done!');
  
  %time = (1:Nsamples)/sr;
  %plot(time, xf, time, thr * ones(1, Nsamples), 'g', time,  -thr * ones(1, Nsamples), 'g', time, thrmax * ones(1, Nsamples), 'r', time,  -thrmax * ones(1, Nsamples), 'r');

  %disp('processchanMG: Locating spikes...');
  locate_spikes_quiroga;  
  %disp('done!');
  %disp(sprintf('%d spikes located!', nspk));
  
  %mytspikes = time(index);
  %figure(1); set(gcf, 'Color', [1 1 1]);
  %hold on; plot(mytspikes, channel*ones(size(mytspikes)), '*'); drawnow;
  
  %disp('processchanMG: Locating spikes and aligning...');
  spike_storing;
  %disp('done!');
  
  
  %---> [spikes,thr,index];
  index = index *1e3/sr; 		% Let's convert the spike times in msec (from # sample)
  
  %disp('processchanMG: Saving spikes waveforms on disk...');
  file_to_cluster = sprintf('%s_spikes.mat', ofname(1:end-4));
  %eval(['save ' char(file_to_cluster) '_spikes.mat spikes index']);    %saves Sc files
  save(file_to_cluster, 'spikes', 'index', 'noise_std_detect','noise_std_sorted','thr','thrmax');
  %disp('done!');

  if savingxf~='n'
      %disp('processchanMG: Saving filtered data on disk...');
      xffname = sprintf('%s_xf.mat', ofname(1:end-4));
      save(xffname,'xf');
      %disp('done!');
  end
  
  clear xf;					% Clear the filtered data from the memory.
  clear xf_detect;			% Clear the filtered data from the memory.
  
  if delete_dat_files		% Do we delete the *.dat files?
    %disp('processchanMG: Deleting the raw data from the disk...');
  	delete(ofname); 		% deletes the .dat file
    %disp('done!');
  end

  
  %digits=round(stdmin * 100);
  %file_to_cluster = sprintf('%s_sp_th.mat%s', ofname(1:end-4),num2str(digits));  
  %nfile=[char(file_to_cluster) '_sp_th.mat' num2str(digits)];
  %eval(['save ' nfile ' spikes index']);    %save files for analysis
  %save(file_to_cluster, 'spikes', 'index');

  
  %figure(2); clf; set(gcf, 'Color', [1 1 1]);
  %for h=1:nspk,
  % hold on; 
  %  if (spikes(h,20)<0) plot(spikes(h,:)); drawnow; end
  %end
  %hold off;  
end
%-----------------------------------------------------------------------------------------
%-----------------------------------------------------------------------------------------
%-----------------------------------------------------------------------------------------

%-----------------------------------------------------------------------------------------

if type=='digi'
    %processing signals from digital

      %disp('processchanMG: Importing digital channel into Matlab...');
      x = load_bin_digi(ofname);	% Loads the *.dat file (i.e., binary digital waveform)
      x = x(:);					    % Makes it by default a column vector (i.e., n x 1)  
      %disp('done!');

      % indexes=find(x==1);
      % indexes_diff=diff(indexes);
      % stimuli=indexes([1 find(indexes_diff>1)'+1]);

      % stimuli= stimuli *1e3/sr; % spike times in ms

      % stimuli_end=indexes([find(indexes_diff>1)' end]);

      % stimuli_end=stimuli_end*1e3/sr; % spike times in ms

      %file_to_cluster = sprintf('%s_stimuli.mat', ofname(1:end-4));
      %%eval(['save ' char(file_to_cluster) '_spikes.mat spikes index']);    %saves Sc files
      %save(file_to_cluster, 'stimuli', 'stimuli_end');
      file_to_cluster = sprintf('%s_stimuli.mat', ofname(1:end-4));
      %%eval(['save ' char(file_to_cluster) '_spikes.mat spikes index']);    %saves Sc files
	  stim_value = sparse(x);
	  %stim_time  = (1:Nsamples)/sr;
      save(file_to_cluster, 'sr', 'stim_value');

	  clear x;
	  clear stim_value;
	  clear stim_time;
	  
      if delete_dat_files		% Do we delete the *.dat files?
       %disp('processchanMG: Deleting the raw data from the disk...');
  	   delete(ofname); 		% deletes the .dat file
       %disp('done!');
      end

end

%clear all;

end
