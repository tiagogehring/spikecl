function sample_raster_and_psth(A)
%sample_raster_and_psth plots sample raster and psth of the first 300 s. 
% inputs: 
%       A         : nx3 matrix
%                   first column is the spike time stamp
%                   second column is the electrode name
%                   third column is the electrode number
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 01:58)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 01:58:02 $
%
global FIGS INFO OUTPATH SHOW;

T = INFO.T;
%--------------------------------------------
% Sample raster and PSTH for (the first) 300s
%--------------------------------------------

D     = 100.;
edges = 0:D:T;
psth  = histc(A(:,1), edges);

FIGS = [FIGS, figure]; clf; cla;
if ~SHOW
    set(gcf, 'Visible', 'off')
end
set(gcf, 'renderer', 'zbuffer');
set(gcf, 'Color', [1 1 1]);

indx = find( (A(:,1)>=(0)) & (A(:,1)<=(300000.)) );
spks = A(indx,:);

h1= subplot(2,1,1);
for h=1:length(indx),
    L = line([1 1]*spks(h,1)/1000., [0 1]+abs(spks(h,2)));
    set(L, 'Color', [0 0 0], 'LineWidth', 1);
end
ylim([5 90]);
xlabel('time [s]');
ylabel('Electrode name');
XLIM = get(gca, 'XLim');
title('Sample spontaneous activity [first 300 sec]');


h2 = subplot(2,1,2);
B = bar(edges/1000., psth, 'histc');
set(gca, 'XLim', XLIM)
xlabel('time [s]');
ylabel('PSTH');
set(B, 'FaceColor', [0 0 0], 'EdgeColor', [0 0 0]);

POS = get(h1, 'Position');
set(h1, 'Position', [POS(1) POS(2)-0.2 POS(3) POS(4)+0.2]);
POS = get(h2, 'Position');
set(h2, 'Position', [POS(1) POS(2) POS(3) POS(4)-0.1]);
set([h1 h2], 'FontName', 'Arial', 'FontSize', 15, 'Box', 'on');
set(h1, 'XTickLabel', '');
print(gcf, '-dpng', '-loose',  sprintf('%s/sample_raster_psth.png', OUTPATH))
saveas(gcf, sprintf('%s/sample_raster_psth', OUTPATH), 'fig')
print(gcf, '-dpdf', '-loose',  sprintf('%s/sample_raster_psth.pdf', OUTPATH))


end


