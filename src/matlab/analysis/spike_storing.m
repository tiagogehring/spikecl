%
% spike_storing.m stores the detected waveforms from each electrode in a 
% raw format with a predefined waveform size specified in the parameters 
% file.
%
% It assumes that xf_detect (i.e., n x 1) is the band-pass filtered data vector (for event detection)
% It also assumes that xf (i.e., n x 1) is the band-pass filtered data vector (for spike sorting)
% index[] also contains the nspk detected events.
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Tuesday Dec 3rd, 2013 - 12:36)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 12:36:27 $
%  
%
% spike_storing.m
%
% Script derived from the Quiroga's package, to perform storing of detected event waveforms
%
% It assumes that xf_detect (i.e., n x 1) is the band-pass filtered data vector (for event detection)
% It also assumes that xf (i.e., n x 1) is the band-pass filtered data vector (for spike sorting)
% index[] also contains the nspk detected events.
%

ls     = w_pre + w_post;			% Size of the waveform sample to store
spikes = zeros(nspk,ls+4);			% Memory pre-allocation
xf     = [xf ; zeros(w_post,1)];                % Enlarges 'xf' to avoid final trimming problems; it was xf=[xf zeros(1,w_post)];

for i=1:nspk                                    % For each event detected, store the waveform, unless is too large (i.e., it's an artifact)
    if max(abs( xf(index(i)-w_pre:index(i)+w_post) )) < thrmax               
        spikes(i,:)=xf(index(i)-w_pre-1:index(i)+w_post+2);
    else
    disp('Artifact removed!!!');
    end
end
aux           = find(spikes(:,w_pre)==0);       %erases indexes that were artifacts
spikes(aux,:) = [];
index(aux)    = [];
       
 
nspk          = size(spikes,1);
s             = 1:size(spikes,2);
ints          = 1/int_factor:1/int_factor:size(spikes,2);
intspikes     = zeros(1,length(ints));
spikes1       = zeros(nspk,ls);

switch interpolation
    case 'n'
        spikes(:,end-1:end)=[];       %eliminates borders that were introduced for interpolation 
        spikes(:,1:2)=[];
    case 'y'
        %Does interpolation
%-----------------------------------------------------------------------------
        switch detect
    case 'pos'
        for i=1:nspk
            intspikes(:) = spline(s,spikes(i,:),ints);
            [maxi iaux]=max((intspikes(w_pre*int_factor:w_pre*int_factor+8))); 
            iaux = iaux + w_pre*int_factor -1;
            spikes1(i,w_pre:-1:1) = intspikes(iaux:-int_factor:iaux-w_pre*int_factor+int_factor);
            spikes1(i,w_pre+1:ls) = intspikes(iaux+int_factor:int_factor:iaux+w_post*int_factor);

        end
    case 'neg'
        for i=1:nspk
            intspikes(:) = spline(s,spikes(i,:),ints);
            [maxi iaux]=min((intspikes(w_pre*int_factor:w_pre*int_factor+8))); 
            iaux = iaux + w_pre*int_factor -1;
            spikes1(i,w_pre:-1:1) = intspikes(iaux:-int_factor:iaux-w_pre*int_factor+int_factor);
            spikes1(i,w_pre+1:ls) = intspikes(iaux+int_factor:int_factor:iaux+w_post*int_factor);
        end
    case 'both'
        for i=1:nspk
            intspikes(:) = spline(s,spikes(i,:),ints);
            [maxi iaux]=max(abs(intspikes(w_pre*int_factor:w_pre*int_factor+8))); 
            iaux = iaux + w_pre*int_factor -1;
            spikes1(i,w_pre:-1:1) = intspikes(iaux:-int_factor:iaux-w_pre*int_factor+int_factor);
            spikes1(i,w_pre+1:ls) = intspikes(iaux+int_factor:int_factor:iaux+w_post*int_factor);
        end
        spikes = spikes1;
        clear spikes1;
end
%-----------------------------------------------------------------------------
        
end

