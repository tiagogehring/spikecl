function D = largest_burst(A)
%largest_burst calculates the largest population burst present at the
% recording file. It also plots sample rasters and PSTHs centering the 
% detected largest burst for 10 seconds and 1 second.
% 
% inputs: 
%       A         : nx3 matrix
%                   first column is the spike time stamp
%                   second column is the electrode name
%                   third column is the electrode number
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 01:17)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 01:17:43 $
% 


global FIGS INFO OUTPATH SHOW;
%--------------------------------------------
% Sample raster and PSTH for 10s and 1s, centered
% around the largest burst
%--------------------------------------------

try

T     = INFO.T;
D     = 3.;
INFO.BURSTS.largest_burst_bin = D;
edges = 0:D:T;
psth  = histc(A(:,1), edges);
%B = bar(edges, psth, 'histc');

M     = find(psth==max(psth));
M = M(1);

while (max(psth)>50),
 psth(M) = 0;
 M = find(psth==max(psth));
 M = M(1);
end

INFO.t1a    = edges(M) - 5000.;
INFO.t1b    = edges(M) + 5000.;
INFO.t2a    = edges(M) - 500;
INFO.t2b    = edges(M) + 500;

%--------

D     = 30.;
INFO.BURSTS.largest_burst_plot_bin = D;
edges = 0:D:T;
psth  = histc(A(:,1), edges);

FIGS = [FIGS, figure]; clf; cla;
if ~SHOW
    set(gcf, 'Visible', 'off')
end
set(gcf, 'renderer', 'zbuffer');
set(gcf, 'Color', [1 1 1]);
set(gca, 'FontName', 'Arial', 'FontSize', 15, 'Box', 'on');

indx = find( (A(:,1)>=(INFO.t1a)) & (A(:,1)<=(INFO.t1b)) );
spks = A(indx,:);

for h=1:length(indx),
    L = line([1 1]*spks(h,1)/1000., [0 1]+abs(spks(h,2)));
    set(L, 'Color', [0 0 0], 'LineWidth', 1);
end
ylim([5 90]);
xlabel('time [s]');
ylabel('Electrode name');
XLIM = get(gca, 'XLim');
title('Largest population burst (raster).');
print(gcf, '-dpng', '-loose',  sprintf('%s/largest_burst_raster.png', OUTPATH))
saveas(gcf, sprintf('%s/largest_burst_raster', OUTPATH), 'fig')
print(gcf, '-dpdf', '-loose',  sprintf('%s/largest_burst_raster.pdf', OUTPATH))



FIGS = [FIGS, figure]; clf; cla;
if ~SHOW
    set(gcf, 'Visible', 'off')
end
set(gcf, 'renderer', 'zbuffer');
set(gcf, 'Color', [1 1 1]);
set(gca, 'FontName', 'Arial', 'FontSize', 15, 'Box', 'on');
B = bar(edges/1000., psth, 'histc');
xlim([INFO.t1a INFO.t1b]);
xlabel('time [s]');
ylabel('PSTH');
set(B, 'FaceColor', [0 0 0], 'EdgeColor', [0 0 0]);
set(gca, 'XLim', XLIM)
title('Largest population burst (PSTH).');
print(gcf, '-dpng', '-loose',  sprintf('%s/largest_burst_psth.png', OUTPATH))
saveas(gcf, sprintf('%s/largest_burst_psth', OUTPATH), 'fig')
print(gcf, '-dpdf', '-loose',  sprintf('%s/largest_burst_psth.pdf', OUTPATH))


%--------

D     = 10.;
INFO.BURSTS.largest_burst_plot_zoomed_bin = D;
edges = 0:D:T;
psth  = histc(A(:,1), edges);

FIGS = [FIGS, figure]; clf; cla;
if ~SHOW
    set(gcf, 'Visible', 'off')
end
set(gcf, 'renderer', 'zbuffer');
set(gcf, 'Color', [1 1 1]);
set(gca, 'FontName', 'Arial', 'FontSize', 15, 'Box', 'on');

indx = find( (A(:,1)>=(INFO.t2a)) & (A(:,1)<=(INFO.t2b)) );
spks = A(indx,:);

for h=1:length(indx),
    L = line([1 1]*spks(h,1)/1000., [0 1]+abs(spks(h,2)));
    set(L, 'Color', [0 0 0], 'LineWidth', 1);
end
ylim([5 90]);
xlabel('time [s]');
ylabel('Electrode name');
XLIM = get(gca, 'XLim');
title('Largest population burst (zoom, raster).');
print(gcf, '-dpng', '-loose',  sprintf('%s/largest_burst_raster_zoom.png', OUTPATH))
saveas(gcf, sprintf('%s/largest_burst_raster_zoom', OUTPATH), 'fig')
print(gcf, '-dpdf', '-loose',  sprintf('%s/largest_burst_raster_zoom.pdf', OUTPATH))


FIGS = [FIGS, figure]; clf; cla;
if ~SHOW
    set(gcf, 'Visible', 'off')
end
set(gcf, 'renderer', 'zbuffer');
set(gcf, 'Color', [1 1 1]);
set(gca, 'FontName', 'Arial', 'FontSize', 15, 'Box', 'on');
B = bar(edges/1000., psth, 'histc');
set(gca, 'XLim', XLIM)
xlabel('time [s]');
ylabel('PSTH');
set(B, 'FaceColor', [0 0 0], 'EdgeColor', [0 0 0]);
title('Largest population burst (zoom, PSTH).');
print(gcf, '-dpng', '-loose',  sprintf('%s/largest_burst_psth_zoom.png', OUTPATH))
%saveas(gcf, sprintf('%s/largest_burst_psth_zoom', OUTPATH), 'png')
saveas(gcf, sprintf('%s/largest_burst_psth_zoom', OUTPATH), 'fig')
print(gcf, '-dpdf', '-loose',  sprintf('%s/largest_burst_psth_zoom.pdf', OUTPATH))

catch exception1 
  errorString = sprintf(['something went wrong during loading %s \n %s \n at %s \n in line %s \n %s'], fname, exception1.identifier, exception1.stack(1,1).name, num2str(exception1.stack(1,1).line), exception1.message);
  disp(sprintf('encountered error during calculating and saving largest_burst: \n %s',errorString));
end



end
