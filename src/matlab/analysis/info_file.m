function out = info_file(fname_spikes)
%info_file loads the informations contained in the file name of the "info" file
% corresponding to the spike file fname..., unless that file does not exist. 
%
% inputs: 
%       fname_spikes : the _spikes.txt file. usually this file is located 
%                      at the /data1/WWWINTERFACE/OUTPUT_PREPROCESSED_FILES
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 01:11)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 01:11:05 $
% 
% loads the informations contained in the file name of the "info" file
% corresponding to the spike file fname..., unless that file does not exist.
%
global FIGS INFO;

k     = strfind(fname_spikes, '_spikes.txt');               % find the substring
fname = sprintf('%s_info.txt',fname_spikes(1:k-1));     % shape the filename

if (exist(fname, 'file'))
    out     = 0;
    fp      = fopen(fname, 'r');
    INFO.T  = str2num(fgetl(fp));    % recording duration [msec]
    INFO.Ns = str2num(fgetl(fp));    % duration in number of samples
    INFO.sr = str2num(fgetl(fp));    % sampling rate [Hz]
    fclose(fp);
else
    out     = -1;
    P       = load(fname_spikes);
    INFO.T  = max(P(:,1));          % some informations are deduced
    INFO.sr = 25000;                % other assumed
    INFO.Ns = 25000 * INFO.T;       % and derived.
end % if
end % function info_filename()
%--------------------------------------------------------------------------

