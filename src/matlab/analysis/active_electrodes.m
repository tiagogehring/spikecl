function active_electrodes(A)
%active_electrodes calculates the number of electrodes with an average
% 	rate of >0.02 Hz, throughout the recording session.
%
%	input A: nx3 matrix
%			 first column is the spike time stamp
%			 second column is the electrode name
% 			 third column is the electrode number
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 2nd, 2013 - 15:17)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/02 15:17:08 $
%  

global FIGS INFO OUTPATH SHOW;
%--------------------------------------------
% Calculate no. active electrodes i.e., those with an average
% rate of >0.02 Hz, throughout the recording session.
%--------------------------------------------

electrodes = unique(abs(A(:,2))); % using e.g., the decorated names
M          = length(electrodes);  % number of electrodes available
Nspikes    = zeros(M,1);          % array to contain the #events / chan

for h=1:M, Nspikes(h) = length(find(abs(A(:,2))==electrodes(h))); end


%--------------------------------------------
D = max(Nspikes)/20.;
edges = [1:D:max(Nspikes)];

FIGS = [FIGS, figure]; clf; cla;
if ~SHOW
    set(gcf, 'Visible', 'off')
end
set(gcf, 'renderer', 'zbuffer');
set(gcf, 'Color', [1 1 1]);
set(gca, 'FontName', 'Arial', 'FontSize', 15, 'Box', 'on');

hNspikes = histc(Nspikes, edges);
B = bar(edges, hNspikes, 'histc');

xlabel(sprintf('# Events, in %.0f sec', (max(A(:,1)))/1000.));
ylabel('# Electrodes');
set(B, 'FaceColor', [0 0 0], 'EdgeColor', [0 0 0]);

XTICK = get(gca, 'XTick');
XTICK(1) = 1; set(gca, 'XTick', XTICK);

INFO.Nactive_electrodes    = length(find(Nspikes>(0.02 * INFO.T / 1000.)));
title(sprintf('Active electrodes (i.e., >0.02 Hz): %d out of 60 (%.1f %%).', INFO.Nactive_electrodes, 100*INFO.Nactive_electrodes/60.));

print(gcf, '-dpng', '-loose',  sprintf('%s/active_electrodes.png', OUTPATH))
saveas(gcf, sprintf('%s/active_electrodes', OUTPATH), 'fig')
print(gcf, '-dpdf', '-loose', sprintf('%s/active_electrodes.pdf', OUTPATH))


end
%--------------------------------------------------------------------------

