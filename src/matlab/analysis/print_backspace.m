function print_backspace(num)
%print_backspace prints backspace(s) specified by the variable 'num'
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 01:43)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 01:43:11 $
% 
if num ~= 0,
    for i=1:num
        fprintf('\b');
    end
end
end % print_backspace()
%--------------------------------------------------------------------------

