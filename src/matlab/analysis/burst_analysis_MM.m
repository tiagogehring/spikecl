function pelt_matrix = burst_analysis_MM(A)
%burst_analysis_MM detects the bursts as per the Burst detection algorithm
% described in Van Pelt et al., 2004; IEEE Trans. Biomed. Eng., 51(11):2051-62.
%
%	input A: nx3 matrix
%			 first column is the spike time stamp
%			 second column is the electrode name
% 			 third column is the electrode number
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 2nd, 2013 - 15:29)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/02 15:29:15 $
%  

global FIGS INFO SHOW OUTPATH

%
% Burst detection algorithm (from Van Pelt et al., 2004; IEEE Trans...)
% Gamba's version, smaller, simpler, faster. but just for 60 electrodes.
%
% Modified from Gambazzi's 2009 original version. M.G. Apr 2013
% as it was not working.
% ---------------------------------------------------------- %
%


% Settings ----------------------------------------------------------------

BURST_BIN       = 25.;  % 25 ms - bin size to detect the bursts
PELT_FACTOR     = 0.05; % define start and stop bin as 5% of the peak ampl.
%TRUNK_RECORDING_AFTER = 15*60*1000; % min*sec*ms <-- SET THE VALUE!!!

TRUNK_RECORDING_AFTER = INFO.T

PRECISE_LIMITS  = 0;    % set anything but 0 to have a sub-ms precision
% but watch out: it gets extremely slow and
% useless!!

SN_RATIO_THRESHOLD = 0; % when events are > than X time the recurrent
% activity*active channels -> this is a burst
% if 0, a fixed BURST_THRESHOLD will be used.

BURST_THRESHOLD    = 100;  % fixed threshold on every recording
FORCE_MINIMUM_TH   = 100;  % if dynamic threshold is < than X, set X.

PLOT         = 0; % set anything but 0 to plot some data (useful to debug)
SHOW_SUMMARY = 0; % set anything but 0 to show a summary at the end

% DO NOT EDIT BELOW THIS LINE ---------------------------------------------
% -------------------------------------------------------------------------

start_clock = rem(now,1);

RECORDING_BIN = 1000./INFO.sr; % [millisec]


% 2. Creating and filling the raster_matrix -------------------------------
% -------------------------------------------------------------------------
% now we create the big matrix
% lines:   nchan -> (1:60)
% columns: length(0:RECORDING_BIN:TRUNK_RECORDING_AFTER) -> ms of recording

% Brief data conversion, into a 2-column style of the older analysis.
spikes              = A;
spikes(:,3)         = abs(spikes(:,3));
[spikes, tmpspikeI] = sortrows(spikes, 1);
spikes              = [spikes(:,1) , spikes(:,3)];

rows                = 60;
cols                = length(0:RECORDING_BIN:TRUNK_RECORDING_AFTER);

fprintf('Allocating "raster_matrix": %d lines, %d columns (pls wait!)\n',rows,cols);
raster_matrix = repmat(false,rows,cols);

%fprintf('\nFilling "raster_matrix":    0%%');
%s='00';

for i=1:length(spikes), % For each spike recorded...
    %if mod(i,length(spikes)/100) == 0,  % refresh the screen
    %    print_backspace(length(s));     % refresh the screen
    %    s = sprintf('%d%s',round(i/length(spikes)*100),'%'); % refresh
    %    fprintf(s); fprintf('%%');      % refresh the screen
    %end
    
    %raster_matrix(spikes(i,2)+1,int64(spikes(i,1)/RECORDING_BIN)) = true;
    raster_matrix(spikes(i,2),int64(spikes(i,1)/RECORDING_BIN)) = true;
end

%print_backspace(length(s));
%fprintf('OK');

% 3. Let's create a pelt_matrix from the raster_matrix --------------------
% -------------------------------------------------------------------------
% lets prepare a second matrix with 3 lines and
% length(0:BURST_BIN:TRUNK_RECORDING_AFTER)
% row1: active channels in a BURST_BIN sliding window [BURST_BIN*(i-1):BURST_BIN/2*i]
% row2: idem but with number of spikes
% row3: row1*row2
% row4: detect if is a peak or not (is used in the peak detection)

pelt_cols   = length(0:BURST_BIN:TRUNK_RECORDING_AFTER);
pelt_matrix = repmat(int16(0),4,pelt_cols); % int16 signed, +32000 should be fine


%fprintf('\nFilling "pelt_matrix":       0%%');
%s='00%';
for i=1:pelt_cols-1,
    %if mod(i,ceil(pelt_cols/100)) == 0, % refresh the screen
    %    print_backspace(length(s));     % refresh the screen
    %    s = sprintf('%d%s',round(i/pelt_cols*100),'%'); % refresh the screen
    %    fprintf(s); fprintf('%%');      % refresh the screen
    %end
    
    start_time = (i-1)*BURST_BIN; % time in ms
    stop_time  = (i)*BURST_BIN;
    % now we have to find the equivalent in raster_matrix index
    
    start_raster_i = start_time/RECORDING_BIN;
    if start_raster_i < 1; start_raster_i = 1; end
    stop_raster_i = stop_time/RECORDING_BIN;
    if stop_raster_i > cols; stop_raster_i = cols; end
    
%      % for loop added by mufti for storing the individual raster
%     for r = 1:rows
%         splitted_raster(i,:) = raster_matrix(r,start_raster_i:stop_raster_i);
%     end
    t_raster_matrix  = sum(raster_matrix(:,start_raster_i:stop_raster_i),2);
    pelt_matrix(1,i) = length(find(t_raster_matrix~=0));
    pelt_matrix(2,i) = sum(t_raster_matrix);
    pelt_matrix(3,i) = pelt_matrix(1,i)*pelt_matrix(2,i);
end
%print_backspace(length(s));
%fprintf('OK\n');

if ~BURST_THRESHOLD,
    BURST_THRESHOLD = SN_RATIO_THRESHOLD*mean(pelt_matrix(1,:))*mean(pelt_matrix(2,:));
    if BURST_THRESHOLD < FORCE_MINIMUM_TH, BURST_THRESHOLD = FORCE_MINIMUM_TH; end
end

fprintf('Detection threshold:        %.2f [active_ch*events/bin]\n',BURST_THRESHOLD);

if PLOT,
    figure(int8(PLOT));clf; 
    if ~SHOW
        set(gcf, 'Visible', 'off')
    end
    hold on;
    leg1 = plot(pelt_matrix(3,:)); % this gives the blue line
    leg2 = plot(pelt_matrix(1,:),'ro');
    leg3 = plot(pelt_matrix(2,:),'gx');
    leg4 = line([0 length(pelt_matrix(3,:))],[BURST_THRESHOLD BURST_THRESHOLD],'LineWidth',2,'Color',[1 0 0]);
    legend([leg1 leg2 leg3 leg4], 'active chan * events/bin', 'active chan', 'events', 'threshold');
end


% 4. Detecting peaks in the pelt matrix -----------------------------------
% -------------------------------------------------------------------------

% Def: a peak is the max(row3) > BURST_THRESHOLD between two points
% <= PELT_FACTOR*max(row3)

% pelt_burst_i is a matrix with #burst lines and 3 columns
% column 1: pelt_matrix index for the left limit
% column 2: pelt_matrix index for the peak
% column 3: pelt_matrix index for the right limit


DDD = fix(300. / BURST_BIN);
fprintf('Detecting burst peaks:      0%%');
burst_n=0;
pelt_burst_i = [];
%s='0%';
i=1;
while (i<pelt_cols),
    if (pelt_matrix(1,i) >= (INFO.Nactive_electrodes / 4.))
        burst_n = burst_n+1;
        %----------------------------------------------
        tmp_peak_i = i+1;
        tmp_peak_v = pelt_matrix(1,i);
        while (pelt_matrix(1,tmp_peak_i) > tmp_peak_v) && (tmp_peak_i<pelt_cols),
            tmp_peak_v = pelt_matrix(1,tmp_peak_i);
            tmp_peak_i = tmp_peak_i + 1;
        end
        pelt_peak_i = tmp_peak_i - 1;
        %----------------------------------------------
        pelt_left_i  = max([1 pelt_peak_i-DDD]);
        pelt_right_i = min([pelt_peak_i+DDD pelt_cols]);
        
        pelt_burst_i(burst_n,1) = pelt_left_i;
        pelt_burst_i(burst_n,2) = pelt_peak_i;
        pelt_burst_i(burst_n,3) = pelt_right_i;
        
        i = pelt_right_i+fix(DDD/2);
    else
        i = i + 1;
    end
end

for i=1:burst_n,
    peak_i = pelt_burst_i(i,2);
    threshold = PELT_FACTOR * pelt_matrix(3,peak_i);
    
    ti = peak_i-1; if (ti <= 1), ti =1; end
    while (pelt_matrix(3,ti) > threshold) && (ti>1),
        ti = ti-1;
    end
    pelt_left_i = ti;
    %----------------------------------------------
    ti = peak_i+1; if (ti >= pelt_cols), ti =pelt_cols; end
    while (pelt_matrix(3,ti) > threshold) && (ti<pelt_cols),
        ti = ti+1;
    end
    pelt_right_i = ti;
    %----------------------------------------------
    pelt_burst_i(i,1) = pelt_left_i;
    pelt_burst_i(i,3) = pelt_right_i;
end




if burst_n == 0,
    %print_backspace(length(s));
    fprintf('No burst found,... we stop here.\n');
    INFO.BURSTS.message = 'No burst found,... exit.';
    INFO.BURSTS.Nburst     = 0;
    INFO.BURSTS.DUR        = 0;
    INFO.BURSTS.meanDUR    = 0;
    INFO.BURSTS.stdDUR     = 0;
    INFO.BURSTS.IBI        = 0;
    INFO.BURSTS.meanIBI    = 0;
    INFO.BURSTS.stdIBI     = 0;
    INFO.BURSTS.tpeaks     = 0;
    INFO.BURSTS.trights    = 0;
    INFO.BURSTS.tlefts     = 0;
    %BURSTS.name       = pathname;
    INFO.BURSTS.Tdur       = 0;
    INFO.BURSTS.Nspikes    = length(spikes);
    INFO.BURSTS.Nspikes_IB = 0; % to be fixed;
    INFO.BURSTS.SPIKES     = spikes;
    INFO.BURSTS.TH         = 0;
    INFO.BURSTS.pelt_matrix=0;
    INFO.BURSTS.burst_detect_bin=0;
    %savepath=sprintf('%s/analysis.mat',pathname);
    %save(savepath, 'BURSTS');
    e = rem(now,1) - start_clock;
    fprintf('Elapsed time:               %sh %sm %ss\n',datestr(e,'HH'),datestr(e,'MM'),datestr(e,'SS'));
    return;
end

%print_backspace(length(s));
fprintf('OK (%d found)\n',burst_n);

if PLOT,
    figure(int8(PLOT));
    if ~SHOW
        set(gcf, 'Visible', 'off')
    end
    for i=1:size(pelt_burst_i,1),
        line([pelt_burst_i(i,1) pelt_burst_i(i,1)],[0 1000],'Color',[0 .5 0],'LineStyle','-');
        line([pelt_burst_i(i,2) pelt_burst_i(i,2)],[0 1000],'Color',[1 0 0]);
        line([pelt_burst_i(i,3) pelt_burst_i(i,3)],[0 1000],'Color',[0 1 0],'LineStyle','-');
    end
    title(sprintf('BURST = %d', length(pelt_burst_i)))
end



% 5. Convert pelt_matrix indices in raster_matrix indices -----------------
% -------------------------------------------------------------------------

% now we have pelt_burst_i with the index of pelt_matrix_indices for every
% burst: [left, peak, right]

% left has to be converted in the left index in the raster_bin
% peak => center
% right => right

raster_burst_i(:,1) = (pelt_burst_i(:,1) -1) .*BURST_BIN/RECORDING_BIN +1;
raster_burst_i(:,2) = (pelt_burst_i(:,2) -1) .*BURST_BIN/RECORDING_BIN +round(0.5*BURST_BIN/RECORDING_BIN);
raster_burst_i(:,3) = (pelt_burst_i(:,3) -1) .*BURST_BIN/RECORDING_BIN +round(BURST_BIN/RECORDING_BIN);

% if an index is bigger than cols,... set it to cols.
raster_burst_i(raster_burst_i(:,3)>cols,3) = cols;
raster_burst_i(raster_burst_i(:,2)>cols,2) = cols;
raster_burst_i(raster_burst_i(:,1)>cols,1) = cols;



% 6. Detecting the real burst limits --------------------------------------
% -------------------------------------------------------------------------

t_lefts  = zeros(size(raster_burst_i,1),1);
t_rights = zeros(size(raster_burst_i,1),1);
t_peaks  = zeros(size(raster_burst_i,1),1);

fprintf('Detecting burst limits:     0%%');
s='0%';

for i=1:size(raster_burst_i,1),
    
    if mod(i,ceil(length(raster_burst_i)/100)) == 0,
        print_backspace(length(s));
        s = sprintf('%d%s',round(i/length(raster_burst_i)*100),'%');
        fprintf(s); fprintf('%%');
    end
    
    % left 50% limit
    raster_start_i = raster_burst_i(i,1);
    totspikesL  = sum(sum(raster_matrix(:,raster_start_i:raster_burst_i(i,2))));
    totspikesLL = totspikesL;
    while (totspikesLL >= 0.5*totspikesL && totspikesL~=0 && raster_start_i < raster_burst_i(i,2)),
        if PRECISE_LIMITS, raster_start_i=raster_start_i+1;     % add RECORDING_BIN every step
        else raster_start_i=raster_start_i+1/RECORDING_BIN; end % add 1 ms every step
        
        if (raster_start_i >= raster_burst_i(i,2)), raster_start_i = raster_burst_i(i,2); end
        
        totspikesLL = sum(sum(raster_matrix(:,raster_start_i:raster_burst_i(i,2))));
    end
    if totspikesL==0, raster_start_i = raster_burst_i(i,2); end
    
    % right 50% limit
    raster_stop_i = raster_burst_i(i,2);
    totspikesR  = sum(sum(raster_matrix(:,raster_stop_i:raster_burst_i(i,3))));
    totspikesRR = totspikesR;
    while (totspikesRR >= 0.5*totspikesR && totspikesR~=0 && raster_stop_i<raster_burst_i(i,3) ),
        if PRECISE_LIMITS, raster_stop_i=raster_stop_i+1;
        else raster_stop_i=raster_stop_i+1/RECORDING_BIN; end
        
        if (raster_stop_i >= raster_burst_i(i,3)), raster_stop_i = raster_burst_i(i,3); end
        
        totspikesRR = sum(sum(raster_matrix(:,raster_stop_i:raster_burst_i(i,3))));
    end
    if totspikesR==0,  raster_stop_i = raster_burst_i(i,2); end
    
    % !!! these are still the 50% borders!
    t_lefts(i)  = raster_start_i     *RECORDING_BIN;
    t_peaks(i)  = raster_burst_i(i,2)*RECORDING_BIN; % this is a stupid value, based on BURST_BIN
    t_rights(i) = raster_stop_i      *RECORDING_BIN;
end

print_backspace(length(s));
fprintf('OK\n');


% fix the left and right borders by duplicating the distance between
% border and peak
for i=1:size(raster_burst_i,1),
    t_lefts(i)  = t_lefts(i)  - (t_peaks(i)-t_lefts(i));
    t_rights(i) = t_rights(i) + (t_rights(i)-t_peaks(i));
end

if PLOT,
    hold off;
end

% ------------------------------added by mufti-----------------------------------
% this will create a matrix to store the bursts with events across all electrodes

left_max = max(raster_burst_i(:,2)-raster_burst_i(:,1));
right_max = max(raster_burst_i(:,3)-raster_burst_i(:,2));
max_distance = left_max + right_max;
mid_ind = ceil(max_distance/2);

if left_max > mid_ind
    max_distance = max_distance + (left_max + 1 - mid_ind) * 2;
    mid_ind = ceil(max_distance/2);
end

if right_max > mid_ind
    max_distance = max_distance + (right_max + 1 - mid_ind) * 2;
    mid_ind = ceil(max_distance/2);
end

bursts_across_electrodes = zeros(size(raster_burst_i,1),max_distance);

t = linspace(0,max_distance*RECORDING_BIN,max_distance);

for xx = 1:size(raster_burst_i,1)
    % set the starting index to 1 and align accordingly
    norm_ind = (raster_burst_i(xx,:)-raster_burst_i(xx,1)) + 1;
    offset_ind = mid_ind - norm_ind(2);
    new_ind = norm_ind + offset_ind;
    bursts_across_electrodes(xx,new_ind(1):new_ind(3)) = sum(raster_matrix(:,raster_burst_i(xx,1):raster_burst_i(xx,3)));
end

% this is to create the real 3d pelt matrix for the plotting

bursts_in_electrodes = zeros(rows,max_distance);

psth_bin = 10; % 10 ms bin
edges = 0:psth_bin:max_distance*RECORDING_BIN;

new_pelt_matrix = zeros(rows,length(edges),burst_n);

for xx = 1:rows % for each electrode
    % set the starting index to 1 and align accordingly
    for bur = 1:burst_n % for each burst
        norm_ind = (raster_burst_i(bur,:)-raster_burst_i(bur,1)) + 1;
        offset_ind = mid_ind - norm_ind(2);
        new_ind = norm_ind + offset_ind;
        temp_bursts_in_electrodes = zeros(1,max_distance);
        temp_bursts_in_electrodes(new_ind(1):new_ind(3)) = raster_matrix(xx,raster_burst_i(bur,1):raster_burst_i(bur,3));
        bursts_in_electrodes(xx,:) = bursts_in_electrodes(xx,:) + temp_bursts_in_electrodes;
        new_pelt_matrix(xx,:,bur) = histc(find(temp_bursts_in_electrodes>0)*RECORDING_BIN,edges);
    end
end

plot_pelt_matrix(new_pelt_matrix, edges, OUTPATH)

% 7. Saving burst information as the old "burst_detect.," used to do ------
% -------------------------------------------------------------------------

INFO.BURSTS.Nburst     = length(t_peaks);
INFO.BURSTS.DUR        = t_rights - t_lefts;
INFO.BURSTS.meanDUR    = mean(t_rights - t_lefts);
INFO.BURSTS.stdDUR     = std(t_rights - t_lefts);
INFO.BURSTS.IBI        = t_rights(2:end) - t_lefts(1:end-1);
INFO.BURSTS.meanIBI    = mean(t_rights(2:end) - t_lefts(1:end-1));
INFO.BURSTS.stdIBI     = std(t_rights(2:end) - t_lefts(1:end-1));
INFO.BURSTS.tpeaks     = t_peaks;
INFO.BURSTS.trights    = t_rights;
INFO.BURSTS.tlefts     = t_lefts;
%BURSTS.name       = pathname;
INFO.BURSTS.Tdur       = TRUNK_RECORDING_AFTER;
INFO.BURSTS.Nspikes    = length(spikes);
INFO.BURSTS.Nspikes_IB = 0; % to be fixed;
INFO.BURSTS.SPIKES     = spikes;
INFO.BURSTS.TH         = BURST_THRESHOLD;
INFO.BURSTS.pelt_matrix=pelt_matrix;
INFO.BURSTS.burst_detect_bin = BURST_BIN;
if PRECISE_LIMITS,
    INFO.BURSTS.message     = 'OK - PRECISE LIMITS';
else
    INFO.BURSTS.message     = 'OK - 1ms ROUNDED LIMITS';
end

if SHOW_SUMMARY,
    fprintf('Number of bursts:           %d\n',INFO.BURSTS.Nburst);
    fprintf('Average duration:           %d\n',INFO.BURSTS.meanDUR);
end

% 9. plot the pelt diagram for average 

%-----------------------------------------------

%savepath=sprintf('%s/analysis.mat',pathname);
%save(savepath, 'BURSTS');
%disp('Saving file "analysis.mat": OK');
e = rem(now,1) - start_clock;
fprintf('Elapsed time:               %sh %sm %ss\n',datestr(e,'HH'),datestr(e,'MM'),datestr(e,'SS'));
%return;

% close all

end % burst_detect()
%--------------------------------------------------------------------------

