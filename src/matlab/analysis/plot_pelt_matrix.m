function plot_pelt_matrix(new_pelt_matrix, edges, OUTPATH)
%plot_pelt_matrix plots the burst profile across different electrodes.
%
% inputs: 
%       new_pelt_matrix : a 3D matrix containing
%                   	  1D: number of electrodes (1..60)
%						  2D: event raster during the burst
%						  3D: bursts
%       edges           : binned edges of time values
%       OUTPATH         : path of the preprocessed data files
%
% copyright 2013 - Michele Giugliano, PhD (https://www.uantwerpen.be/en/staff/michele-giugliano/) (Antwerp, Monday Dec 3rd, 2013 - 00:08)
% (bug-reports to qst@tnb.ua.ac.be)
% $Revision: 1.0 $  $Date: 2013/12/03 00:08:09 $
%  
figure('Name',['Bursting profile with bin size: ' num2str(edges(2)-edges(1)) ' ms'],'NumberTitle','off','Color',[1,1,1]);
rows = 60;
pm = mean(new_pelt_matrix,3); 
x_ticks = (-edges(end-1)/2:(edges(2)-edges(1)):edges(end)/2)*0.001; % to sec
h(1)=subplot(411); 
set(gca,'FontSize',12)
area(edges, mean(pm,1),'FaceColor',[0.6,0.6,0.6]),
xlim([edges(1) edges(end)])
% xlabel('time [ms]')
ylabel(h(1),'# spikes/burst/bin')
title(h(1),['avegare population burst profile (bin size ' num2str(edges(2)-edges(1)) ' ms)'])


h(2)=subplot(4,1,2:4);
% baseLine=0.2; 
hold on, 
for xx=1:rows, 
    plot(edges, pm(xx,:)+xx,'Color',[0.7,0.7,0.7]), 
%     fill(edges,pm(xx,:)+xx,[0.4,0.4,0.4],'EdgeColor','none'),
end, 
for xx=1:rows, 
%     plot(edges, pm(xx,:)+xx,'Color',[0.7,0.7,0.7]), 
    fill(edges,pm(xx,:)+xx,[0.4,0.4,0.4],'EdgeColor','none'),
end, 

xlim([edges(1) edges(end)])
if ~mod(length(x_ticks),2)
    set(h(1),'XTick',edges(1:8:end),'XTickLabel',x_ticks(1:8:end),'FontSize',12)
    set(h(2),'XTick',edges(1:8:end),'XTickLabel',x_ticks(1:8:end),'FontSize',12)
else
    set(h(1),'XTick',edges(1:6:end),'XTickLabel',x_ticks(1:6:end),'FontSize',12)
    set(h(2),'XTick',edges(1:6:end),'XTickLabel',x_ticks(1:6:end),'FontSize',12)
end

ylim([1 max(pm(rows,:)+rows)])
xlabel('time [s]')
ylabel('# electrodes')
% title('individual burst profile in different electrodes')
hold off

print(gcf, '-dpdf', '-loose',  sprintf(['%s' filesep 'vanpelt_plot_complete'], OUTPATH))
print(gcf, '-dpng', '-loose',  sprintf(['%s' filesep 'vanpelt_plot_complete'], OUTPATH))
saveas(gcf, sprintf(['%s' filesep 'vanpelt_plot_complete'], OUTPATH), 'fig')