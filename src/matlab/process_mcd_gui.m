function process_mcd_gui(varargin)
    % for the GUILayout package we have 2 different versions for (Matlab 2014a
    % and below and newer versions)
    v = version();
    if str2num(v(1:3)) <= 8.3 % <= Matlab 2014a
        addpath(fullfile(fileparts(mfilename('fullpath')), '/3rdparty/GUILayout/v1'));
        addpath(fullfile(fileparts(mfilename('fullpath')), '/3rdparty/GUILayout/v1/Patch'));
    else        
        addpath(fullfile(fileparts(mfilename('fullpath')), '/3rdparty/GUILayout/v2'));
    end    
    addpath(fullfile(fileparts(mfilename('fullpath')), '/3rdparty'));
    wnd = process_mcd_main(varargin{:});
    wnd.show();
end