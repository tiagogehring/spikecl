#include <iostream>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "data_source/mcs_file_analog.hpp"
#include "dsp/bidirectional_filter.hpp"
#include "multichannel/analog_data.hpp"
#include "config/common_setup.hpp"
#include "config/filter_setup.hpp"

namespace po = boost::program_options;
namespace fs = boost::filesystem;

#if USE_DOUBLE_PRECISION
   using fp_type = double;
#else
   using fp_type = float;
#endif

/**
 * Type definitions
 */
// our data source - analog data from MCS files in this case
using data_source_type = data_source::mcs_file_analog;
// input data type (usually 16 bit integers as read by the A/D converter)
using input_type = data_source_type::data_type;
using output_type = fp_type;
// input buffer type that can store and address multiple data channels and chunks
using input_buffer_type = multichannel::analog_data<input_type>;
// output, processed buffer type output from the filter (float/double)
using output_buffer_type = multichannel::analog_data<output_type>;
// the filter type -> takes data from the source (usualy 16 bit integers) and converts them to (filtered) floating points;
using filter_type = dsp::bidirectional_filter<typename data_source_type::data_type, output_type>;
// stores filter coefficients and other properties
using filter_kernel_type = filter_type::kernel_type;

/**
 * Main function
 */
int main(int argc, char* argv[])
{
   using namespace std;

   string input, output; /// input and output file names
   common_setup cs;
   filter_setup fs;

   try
   {
      /**
       * Process command line arguments
       */
	   po::options_description desc("Allowed options");
      // add options shared amont all tools
	   desc.add_options()
		  ("help", "show this message")
		  ("input", po::value<string>(&input), "input file name")
		  ("output,o", po::value<string>(&output), "output file name");
      cs.add_options(desc);
      fs.add_options(desc);
	   po::positional_options_description pos_desc;
	   pos_desc.add("input", -1);

	   po::variables_map vm;
	   po::store( po::command_line_parser(argc, argv)
				 .options(desc)
				 .positional(pos_desc)
				 .run(), vm);
	   po::notify(vm);

	   if (vm.count("help"))
	   {
		  cout << endl << desc << endl;
		  return 0;
	   }

	   if (vm.count("input") == 0)
	   {
		  cerr << "No input file specified!";
		  cout << endl << desc << endl;
		  return 1;
	   }

      // regular file - just check if it exists
      if (!fs::exists(input))
      {
         cerr << "Input file not found!" << endl;
         return 1;
      }
      if (fs::is_directory(input))
      {
         cerr << input << " is a directory!" << endl;
         return 1;
      }

	   fs::path outdir;
	   if (vm.count("output"))
	   {
		  outdir = fs::path(vm["output"].as<string>());
		  if (!fs::exists(outdir))
		  {
			 cerr << "Directory " << outdir << " not found. Aborting.";
			 return 1;
		  }
	   }
	   else
	   {
		  outdir = fs::current_path();
	   }

      // process common command line arguments
      cs.process_options(vm);
      // process filter command line arguments
      fs.process_options(vm);

      /**
       * Initialization
       */

      // data source (our file)
      data_source::mcs_file_analog f{};
      // OpenCL context
      boost::compute::context ctx{cs.device()};
      // OpenCL command queue
      boost::compute::command_queue queue{ctx, cs.device()};

      // load file
      f.load_file(input);
      std::cout << "File " << input << " loaded." << std::endl;
      std::cout << "Sampling rate: " << f.dimensions().sampling_rate << " Hz  Time span: " << f.dimensions().timespan() << " seconds" << std::endl;

      // filter kernel
      filter_kernel_type kern { fs.filter_coefficients(f.dimensions().sampling_rate)
                              , 1
                              , static_cast<int>(dsp::filter_kernel_options::scale_input)
                              , f.reference_value()
                              , static_cast<fp_type>(f.scalling_factor())
      };
      // filter
      filter_type filter{ctx, kern};
      // maximum number of samples per data chunk (limited by the OpenCL runtime)
      auto max_ns = cs.device().get_info<size_t>(CL_DEVICE_MAX_MEM_ALLOC_SIZE)/sizeof(fp_type);

      // input buffer
      input_buffer_type in{ctx, 1, max_ns};
      // output buffer
      output_buffer_type out{ctx, 1, max_ns};
      // intermediate buffer - output from the 1st pass of the filter)
      output_buffer_type first{ctx, 1, max_ns};

      // maps channel indices to electrode numbers
      vector<size_t> elect_map;
      for (size_t i = 0; i < f.dimensions().channels; ++i)
      {
         std::string lbl = f.channel_identification(i);
         size_t en = boost::lexical_cast<size_t>(lbl.substr(lbl.size() - 2, 2));
         elect_map.push_back(en);
      }
      std::cout << "Found " << f.dimensions().channels << " analog channels with " << f.dimensions().samples << " samples each." << std::endl;

      /**
       * Processing
       */
      std::cout << "Reading data..." << std::endl;
      // read all the samples
      in.read(f, f.dimensions().samples, queue);

      size_t nchan = cs.values().channels;
      if (nchan == (size_t)-1 || nchan == 0)
      {
         // process until end of file
         nchan = f.dimensions().channels - cs.values().channel_offset;
      }

      first.reshape(in);
      out.reshape(in);
      // process n channels in parallel
      size_t co = cs.values().channel_offset;

      do
      {
         // number of real channels to process
         size_t nc = std::min(cs.values().parallel_channels, cs.values().channel_offset + nchan - co);
         // number of work items or blocks of channels to processd (dependent on the SIMD width)
         size_t wi = nc / cs.values().vector_width;
         if (nc % cs.values().vector_width != 0)
            wi += 1;

         filter(in.data()[0], co, nc, first.data()[0], out.data()[0], true, queue);
         co += cs.values().vector_width*cs.values().parallel_channels;
      } while(co < cs.values().channel_offset + nchan);
      queue.finish();
   }
   catch(exception& e)
   {
      std::cerr << e.what();
      return 1;
   }

   return 0;
}
