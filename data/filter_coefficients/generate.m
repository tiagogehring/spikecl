function generate(f_lo, f_hi) 
    % Pre-computes filter coefficients for a range of frequencies
    format long;

    % filter order
    order = 2;
    %frequency range
    freq = 7000:100:100000;
    % description
    desc = ['Band-pass filter (' num2str(f_lo) '-' num2str(f_hi) ') Hz)'];

    vals = zeros(length(freq), 2*(2*order + 1) + 1);
    for i = 1:length(freq)
        % use an elliptic filter
        [b, a] = ellip(2, 0.1, 40, [f_lo f_hi]*2/freq(i));
        vals(i, :) = [freq(i), b, a];
    end
    dlmwrite(['ellip2_' num2str(f_lo) '_' num2str(f_hi) '.csv'], vals, 'precision', 16);

    % generate also a C++ source file that can be used as the default set of
    % coefficients
    fid = fopen('default_filter_coefficients.cpp', 'wt');
    fprintf(fid, 'char g_default_filter_description[] = \"%s\";', desc); 
    fprintf(fid, '\ndouble g_default_filter_coefficients[][%d] = {', 2*(2*order + 1) + 1);
    for i = 1:size(vals, 1)
        fprintf(fid, '\n{%d.', vals(i, 1));
        for j = 2:size(vals, 2)
            fprintf(fid, ',%.16f', vals(i, j));
        end
        fprintf(fid, '}');
        if (i < size(vals, 1))
            fprintf(fid, ',');
        end    
    end
    fprintf(fid, '\n};');
    fclose(fid);
end