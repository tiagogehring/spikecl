[TOC]

# Introduction

spiketool is a command line utility to process data from microelectrode array (MEA) recordings. The analyzes currently supported are data filtering (using a zero-phase band-pass elliptic filter by default) and spike detection based on a threshold estimation from the noise of the data ([Quiroga][quiroga]). 

In order to achieve optimal performance multiple data channels can be processed in parallel and the processing of a set of channels are reading of new ones runs concurrently. 

[quiroga]: http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=6790337

# Sources

Sources can be downloaded with git:

       git clone https://{username}@bitbucket.org/tiagogehring/spiketool.git

(replace {username} by your bitbucket user name - to gain access to the repository please send an e-mail to t.gehring@sheffield.ac.uk). For building instructions please see below. 

# Running spiketool

spiketool is a command line based utility. When run without arguments the
available command line options will be shown. One or more MCD files can be
processed at one time and by default, if no other option is specified, all
data channels will filtered and then a spike detection algorithm will be run.
This will generate a subdirectory with the same name as the file (without
extension) containing all generated files. By default following files are
generated:

* Info file ({FILENAME}_info.txt): contains generic file data such as number of samples and data channels as well the electrode names for each channel;
* Spikes file ({FILENAME}_spikes.txt): contains a list of all of the spikes for each data channel (first column: spike time in milliseconds, electrode number, data channel index (starting with 1). Note: negative spikes will be identified by a minus sign in the channel and electrode numbers;
* Waveforms file ({FILENAME}_chan_x##.csv): One file per channel containing the data points that make up a spike. By default the 20 points preceeding and the 44 succeeding the spike peak are saved. Each line consists of the spike time (in milliseconds) followed by the data points;

## Basic usage: processing one file

To process one file named {FILENAME} simply run:

	spiketool {FILENAME}

This will create a subdirectory called {FILENAME} (without extension) on the current directory containing the resulting data files.

## Processing multiple files

Multiple files can be passed as command line arguments. Masks are also accepted:

	spiketool {FILENAME_A} {FILENAME_B} *.mcd

will process one file at a time generating one subdirectory with results for each one of them.

## Specifying the output directory

By default the current directory is used as base for the resulting output data directory. To specific a different one use the --output (-o) argument:

	spiketool --output /tmp {FILENAME} 

will put the resulting files in the /tmp/{FILENAME} directory. By default a subdirectory is automatically created, to avoid this use --create-dir=0 (or
-C=0):

	spiketool --output /tmp --no-create-dir {FILENAME} 

If the output directory already exists it will not be overwritten by default and processing of the file will be skipped. To override this behaviour pass
the --overwrite command line argument.

## Data filter

By default a zero-phase band-pass filter (400-3000 Hz) will be applied to the data. If the default filter is not suitable explicit filter coefficients
of the infinite-impulse response (IIR) filter can be specified with the -b and -a command line arguments (the former specifies the input data coefficients,
the second the recursive coefficients).

Since the filter will in general depend on the sampling frequency of the input data, a pre-computed set of coefficients can be specified with the 
--coeff-file command line argument. The input file has to be int he CSV format and in each line contain the sampling frequency followed by the b and a 
filter coefficients (a total of 2xORD + 1 each, where ORD = the filter order).

## Extracting data channels

The tool can also save the raw data for each channel (filtered or not) to individual files (--extract-channels or -E flag):

	spiketool --extract-channels=1 {FILENAME} 

will save each data channel to a {FILEANAME}_chan_x##.dat file.

_Note_: The above command will also perform the default operations (filtering and spike detection) on each data channel. To skip this and store the unfiltered data only for each channel run:

	spiketool --extract-channels=1 --filter=0 --spikes=0 {FILENAME}

# Runtime dependencies

## OpenCL

Spiketool uses [OpenCL][ocl] to parallelize and optimize the computationally intensive tasks for the available hardware (multicode CPU or GPU). As such it depends on an OpenCL implementation (e.g. pocl or Intel's, AMD's or nVidia's proprietary implementations) being installed on the system. The Portable OpenCL (pocl) implementation [pocl] is the recommended platform since it offers in general better performance; this because its code is compiled and highly optimized for the underlying hardware during installation. 
Modern Mac OS X systems support OpenCL nativelly. However, using pocl can also be advantageous since it can as already said lead to performance benefits. The pocl library can be build automatically as part of spiketool building instructions (see below). Alternatively, a proprietary OpenCL platform can be installed in Windows or Linux. Please refer to [Intel][intelocl], [AMD][amdocl] or [nVidia][nvidiaocl] it the system hardware is an Intel CPU, AMD CPU/GPU or nVidia GPU respectively.

### Intel OpenCL Runtime for Linux

On some Linux distributions (e.g. Ubuntu) the OpenCL Runtime installation will show a warning that the platform is not supported and will not create  some necessary simlinks. In this cases please install the runtime normally (running sudo ./install.sh from the directory where it was unpacked), ignore any warnings during the install and run

          sudo ln -s /opt/intel/opencl-########/lib64/libOpenCL.so.1.2 /usr/lib/libOpenCL.so
          sudo ldconfig
          sudo ln -s /opt/intel/opencl-########/etc/intel.icd /etc/OpenCL/vendors/intel.icd

_note:_ if the application still fails to run the directory /opt/intel/opencl-#########/lib64 might have to be added to the library path. On Ubuntu this can be done by adding the name of the folder to a file (eg. intelocl.conf) inside the /etc/ld.so.conf.d/ directory and running ldconfig as root (with sudo).

## Multichannel Systems' MCStream library

To read multichannel recording files the MCStream library from Multichannel Systems is needed. This library is distributed with the MCRack software and supports Windows, Linux, and OSX (even though MCRack is Windows only). The MCStream library is already distributed with spiketool, Multichannel Systems' license and terms & conditions of use might apply. 

## Boost C++ libraries

Spiketool makes heavy use of the [Boost][boost] C++ libraries. These have to be installed (at least version 1.54) on the system if the code was not linked statically to boost (see building spiketool below). 

# Building spiketool

A reasonably modern C++ compiler with C++11 support is required to build spiketool (>= g++-4.8, >= clang-3.4, >= Visual C++ 2013). 

### Ubuntu 14.04

For Ubuntu 14.04 it is recommended to use the clang compiler to build spiketool since the default compiler, g++-4.8, has C++11 related bugs that make the compilation fail. To use clang simple install it

         sudo apt-get install clang

and then follow the instructions below setting the CXX variable to 'clang++' before running cmake, i.e.:

         CXX=clang++ cmake ../

to setup the sources. 

### Windows

Compiler tested: VC++ 12.0 (comes with Visual Studio 2013 - the free "Express" edition is sufficient to build the code as it is a command line tool). Both 32bit and 64bit versions are supported. Cygwin should work but is currently not tested.
  
## Dependencies

Following tools/libraries are required to compile the sources: 

* [CMake][cmake] (to configure the build)
* [Boost][boost] libraries
* [Boost Compute][boost_comp] used for OpenCL abstraction. Fetched automatically when cloning the main repository. 

For installing CMake and Boost see platform specific steps below. 

[ocl]: https://www.khronos.org/opencl
[intelocl]: https://software.intel.com/en-us/articles/opencl-drivers
[inteloclsdk]: https://software.intel.com/en-us/articles/intel-sdk-for-opencl-applications-installation-guide
[amdocl]: http://developer.amd.com/tools-and-sdks/opencl-zone/amd-accelerated-parallel-processing-app-sdk/
[nvidiaocl]: https://developer.nvidia.com/opencl
[pocl]: http://portablecl.org/
[boost]: http://www.boost.org/
[cmake]: http://www.cmake.org/
[boost_comp]: https://github.com/kylelutz/compute
[cygwin]: https://www.cygwin.com/

### Linux

CMake and the Boost libraries are pre-packaged in most Linux distributions and can be installed using the standard package manager of your distribution. For Boost
Compute see comments below.

### Max OS X 

Mac OS X offers the clang compiler out of the box (running "clang" or "clang++" on a terminal will trigger its installation if it is not already installed).

To install cmake and the Boost libraries the easiest way is to use Homebrew. If it is not already installed run

	ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	brew doctor

from a terminal. Then run

	brew install cmake
	brew install boost

to install both packages. 

### Windows

[CMake][cmake] installation files can be downloaded from their website. Use the CMake GUI to generate Visual C++ solution files. The Boost library might have to be manually compiled, in order to do so dowload the file from the [Boost][boost] website and unpack it to a folder. Then from the start menu in the Visual Studio "Tool" sub-folder start a "Developer Command prompt". In the command window that opens browse to the folder where boost was unpacked and run

        bootstrap
        b2 --toolset=msvc-12.0 variant=release link=static threading=multi address-model=64		

to compile the libraries. Change *threading=multi* to *threading=shared* in case that the dynamic linked version of Boost will be used. Change also the toolset to the appropriate Visual C++ version and the address-model to 32 for a 32bit build. 

## General building instructions

In Windows open the solution file generated by CMake and use the IDE to build it. In Unix variants change to the directory where spiketool sources was unpacked (here assumed to be ../spiketool) and create a new build directory

	mkdir build
	cd build

Then run cmake to configure the sources:

	cmake ../

And finally compile it:

	make

The generated spiketool binary will be found in the ./bin directory.

### Compiling Boost libraries statically

To remove the runtime dependencies (at the cost of a larger binary size) pass
the USE_STATIC_BOOST flag during the configuration step (note that the
static libraries have to be available in the system - these might not be
installed by default during the Boost installation process):

	cmake ../ -DUSE_STATIC_BOOST=1

### Inspecting OpenCL kernels

The OpenCL generated kernels (functions) can be inspected by setting the SHOW_KERNELS
environment variable to 1:

	SHOW_KERNELS=1 ./spiketool ...

The generated OpenCL kernels will be dumped to the standard output during execution.

### Code tests

Tests can be compiled by running 

	make tests

This will generate the tests binary that can be run to test the filtering and spike 
detection routines.

### Documentation

Doxygen generated documentation for the code can be build by running

	make doc

Note that doxygen has to be installed. The generated html documentation will be found
at ./bin/doc.

# Code documentation

Automatically generated Doxygen documentation for the code can be found here: [documentation](http://tiagogehring.bitbucket.org/spiketool/annotated.html); For the latest version or see above for instructions on how to generate the documention from the source tree.
